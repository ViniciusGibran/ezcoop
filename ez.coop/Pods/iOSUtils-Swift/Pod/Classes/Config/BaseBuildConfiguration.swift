//
//  BaseBuildConfiguration.swift
//  Pods
//
//  Created by Livetouch-Mini01 on 15/06/2016.
//
//

import UIKit

public protocol BaseConfigurationDelegate {
    
    func getErrorMessageFilename() -> String
    func getDatabaseHelper() throws -> DatabaseHelper
}

public class BaseBuildConfiguration: NSObject, BaseConfigurationDelegate {
    
    public func getErrorMessageFilename() -> String {
        return "utils_error_messages.json"
    }
    
    public func getDatabaseHelper() throws -> DatabaseHelper {
        throw SQLException.DatabaseHelperNotFound
    }
}