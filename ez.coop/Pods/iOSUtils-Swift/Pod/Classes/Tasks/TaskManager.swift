//
//  TaskManager.swift
//  Pods
//
//  Created by Livetouch-Mini01 on 15/06/2016.
//
//

import UIKit

public class TaskManager: NSObject {
    
    //MARK: - Variables
    
    var baseTask : BaseTask!
    
    var operation : NSBlockOperation!
    
    var activityIndicator : UIActivityIndicatorView?
    
    //MARK: - Start Tasks
    
    static public func startTask(baseTask: BaseTask, withActivityIndicator activityIndicator: UIActivityIndicatorView?) -> NSBlockOperation {
        let taskManager = TaskManager()
        taskManager.baseTask = baseTask
        taskManager.activityIndicator = activityIndicator
        
        taskManager.operation = NSBlockOperation(block: {
            return taskManager.executeTaskBackground()
        })
        
        taskManager.baseTask.operation = taskManager.operation
        return taskManager.operation
    }
    
    //MARK: - Spinner
    
    public func cancelActivityIndicator(dispatch: Bool) {
        if let activityIndicator = activityIndicator {
            if dispatch {
                dispatch_async(dispatch_get_main_queue(), {
                    activityIndicator.stopAnimating()
                });
            } else {
                activityIndicator.stopAnimating()
            }
        }
    }
    
    //MARK: - Task Block Methods
    
    public func preExecuteTask() {
        if operation.cancelled {
            return
        }
        
        if let activityIndicator = activityIndicator {
            activityIndicator.startAnimating()
        }
        
        if let preExecute = baseTask.preExecute {
            preExecute()
        }
    }
    
    public func updateViewTask() {
        if operation.cancelled {
            return
        }
        
        if (baseTask.updateView != nil) {
            baseTask.updateView()
        }
        
        cancelActivityIndicator(false)
    }
    
    //MARK: - Perform Tasks
    
    public func executeTaskBackground() {
        if operation.cancelled {
            cancelActivityIndicator(true)
        }
        
        performSelectorOnMainThread(#selector(TaskManager.preExecuteTask), withObject: nil, waitUntilDone: true)
        
        if operation.cancelled {
            cancelActivityIndicator(true)
            return
        }
        
        do {
            try baseTask.execute()
            
            if operation.cancelled {
                cancelActivityIndicator(true)
                return
            }
            
            performSelectorOnMainThread(#selector(TaskManager.updateViewTask), withObject: nil, waitUntilDone: true)
            
        } catch {
            if operation.cancelled {
                cancelActivityIndicator(true)
                return
            }
            
            dispatch_async(dispatch_get_main_queue(), {
                if let onError = self.baseTask.onError {
                    onError(exception: error)
                } else {
                    self.handleDefaultException(error)
                }
                
                self.cancelActivityIndicator(false)
            });
        }
    }
    
    //MARK: - Error Handling
    
    public func handleDefaultException(exception: ErrorType) {
        if let exception = exception as? Exception {
            switch exception {
                case .DomainException(let message):
                    AlertUtils.alert(message)
                
                case .IOException:
                    showError(.IOException)
                
                case .AppSecurityTransportException:
                    ExceptionUtils.alertAppTransportSecurityException()
                
                default:
                    showError(.GenericException)
            }
        }
    }
    
    public func showError(exception: Exception) {
        let errorMessage = ExceptionUtils.getExceptionMessage(exception)
        
        if errorMessage.isEmpty {
            return
        }
        
        AlertUtils.alert(errorMessage)
    }
}