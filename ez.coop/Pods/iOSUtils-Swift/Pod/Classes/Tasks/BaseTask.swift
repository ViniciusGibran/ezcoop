//
//  BaseTask.swift
//  Pods
//
//  Created by Livetouch-Mini01 on 14/06/2016.
//
//

import UIKit
import Foundation

public typealias TaskBlock = () -> ()
public typealias TaskBlockThrowable = () throws -> ()
public typealias TaskBlockError = (exception: ErrorType) -> ()

//MARK: - Task Delegate

public protocol TaskDelegate {
    func startTask(baseTask: BaseTask, withActivityIndicator activityIndicator: UIActivityIndicatorView?) -> NSBlockOperation
}

//MARK: - Base Task

public class BaseTask: NSObject {
    
    //MARK: Variables
    
    var preExecute  : TaskBlock?
    var execute     : TaskBlockThrowable!
    var updateView  : TaskBlock!
    
    var onError     : TaskBlockError?
    
    var operation   : NSBlockOperation!
    
    //MARK: - Inits
    
    override public init() {
        super.init()
    }
    
    convenience public init(execute: TaskBlockThrowable, updateView: TaskBlock, onError: TaskBlockError? = nil) {
        self.init(preExecute: nil, execute: execute, updateView: updateView, onError: onError)
    }
    
    convenience public init(preExecute: TaskBlock?, execute: TaskBlockThrowable, updateView: TaskBlock, onError: TaskBlockError? = nil) {
        self.init()
        
        self.preExecute = preExecute
        self.execute = execute
        self.updateView = updateView
        self.onError = onError
    }
    
    //MARK: - Methods
    
    static public func preExecute(preExecute: TaskBlock, execute: TaskBlockThrowable, updateView: TaskBlock) -> BaseTask {
        return BaseTask.preExecute(preExecute, execute: execute, updateView: updateView, onError: nil)
    }
    
    static public func preExecute(preExecute: TaskBlock, execute: TaskBlockThrowable, updateView: TaskBlock, onError: TaskBlockError?) -> BaseTask {
        let baseTask = BaseTask()
        
        baseTask.preExecute = preExecute
        baseTask.execute = execute
        baseTask.updateView = updateView
        baseTask.onError = onError
        
        return baseTask
    }
}