//
//  Exception.swift
//  Pods
//
//  Created by Livetouch-Mini01 on 15/06/2016.
//
//

import Foundation

public enum Exception : ErrorType {
    
    case AppSecurityTransportException
    case NotImplemented
    
    case FileNotFoundException
    case IllegalArgumentException(message: String)
    case RunTimeException(message: String)
    
    case GenericException
    case IOException
    case DomainException(message: String)
}