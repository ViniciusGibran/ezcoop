//
//  SideMenuEnums.swift
//  Pods
//
//  Created by Livetouch-Mini01 on 17/06/2016.
//
//

import Foundation

public let SideMenuStateNotificationEvent = "SideMenuStateNotificationEvent"

public enum SideMenuPanMode {
    case None
    case CenterViewController
    case SideMenu
    case Default
}

public enum SideMenuState {
    case LeftMenuOpen
    case RightMenuOpen
    case Closed
}

public enum SideMenuStateEvent: Int {
    case WillOpen = 0
    case DidOpen = 1
    case WillClose = 2
    case DidClose = 3
}

public enum SideMenuPanDirection {
    case None
    case Left
    case Right
}