//
//  SideMenuShadow.swift
//  Pods
//
//  Created by Livetouch-Mini01 on 17/06/2016.
//
//

import Foundation

public class SideMenuShadow: NSObject {
    
    //MARK: - Variables
    
    public var enabled : Bool = false {
        didSet {
            draw()
        }
    }
    
    public var radius : CGFloat! {
        didSet {
            draw()
        }
    }
    
    public var opacity : CGFloat! {
        didSet {
            draw()
        }
    }
    
    public var color : UIColor! {
        didSet {
            draw()
        }
    }
    
    public var shadowedView    : UIView!
    
    //MARK: - Static Inits
    
    static public func shadowWithView(shadowedView: UIView) -> SideMenuShadow {
        let shadow = SideMenuShadow.shadowWithColor(UIColor.blackColor(), andRadius: 10.0, andOpacity: 0.75)
        shadow.shadowedView = shadowedView
        return shadow
    }
    
    static public func shadowWithColor(color: UIColor, andRadius radius: CGFloat, andOpacity opacity: CGFloat) -> SideMenuShadow {
        let shadow = SideMenuShadow()
        shadow.color = color
        shadow.radius = radius
        shadow.opacity = opacity
        return shadow
    }
    
    //MARK: - Init
    
    override public init() {
        super.init()
        
        self.enabled = true
        self.color = UIColor.blackColor()
        self.opacity = 0.75
        self.radius = 10.0
    }
    
    //MARK: - Drawing
    
    public func draw() {
        if enabled {
            show()
        } else {
            hide()
        }
    }
    
    public func show() {
        if (shadowedView == nil) {
            return
        }
        
        var pathRect = self.shadowedView.bounds
        pathRect.size = self.shadowedView.frame.size
        
        //TODO: Tentar fazer a sombra parecer que está no centerViewController e não no leftViewController
        
        shadowedView.layer.shadowPath = UIBezierPath(rect: pathRect).CGPath
        shadowedView.layer.masksToBounds = false
        shadowedView.layer.shadowOpacity = Float(opacity)
        shadowedView.layer.shadowRadius = radius
        shadowedView.layer.shadowColor = color.CGColor
        shadowedView.layer.rasterizationScale = DeviceUtils.getScreenScale()
    }
    
    public func hide() {
        if (shadowedView == nil) {
            return
        }
        
        shadowedView.layer.shadowOpacity = 0.0
        shadowedView.layer.shadowRadius = 0.0
    }
    
    //MARK: - Shadowed View Rotation
    
    public func shadowedViewWillRotate() {
        shadowedView.layer.shadowPath = nil;
        shadowedView.layer.shouldRasterize = true
    }
    
    public func shadowedViewDidRotate() {
        draw()
        shadowedView.layer.shouldRasterize = false
    }
}