//
//  SideMenuContainerViewController.swift
//  Pods
//
//  Created by Livetouch-Mini01 on 17/06/2016.
//
//

import Foundation

public class SideMenuContainerViewController: UIViewController, UIGestureRecognizerDelegate {
    
    //MARK: - Variables
    
    private var _centerViewController   :UIViewController?
    
    private var centerViewController    :UIViewController? {
        get {
            return _centerViewController
        }
        
        set {
            removeCenterGestureRecognizers()
            removeChildViewControllerFromContainer(_centerViewController)
            
            guard let newValue = newValue else {
                return
            }
            
            var origin : CGPoint!
            if let oldCenter = _centerViewController {
                origin = oldCenter.view.frame.origin
            } else {
                origin = CGPointZero
            }
            
            _centerViewController = newValue
            
            if (centerViewController == nil) {
                return
            }
            
            addChildViewController(newValue)
            view.addSubview(newValue.view)
            newValue.view.frame = CGRectMake(origin.x, origin.y, newValue.view.frame.width, newValue.view.frame.height)
            
            newValue.didMoveToParentViewController(self)
            
            if (shadow != nil) {
                shadow.shadowedView = newValue.view
            } else {
                shadow = SideMenuShadow.shadowWithView(newValue.view)
            }
            
            shadow.draw()
            addCenterGestureRecognizers()
        }
    }
    
    private var _leftMenuViewController  : UIViewController?
    
    private var leftMenuViewController  : UIViewController? {
        get {
            return _leftMenuViewController
        }
        
        set {
            removeChildViewControllerFromContainer(leftMenuViewController)
            
            _leftMenuViewController = newValue
            
            guard let leftMenuViewController = leftMenuViewController else {
                return
            }
            addChildViewController(leftMenuViewController)
            if let menuContainerView = menuContainerView {
                if (menuContainerView.superview != nil) {
                    menuContainerView.insertSubview(leftMenuViewController.view, atIndex: 0)
                }
            }
            
            leftMenuViewController.didMoveToParentViewController(self)
            
            if viewHasAppeared {
                setLeftSideMenuFrameToClosedPosition()
            }
        }
    }
    
    
    private var _rightMenuViewController : UIViewController?
        
    private var rightMenuViewController : UIViewController? {
        get {
            return _rightMenuViewController
        }
        
        set {
            removeChildViewControllerFromContainer(_rightMenuViewController)
            
            _rightMenuViewController = newValue
            
            guard let rightMenuViewController = _rightMenuViewController else {
                return
            }
            
            addChildViewController(rightMenuViewController)
            if let menuContainerView = menuContainerView {
                if (menuContainerView.superview != nil) {
                    menuContainerView.insertSubview(rightMenuViewController.view, atIndex: 0)
                }
            }
            
            rightMenuViewController.didMoveToParentViewController(self)
            
            if viewHasAppeared {
                setRightSideMenuFrameToClosedPosition()
            }
        }
    }
    
    private var menuState   : SideMenuState = .Closed
    
    private var panMode     : SideMenuPanMode = .Default
    
    // menu open/close animation duration -- user can pan faster than default duration, max duration sets the limit
    private var menuAnimationDefaultDuration    : CGFloat!
    private var menuAnimationMaxDuration        : CGFloat!
    
    // width of the side menus
    private var menuWidth       : CGFloat!
    private var leftMenuWidth   : CGFloat!
    private var rightMenuWidth  : CGFloat!
    
    // shadow
    private var shadow  : SideMenuShadow!
    
    // menu slide-in animation
    private var menuSlideAnimationEnabled   : Bool = false
    private var menuSlideAnimationFactor    : CGFloat! // higher = less menu movement on animation
    
    private var menuContainerView   : UIView?
    
    private var panGestureOrigin    : CGPoint!
    private var panGestureVelocity  : CGFloat!
    
    private var panDirection    : SideMenuPanDirection = .None
    
    private var viewHasAppeared : Bool = false
    
    //MARK: - Static Inits
    
    static public func containerWithCenterViewController(centerViewController: UIViewController, andLeftMenuViewController leftMenuViewController: UIViewController?, andRightMenuViewController rightMenuViewController: UIViewController?) -> SideMenuContainerViewController {
        
        let controller = SideMenuContainerViewController()
        controller.leftMenuViewController = leftMenuViewController
        controller.centerViewController = centerViewController
        controller.rightMenuViewController = rightMenuViewController
        return controller
    }
    
    //MARK: - Inits
    
    init() {
        super.init(nibName: nil, bundle: nil)
        self.setDefaultSettings()
    }
    
    public required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        
        self.setDefaultSettings()
    }
    
    //MARK: - Setups
    
    public func setDefaultSettings() {
        if (menuContainerView != nil) {
            return
        }
        
        menuContainerView = UIView()
        menuState = .Closed
        menuWidth = 270.0
        leftMenuWidth = 0.0
        rightMenuWidth = 0.0
        menuSlideAnimationFactor = 3.0
        menuAnimationDefaultDuration = 0.2
        menuAnimationMaxDuration = 0.4
        panMode = .Default
        panGestureVelocity = 0.0
        viewHasAppeared = false
    }
    
    public func setupMenuContainerView() {
        guard let menuContainerView = menuContainerView else {
            return
        }
        
        if (menuContainerView.superview != nil) {
            return
        }
        
        menuContainerView.frame = self.view.bounds
        menuContainerView.autoresizingMask = [.FlexibleHeight, .FlexibleWidth]
        
        view.insertSubview(menuContainerView, atIndex: 0)
        
        if let leftMenuViewController = leftMenuViewController where leftMenuViewController.view.superview == nil {
            menuContainerView.addSubview(leftMenuViewController.view)
        }
        
        if let rightMenuViewController = rightMenuViewController where rightMenuViewController.view.superview == nil {
            menuContainerView.addSubview(rightMenuViewController.view)
        }
    }
    
    //MARK: - View Lifecycle
    
    override public func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override public func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        if !viewHasAppeared {
            setupMenuContainerView()
            setLeftSideMenuFrameToClosedPosition()
            setRightSideMenuFrameToClosedPosition()
            addGestureRecognizers()
            
            shadow.draw()
            
            viewHasAppeared = true
        }
    }
    
    public override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        let insets = UIEdgeInsetsMake(topLayoutGuide.length, 0, 0, 0)
        if let leftMenuViewController = leftMenuViewController where leftMenuViewController.automaticallyAdjustsScrollViewInsets {
            if let scrollView = leftMenuViewController.view as? UIScrollView {
                scrollView.contentInset = insets
            }
        }
        if let rightMenuViewController = rightMenuViewController where rightMenuViewController.automaticallyAdjustsScrollViewInsets {
            if let scrollView = rightMenuViewController.view as? UIScrollView {
                scrollView.contentInset = insets
            }
        }
    }
    
    public override func preferredStatusBarStyle() -> UIStatusBarStyle {
        if let centerViewController = centerViewController {
            if let centerViewController = centerViewController as? UINavigationController {
                if let topViewController = centerViewController.topViewController {
                    return topViewController.preferredStatusBarStyle()
                }
            }
            
            return centerViewController.preferredStatusBarStyle()
        }
        return .Default
    }
    
    //MARK: - Rotation
    
    public override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        if let centerViewController = centerViewController {
            if let navigationController = centerViewController as? UINavigationController {
                if let topViewController = navigationController.topViewController {
                    return topViewController.supportedInterfaceOrientations()
                }
            }
            return centerViewController.supportedInterfaceOrientations()
        }
        return super.supportedInterfaceOrientations()
    }
    
    public override func shouldAutorotate() -> Bool {
        if let centerViewController = centerViewController {
            if let navigationController = centerViewController as? UINavigationController {
                if let topViewController = navigationController.topViewController {
                    return topViewController.shouldAutorotate()
                }
            }
            return centerViewController.shouldAutorotate()
        }
        return true
    }
    
    public override func preferredInterfaceOrientationForPresentation() -> UIInterfaceOrientation {
        if let centerViewController = centerViewController {
            if let navigationController = centerViewController as? UINavigationController {
                if let topViewController = navigationController.topViewController {
                    return topViewController.preferredInterfaceOrientationForPresentation()
                }
            }
            return centerViewController.preferredInterfaceOrientationForPresentation()
        }
        return .Portrait
    }
    
    public override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransitionToSize(size, withTransitionCoordinator: coordinator)
        
        shadow.shadowedViewWillRotate()
    }
    
    //MARK: - View Controller Containment
    
    public func removeChildViewControllerFromContainer(childViewController: UIViewController?) {
        guard let childViewController = childViewController else {
            return
        }
        
        childViewController.willMoveToParentViewController(nil)
        childViewController.removeFromParentViewController()
        childViewController.view.removeFromSuperview()
    }
    
    //MARK: - Gesture Recognizer Helpers
    
    public func panGestureRecognizer() -> UIPanGestureRecognizer {
        let recognizer = UIPanGestureRecognizer(target: self, action: #selector(SideMenuContainerViewController.handlePan(_:)))
        
        recognizer.maximumNumberOfTouches = 1
        recognizer.delegate = self
        
        return recognizer
    }
    
    public func centerTapGestureRecognizer() -> UITapGestureRecognizer {
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(SideMenuContainerViewController.centerViewControllerTapped))
        tapRecognizer.delegate = self
        return tapRecognizer
    }
    
    public func addGestureRecognizers() {
        addCenterGestureRecognizers()
        menuContainerView?.addGestureRecognizer(panGestureRecognizer())
    }
    
    public func addCenterGestureRecognizers() {
        if let centerViewController = centerViewController {
            centerViewController.view.addGestureRecognizer(centerTapGestureRecognizer())
            centerViewController.view.addGestureRecognizer(panGestureRecognizer())
        }
    }
    
    public func removeCenterGestureRecognizers() {
        if let centerViewController = centerViewController {
            centerViewController.view.removeGestureRecognizer(centerTapGestureRecognizer())
            centerViewController.view.removeGestureRecognizer(panGestureRecognizer())
        }
    }
    
    //MARK: - Menu State
    
    public func toggleLeftSideMenuCompletion(completion: (() -> Void)?) {
        if (menuState == .LeftMenuOpen) {
            setMenuState(.Closed, completion: completion)
        } else {
            setMenuState(.LeftMenuOpen, completion: completion)
        }
    }
    
    public func toggleRightSideMenuCompletion(completion: (() -> Void)?) {
        if (menuState == .RightMenuOpen) {
            setMenuState(.Closed, completion: completion)
        } else {
            setMenuState(.RightMenuOpen, completion: completion)
        }
    }
    
    public func openLeftSideMenuCompletion(completion: (() -> Void)?) {
        guard let leftMenuViewController = leftMenuViewController else {
            return
        }
        
        menuContainerView?.bringSubviewToFront(leftMenuViewController.view)
        setCenterViewControllerOffset(leftMenuWidth, animated: true, completion:completion)
    }
    
    public func openRightSideMenuCompletion(completion: (() -> Void)?) {
        guard let rightMenuViewController = rightMenuViewController else {
            return
        }
        
        menuContainerView?.bringSubviewToFront(rightMenuViewController.view)
        setCenterViewControllerOffset(-rightMenuWidth, animated: true, completion:completion)
    }
    
    public func closeSideMenuCompletion(completion: (() -> Void)?) {
        setCenterViewControllerOffset(0.0, animated: true, completion: completion)
    }
    
    public func setMenuState(menuState: SideMenuState, completion: (() -> Void)? = nil) {
        let innerCompletion = {
            self.menuState = menuState
            
            self.setUserInteractionStateForCenterViewController()
            let eventType : SideMenuStateEvent = self.menuState == .Closed ? .DidClose : .DidOpen
            self.sendStateEventNotification(eventType)
            
            if let completion = completion {
                completion()
            }
        }
        
        switch (menuState) {
        case .Closed:
            sendStateEventNotification(.WillClose)
            
            closeSideMenuCompletion({
                self.leftMenuViewController?.view.hidden = true
                self.rightMenuViewController?.view.hidden = true
                innerCompletion()
            })
            
        case .LeftMenuOpen:
            if (leftMenuViewController == nil) {
                return
            }
            
            sendStateEventNotification(.WillOpen)
            leftMenuWillShow()
            openLeftSideMenuCompletion(innerCompletion)
            
        case .RightMenuOpen:
            if (rightMenuViewController == nil){
                return
            }
            sendStateEventNotification(.WillOpen)
            rightMenuWillShow()
            openRightSideMenuCompletion(innerCompletion)
        }
    }
    
    // these callbacks are called when the menu will become visible, not neccessarily when they will OPEN
    
    public func leftMenuWillShow() {
        if let leftMenuViewController = leftMenuViewController {
            leftMenuViewController.view.hidden = false
            menuContainerView?.bringSubviewToFront(leftMenuViewController.view)
        }
    }
    
    public func rightMenuWillShow() {
        if let rightMenuViewController = rightMenuViewController {
            rightMenuViewController.view.hidden = false
            menuContainerView?.bringSubviewToFront(rightMenuViewController.view)
        }
    }
    
    //MARK: - State Event Notification
    
    public func sendStateEventNotification(event: SideMenuStateEvent) {
        NotificationUtils.postNotification(SideMenuStateNotificationEvent, withObject: event.rawValue)
    }
    
    //MARK: - Side Menu Positioning
    
    public func setLeftSideMenuFrameToClosedPosition() {
        guard let leftMenuViewController = leftMenuViewController else {
            return
        }
        
        var leftFrame = leftMenuViewController.view.frame
        leftFrame.size.width = leftMenuWidth
        leftFrame.origin.x = menuSlideAnimationEnabled ? -leftFrame.size.width / menuSlideAnimationFactor : 0.0
        leftFrame.origin.y = 0.0
        leftMenuViewController.view.frame = leftFrame
        leftMenuViewController.view.autoresizingMask = [.FlexibleRightMargin, .FlexibleHeight]
    }
    
    public func setRightSideMenuFrameToClosedPosition() {
        guard let rightMenuViewController = rightMenuViewController else {
            return
        }
        
        var rightFrame = rightMenuViewController.view.frame
        rightFrame.size.width = leftMenuWidth
        rightFrame.origin.x = menuSlideAnimationEnabled ? -rightFrame.size.width / menuSlideAnimationFactor : 0.0
        rightFrame.origin.y = 0.0
        rightMenuViewController.view.frame = rightFrame
        rightMenuViewController.view.autoresizingMask = [.FlexibleLeftMargin, .FlexibleHeight]
    }
    
    public func alignLeftMenuControllerWithCenterViewController() {
        guard let leftMenuViewController = leftMenuViewController else {
            return
        }
        
        var leftMenuFrame = leftMenuViewController.view.frame
        leftMenuFrame.size.width = leftMenuWidth
        
        var xOffset: CGFloat = 0
        if let centerViewController = centerViewController {
            xOffset = centerViewController.view.frame.origin.x
        }
        
        let xPositionDivider = menuSlideAnimationEnabled ? self.menuSlideAnimationFactor : 1.0
        leftMenuFrame.origin.x = xOffset / xPositionDivider - leftMenuWidth / xPositionDivider
        
        leftMenuViewController.view.frame = leftMenuFrame
    }
    
    public func alignRightMenuControllerWithCenterViewController() {
        guard let rightMenuViewController = rightMenuViewController else {
            return
        }
        
        guard let menuContainerView = menuContainerView else {
            return
        }
        
        var rightMenuFrame = rightMenuViewController.view.frame
        rightMenuFrame.size.width = rightMenuWidth
        
        var xOffset: CGFloat = 0
        if let centerViewController = centerViewController {
            xOffset = centerViewController.view.frame.origin.x
        }
        
        let xPositionDivider = menuSlideAnimationEnabled ? self.menuSlideAnimationFactor : 1.0
        
        rightMenuFrame.origin.x = menuContainerView.frame.size.width - rightMenuWidth + xOffset / xPositionDivider + rightMenuWidth / xPositionDivider
        
        rightMenuViewController.view.frame = rightMenuFrame
    }
    
    //MARK: - Side Menu Width
    
    public func setMenuWidth(menuWidth: CGFloat, animated: Bool = true) {
        setLeftMenuWidth(menuWidth, animated: animated)
        setRightMenuWidth(menuWidth, animated: animated)
    }
    
    public func setLeftMenuWidth(leftMenuWidth: CGFloat, animated: Bool = true) {
        self.leftMenuWidth = leftMenuWidth
        
        if (menuState != .LeftMenuOpen) {
            setLeftSideMenuFrameToClosedPosition()
            return
        }
        
        let offset = self.leftMenuWidth
        let effects = {
            self.alignLeftMenuControllerWithCenterViewController()
        }
        
        setCenterViewControllerOffset(offset, additionalAnimations: effects, animated: animated, completion: nil)
    }
    
    public func setRightMenuWidth(rightMenuWidth: CGFloat, animated: Bool = true) {
        self.rightMenuWidth = rightMenuWidth
        
        if (menuState != .RightMenuOpen) {
            setRightSideMenuFrameToClosedPosition()
            return
        }
        
        let offset = -self.rightMenuWidth
        let effects = {
            self.alignRightMenuControllerWithCenterViewController()
        }
        
        setCenterViewControllerOffset(offset, additionalAnimations: effects, animated: animated, completion: nil)
    }
    
    //MARK: - Side Menu Pan Mode
    
    public func centerViewControllerPanEnabled() -> Bool {
        return self.panMode == .Default || self.panMode == .CenterViewController
    }
    
    public func sideMenuPanEnabled() -> Bool {
        return self.panMode == .Default || self.panMode == .SideMenu
    }
    
    //MARK: - Setters
    
    public func setShadowRadius(radius: CGFloat) {
        if (shadow != nil) {
            shadow.radius = radius
        }
    }
    
    public func setPanMode(panMode: SideMenuPanMode) {
        self.panMode = panMode
    }
    
    public func setShadow(shadow: SideMenuShadow) {
        self.shadow = shadow
    }
    
    //MARK: - Getters
    
    public func getCenterViewController() -> UIViewController? {
        return centerViewController
    }
    
    public func getLeftMenuViewController() -> UIViewController? {
        return leftMenuViewController
    }
    
    public func getRightMenuViewController() -> UIViewController? {
        return rightMenuViewController
    }
    
    public func getMenuState() -> SideMenuState {
        return menuState
    }
    
    public func getPanMode() -> SideMenuPanMode {
        return panMode
    }
    
    public func getMenuWidth() -> CGFloat {
        return menuWidth
    }
    
    public func getLeftMenuWidth() -> CGFloat {
        return leftMenuWidth
    }
    
    public func getRightMenuWidth() -> CGFloat {
        return rightMenuWidth
    }
    
    public func getShadow() -> SideMenuShadow {
        return shadow
    }
    
    //MARK: - Gesture Recognizer Delegate
    
    public func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldReceiveTouch touch: UITouch) -> Bool {
        if (gestureRecognizer is UITapGestureRecognizer && menuState != .Closed) {
            return true
        }
        
        if (gestureRecognizer is UIPanGestureRecognizer) {
            guard let gestureView = gestureRecognizer.view else {
                return false
            }
            
            if let centerViewController = centerViewController {
                if (gestureView.isEqual(centerViewController.view)) {
                    return self.centerViewControllerPanEnabled()
                }
            }

            if (gestureView.isEqual(menuContainerView)) {
                return sideMenuPanEnabled()
            }
            
            // pan gesture is attached to a custom view
            return true
        }
        
        return false
    }
    
    public func gestureRecognizerShouldBegin(gestureRecognizer: UIGestureRecognizer) -> Bool {
        if let gestureRecognizer = gestureRecognizer as? UIPanGestureRecognizer {
            let velocity = gestureRecognizer.velocityInView(gestureRecognizer.view)
            let isHorizontalPanning = fabs(velocity.x) > fabs(velocity.y)
            return isHorizontalPanning
        }
        
        return true
    }
    
    public func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWithGestureRecognizer otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return false
    }
    
    //MARK: - Gesture Recognizer Callbacks
    
    public func handlePan(recognizer: UIPanGestureRecognizer) {
        
        var origin: CGPoint!
        var view: UIView? = nil
        if let centerViewController = centerViewController {
            view = centerViewController.view
            origin = view!.frame.origin
        } else {
            origin = CGPointZero
        }
        
        if (recognizer.state == .Began) {
            panGestureOrigin = origin
            panDirection = .None
        }
        
        if (panDirection == .None) {
            let translatedPoint = recognizer.translationInView(view)
            if (translatedPoint.x > 0.0) {
                panDirection = .Right
                if (leftMenuViewController != nil && menuState == .Closed) {
                    leftMenuWillShow()
                }
            } else if (translatedPoint.x < 0.0) {
                panDirection = .Left
                if (rightMenuViewController != nil && menuState == .Closed) {
                    rightMenuWillShow()
                }
            }
        }
        
        if ((menuState == .RightMenuOpen && panDirection == .Left) || (menuState == .LeftMenuOpen && panDirection == .Right)) {
            panDirection = .None
            return
        }
        
        if (panDirection == .Left) {
            handleLeftPan(recognizer)
        } else if (panDirection == .Right) {
            handleRightPan(recognizer)
        }
    }
    
    public func handleLeftPan(recognizer: UIPanGestureRecognizer) {
        if (rightMenuViewController == nil && menuState == .Closed) {
            return
        }
        
        var size: CGSize!
        var view: UIView? = nil
        if let centerViewController = centerViewController {
            view = centerViewController.view
            size = view!.frame.size
        } else {
            size = CGSizeZero
        }
        
        var translatedPoint = recognizer.translationInView(view)
        let adjustedOrigin = panGestureOrigin
        translatedPoint = CGPointMake(adjustedOrigin.x + translatedPoint.x, adjustedOrigin.y + translatedPoint.y)
        
        translatedPoint.x = max(translatedPoint.x, -rightMenuWidth)
        translatedPoint.x = min(translatedPoint.x, leftMenuWidth)
        
        if (menuState == .LeftMenuOpen) {
            // don't let the pan go less than 0 if the menu is already open
            translatedPoint.x = max(translatedPoint.x, 0.0)
        } else {
            // we are opening the menu
            translatedPoint.x = min(translatedPoint.x, 0.0)
        }
        
        setCenterViewControllerOffset(translatedPoint.x)
        
        if (recognizer.state == .Ended) {
            let velocity = recognizer.velocityInView(view)
            let finalX = translatedPoint.x + (0.35 * velocity.x)
            let viewWidth = size.width
            
            if (menuState == .Closed) {
                let showMenu = (finalX < -viewWidth/2.0) || (finalX < -rightMenuWidth/2.0)
                if (showMenu) {
                    panGestureVelocity = velocity.x
                    setMenuState(.RightMenuOpen)
                    
                } else {
                    panGestureVelocity = 0.0
                    setCenterViewControllerOffset(0.0, animated: true, completion: nil)
                }
            } else {
                let hideMenu = finalX < adjustedOrigin.x
                if (hideMenu) {
                    panGestureVelocity = velocity.x
                    setMenuState(.Closed)
                    
                } else {
                    panGestureVelocity = 0.0
                    setCenterViewControllerOffset(adjustedOrigin.x, animated: true, completion: nil)
                }
            }
        } else {
            setCenterViewControllerOffset(translatedPoint.x)
        }
    }
    
    public func handleRightPan(recognizer: UIPanGestureRecognizer) {
        if (leftMenuViewController == nil && menuState == .Closed) {
            return
        }
        
        var size: CGSize!
        var view: UIView? = nil
        if let centerViewController = centerViewController {
            view = centerViewController.view
            size = view!.frame.size
        } else {
            size = CGSizeZero
        }
        
        var translatedPoint = recognizer.translationInView(view)
        let adjustedOrigin = panGestureOrigin
        translatedPoint = CGPointMake(adjustedOrigin.x + translatedPoint.x, adjustedOrigin.y + translatedPoint.y)
        
        translatedPoint.x = max(translatedPoint.x, -rightMenuWidth)
        translatedPoint.x = min(translatedPoint.x, leftMenuWidth)
        
        if (menuState == .RightMenuOpen) {
            // menu is already open, the most the user can do is close it in this gesture
            translatedPoint.x = min(translatedPoint.x, 0.0)
        } else {
            // we are opening the menu
            translatedPoint.x = max(translatedPoint.x, 0.0)
        }
        
        if(recognizer.state == .Ended) {
            let velocity = recognizer.velocityInView(view)
            let finalX = translatedPoint.x + (0.35 * velocity.x)
            let viewWidth = size.width
            
            if (menuState == .Closed) {
                let showMenu = (finalX > viewWidth/2.0) || (finalX > leftMenuWidth/2.0)
                if (showMenu) {
                    panGestureVelocity = velocity.x
                    setMenuState(.LeftMenuOpen)
                    
                } else {
                    panGestureVelocity = 0.0
                    setCenterViewControllerOffset(0.0, animated: true, completion: nil)
                }
            } else {
                let hideMenu = (finalX > adjustedOrigin.x)
                if (hideMenu) {
                    panGestureVelocity = velocity.x
                    setMenuState(.Closed)
                    
                } else {
                    panGestureVelocity = 0.0
                    setCenterViewControllerOffset(adjustedOrigin.x, animated: true, completion: nil)
                }
            }
            
            self.panDirection = .None
            
        } else {
            setCenterViewControllerOffset(translatedPoint.x)
        }
    }
    
    public func centerViewControllerTapped(sender: AnyObject) {
        if (menuState != .Closed) {
            setMenuState(.Closed)
        }
    }
    
    public func setUserInteractionStateForCenterViewController() {
        // disable user interaction on the current stack of view controllers if the menu is visible
        if let navigationController = centerViewController?.navigationController {
            let viewControllers = navigationController.viewControllers
            
            for viewController in viewControllers {
                viewController.view.userInteractionEnabled = (menuState == .Closed)
            }
        }
    }
    
    //MARKL: - Center View Controller Movement
    
    public func setCenterViewControllerOffset(offset: CGFloat, additionalAnimations:(() -> Void)? = nil, animated: Bool, completion: (() -> Void)?) {
        let innerCompletion = {
            self.panGestureVelocity = 0.0
            if let completion = completion {
                completion()
            }
        }
        
        guard let centerViewController = centerViewController else {
            innerCompletion()
            return
        }
        
        if (animated) {
            let centerViewControllerXPosition = abs(centerViewController.view.frame.origin.x)
            let duration = Double(animationDurationFromStartPosition(centerViewControllerXPosition, toEndPosition: offset))
            
            UIView.animateWithDuration(duration, animations: {
                self.setCenterViewControllerOffset(offset)
                if let additionalAnimations = additionalAnimations {
                    additionalAnimations()
                }
                }, completion: { (didComplete) in
                    innerCompletion()
            })
        } else {
            setCenterViewControllerOffset(offset)
            if let additionalAnimations = additionalAnimations {
                additionalAnimations()
            }
            innerCompletion()
        }
    }
    
    public func setCenterViewControllerOffset(xOffset: CGFloat) {
        guard let centerViewController = centerViewController else {
            return
        }
        
        var frame = centerViewController.view.frame
        frame.origin.x = xOffset
        
        centerViewController.view.frame = frame
        
        if (!menuSlideAnimationEnabled) {
            return
        }
        
        if (xOffset > 0.0) {
            alignLeftMenuControllerWithCenterViewController()
            setRightSideMenuFrameToClosedPosition()
            
        } else if (xOffset < 0.0){
            alignRightMenuControllerWithCenterViewController()
            setLeftSideMenuFrameToClosedPosition()
            
        } else {
            setLeftSideMenuFrameToClosedPosition()
            setRightSideMenuFrameToClosedPosition()
        }
    }
    
    public func animationDurationFromStartPosition(startPosition: CGFloat, toEndPosition endPosition: CGFloat) -> CGFloat {
        let animationPositionDelta = abs(endPosition - startPosition)
        
        var duration : CGFloat!
        if (abs(panGestureVelocity) > 1.0) {
            // try to continue the animation at the speed the user was swiping
            duration = animationPositionDelta / abs(panGestureVelocity)
            
        } else {
            // no swipe was used, user tapped the bar button item
            let menuWidth = max(leftMenuWidth, rightMenuWidth)
            let animationPercent = (animationPositionDelta == 0) ? 0 : menuWidth / animationPositionDelta
            duration = self.menuAnimationDefaultDuration * animationPercent
        }
        
        return min(duration, menuAnimationMaxDuration)
    }
}
