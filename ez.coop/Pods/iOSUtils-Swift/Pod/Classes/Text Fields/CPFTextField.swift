//
//  CPFTextField.swift
//  Pods
//
//  Created by Livetouch-Mini01 on 05/07/2016.
//
//

import UIKit

public class CPFTextField: UITextField, UITextFieldDelegate {
    
    //MARK: - Variables
    
    private var onReturn            : (() -> Void)?
    private var onEditingChanged    : ((text: String) -> Void)?
    
    //MARK: - Inits
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override public func awakeFromNib() {
        super.awakeFromNib()
        
        delegate = self
    }
    
    //MARK: - Setups
    
    public func setOnReturn(onReturn: (() -> Void)?) {
        self.onReturn = onReturn
    }
    
    public func setOnEditingChanged(onEditingChanged: ((text: String) -> Void)?) {
        self.onEditingChanged = onEditingChanged
        
        addTarget(self, action: #selector(textFieldDidChange(_:)), forControlEvents: UIControlEvents.EditingChanged)
    }
    
    //MARK: - Toolbar
    
    public func setToolbarWithTitle(title: String, andTextColor color: UIColor) {
        let toolbar = UIToolbar()
        toolbar.barStyle = UIBarStyle.Default
        toolbar.translucent = true
        toolbar.tintColor = color
        toolbar.sizeToFit()
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
        let closePicker = UIBarButtonItem(title: title, style: UIBarButtonItemStyle.Plain, target: self, action: #selector(onClickToolbarButton))
        
        toolbar.setItems([spaceButton, closePicker], animated: false)
        toolbar.userInteractionEnabled = true
        
        inputAccessoryView = toolbar
    }
    
    //MARK: - Getters
    
    public func getUnmaskedText() -> String {
        guard let text = text where CPFUtils.isValid(text) else {
            return ""
        }
        
        return CPFUtils.unmask(text)
    }
    
    //MARK: - Actions
    
    func onClickToolbarButton() {
        onReturn?()
    }
    
    //MARK: - Delegate
    
    public func textFieldDidChange(textField: UITextField) {
        guard let text = text else {
            return
        }
        
        onEditingChanged?(text: text)
    }
    
    public func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        guard let tfText = textField.text else {
            return false
        }
        
        let text = string == "" ? (tfText as NSString).stringByReplacingCharactersInRange(range, withString: string) : tfText + string
        
        if (text.length == 11) {
            let maskedText = CPFUtils.mask(text)
            
            textField.text = maskedText
            onEditingChanged?(text: maskedText)
            
            return false
        }
        
        if (CPFUtils.isMasked(text) && text.length > 14) {
            return false
        }
        
        if (text.characters.count >= 10) {
            let unmaskedText = CPFUtils.unmask(text)
            
            textField.text = unmaskedText
            onEditingChanged?(text: unmaskedText)
            
            return false
        }
        
        if (text.length > 0 && !text.isNumber()) {
            return false
        }
        
        return true
    }
}
