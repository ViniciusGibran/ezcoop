//
//  JSON.swift
//  DesafioLTS
//
//  Created by Guilherme Politta on 08/06/16.
//  Copyright © 2016 Livetouch. All rights reserved.
//

import Foundation

import DCKeyValueObjectMapping
import sqlite3

public class JSON {
    
    static public func bindText(stmt: COpaquePointer, atIndex index: Int, withString string: String) {
        sqlite3_bind_text(stmt, Int32(index), string.UTF8String, -1, nil)
    }
    
    static public func getText(stmt: COpaquePointer, atIndex index: Int) -> String {
        let s = sqlite3_column_text(stmt, Int32(index))
        if let string = NSString(bytes: s, length: sizeof(UnsafePointer<UInt8>), encoding: NSUTF8StringEncoding) {
            return string as String
        }
        return ""
    }
    
    static public func setValue(value: String, withName name: String, andObject object: NSObject) {
        object.setValue(value, forKey: name)
    }
    
    //MARK: - Configurations
    
    static private func getParseConfigs(cls: AnyClass) -> [ParseConfig] {
        return getParseConfigs(cls, clsArray: [])
    }
    
    static private func getParseConfigs(cls: AnyClass, clsArray: [String]) -> [ParseConfig] {
        
        var parsedClassesArray = clsArray
        
        let clz: NSObject.Type = cls as! NSObject.Type
        let tipo = clz.init()
        
        let mirror = Mirror(reflecting: tipo)
        let mirrorType = mirror.subjectType as! NSObject.Type
        
        var nameSpace: String?
        if #available(iOS 9, *) {
            let mirrorTypeString = String(reflecting: mirrorType)
            nameSpace = mirrorTypeString.split(".").first
        }
        
        var parseConfigs : [ParseConfig] = []
        
        if (cls is Entity.Type) {
            let entity = cls as! Entity.Type
            let mappings = entity.getAttributeMappings()
            
            let keys = mappings.keys
            for key in keys {
                let mapping = mappings[key]!
                parseConfigs.append(ParseConfig(container: mirrorType.classForCoder(), attribute: key, remoteAttribute: mapping))
            }
        }
        
        if let children = AnyBidirectionalCollection(mirror.children) {
            for item in children {
                
                if ClassUtils.isBasicClass(item.value)  {
                    continue
                }
                
                if let _ = item.value as? NSArray {
                    
                    let itemMirror = Mirror(reflecting: item.value)
                    
                    let itemMirroType = itemMirror.subjectType
                    
                    var arrayClassString = "\(itemMirroType)"
                    arrayClassString = arrayClassString.replaceFirstOccurrence("Array<", withString: "")
                    arrayClassString = arrayClassString.replaceFirstOccurrence(">", withString: "")
                    
                    guard let arrayElementClass : AnyClass = ClassUtils.swiftClassTypeFromString(arrayClassString, namespace: nameSpace) else {
                        continue
                    }
                    
                    if let itemLabel = item.label {
                        let parseConfig = ParseConfig(container: mirrorType.classForCoder(), contained: arrayElementClass, attribute: itemLabel)
                        
                        parseConfigs.append(parseConfig)
                        
                        if (parsedClassesArray.contains(arrayClassString)) {
                            continue
                        }
                        
                        parsedClassesArray.append(arrayClassString)
                        
                        parseConfigs.appendContentsOf(getParseConfigs(arrayElementClass, clsArray: parsedClassesArray))
                    }
                } else {
                    let itemMirror = Mirror(reflecting: item.value)
                    
                    let itemMirroType = itemMirror.subjectType
                    
                    var classString = "\(itemMirroType)"
                    classString = classString.replaceFirstOccurrence("Optional<", withString: "")
                    classString = classString.replaceFirstOccurrence(">", withString: "")
                    
                    
                    if (ClassUtils.isBasicClassString(classString) || parsedClassesArray.contains(classString)) {
                        continue
                    }
                    
                    parsedClassesArray.append(classString)
                    
                    if let elementClass : AnyClass = ClassUtils.swiftClassTypeFromString(classString, namespace: nameSpace) {
                        parseConfigs.appendContentsOf(getParseConfigs(elementClass, clsArray: parsedClassesArray))
                    }
                }
            }
        }
        
        return parseConfigs
    }
    
    //MARK: - Parse Object
    
    static public func fromJson(string string: String?, cls: AnyClass) throws -> AnyObject {
        guard let string = string where string.isNotEmpty else {
            throw NSError(domain: "ParseJsonDomain", code: 1, userInfo: [NSLocalizedDescriptionKey : "Erro ao fazer parse no json."])
        }
        
        guard let data = string.dataUsingEncoding(NSUTF8StringEncoding) else {
            throw NSError(domain: "ParseJsonDomain", code: 1, userInfo: [NSLocalizedDescriptionKey : "Erro ao fazer parse no json."])
        }
        
        return try fromJson(data: data, cls: cls)
    }
    
    static public func fromJson(data data: NSData, cls: AnyClass) throws -> AnyObject {
        return try fromJson(data: data, cls: cls, attributes: getParseConfigs(cls))
    }
    
    static public func fromJson(data data: NSData, cls: AnyClass, attributes :[ParseConfig]) throws -> AnyObject {
        
        let dict = try NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers) as! [NSObject : AnyObject]
        
        let config = DCParserConfiguration()
        
        for attribute in attributes {
            if attribute.isAttributeMapping {
                config.addObjectMapping(attribute.getObjectMapping())
            } else {
                config.addArrayMapper(attribute.getArrayMapping())
            }
        }
        
        let parser = DCKeyValueObjectMapping.mapperForClass(cls, andConfiguration: config)
        
        let obj = parser.parseDictionary(dict)
        
        if let obj = obj {
            return obj
        }
        
        throw NSError(domain: "ParseJsonDomain", code: 1, userInfo: [NSLocalizedDescriptionKey : "Erro ao fazer parse na resposta."])
    }
    
    //MARK: - Parse List
    
    static public func fromJsonList(data: NSData, cls : AnyClass) throws -> [AnyObject] {
        return try fromJsonList(data: data, cls: cls, attributes: getParseConfigs(cls))
    }
    
    static public func fromJsonList(data data: NSData, cls: AnyClass, attributes: [ParseConfig]) throws -> [AnyObject] {
        var listRetorno : [AnyObject] = []
        
        let array = try NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers) as! [AnyObject]
        
        let config = DCParserConfiguration()
        
        for attribute in attributes {
            if attribute.isAttributeMapping {
                config.addObjectMapping(attribute.getObjectMapping())
            } else {
                config.addArrayMapper(attribute.getArrayMapping())
            }
        }
        
        let parser = DCKeyValueObjectMapping.mapperForClass(cls, andConfiguration: config)
        
        for objArray:AnyObject in array {
            let dict = objArray as! [NSObject : AnyObject]
            
            let obj = parser.parseDictionary(dict)
            
            if let obj = obj {
                listRetorno.append(obj)
            }
        }
        
        return listRetorno
    }
}