//
//  BasePickerAdapter.swift
//  Pods
//
//  Created by Livetouch-Mini01 on 28/06/2016.
//
//

import UIKit

public class BasePickerAdapter: NSObject, UIPickerViewDataSource, UIPickerViewDelegate {
    
    ///Não esquecer de guardar uma instância do seu adapter na classe que o utiliza.
    
    //MARK: - Picker View Data Source
    
    public func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    public func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return 0
    }
    
    //MARK: - Picker View Delegate
    
    public func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return nil
    }
    
    public func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {}
}
