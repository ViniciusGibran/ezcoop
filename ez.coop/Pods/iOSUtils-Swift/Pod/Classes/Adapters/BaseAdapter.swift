//
//  BaseAdapter.swift
//  Pods
//
//  Created by Livetouch-Mini01 on 24/06/2016.
//
//

import UIKit

public class BaseAdapter: NSObject, UITableViewDataSource, UITableViewDelegate {
    
    ///Não esquecer de guardar uma instância do seu adapter na classe que o utiliza.
    
    //MARK: - Table View Data Source
    
    public func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    public func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 0
    }
    
    public func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        return UITableViewCell()
    }
    
    //MARK: - Table View Delegate
    
    public func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.0
    }
    
    public func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return nil
    }
    
    public func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {}
}
