//
//  AnimatedTransitioning.swift
//  Pods
//
//  Created by Livetouch-Mini01 on 24/06/2016.
//
//

import UIKit

public class AnimatedTransitioning: NSObject, UIViewControllerAnimatedTransitioning {
    
    //MARK: - Variables
    
    public var isPresenting    : Bool = false
    
    //MARK: - View Controller Animated Transitioning
    
    public func transitionDuration(transitionContext: UIViewControllerContextTransitioning?) -> NSTimeInterval {
        return 0.25
    }
    
    public func animateTransition(transitionContext: UIViewControllerContextTransitioning) {
        
        guard let inView = transitionContext.containerView() else {
            return
        }
        
        guard let toVC = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey) else {
            return
        }
        
        guard let fromVC = transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey) else {
            return
        }
        
        if isPresenting {
            toVC.view.frame = inView.frame
            inView.addSubview(toVC.view)
            
            toVC.view.alpha = 0.0
        }
        
        UIView.animateWithDuration(transitionDuration(transitionContext), animations: {
            if (self.isPresenting) {
                toVC.view.alpha = 1.0
            } else {
                fromVC.view.alpha = 0.0
            }
        }, completion: { (finished) in
            transitionContext.completeTransition(true)
        })
    }
}
