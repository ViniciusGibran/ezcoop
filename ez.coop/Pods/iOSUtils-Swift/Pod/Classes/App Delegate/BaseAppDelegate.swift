//
//  BaseAppDelegate.swift
//  Pods
//
//  Created by Livetouch-Mini01 on 15/06/2016.
//
//

import UIKit

@UIApplicationMain
public class BaseAppDelegate: UIResponder, UIApplicationDelegate {
    
    public var window: UIWindow?
    
    public func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        return true
    }
    
    public func getConfig() -> BaseConfigurationDelegate {
        return BaseBuildConfiguration()
    }
    
    public func createNavigationController(viewController: UIViewController) -> UIWindow {
        let navigationController = UINavigationController(rootViewController: viewController)
        
        let window = UIWindow(frame: UIScreen.mainScreen().bounds)
        window.rootViewController = navigationController
        window.makeKeyAndVisible()
        
        return window
    }
    
    public func createTabBarWithViewControllers(viewControllers: [UIViewController]) -> UITabBarController {
        
        var tabs : [UINavigationController] = []
        
        for viewController in viewControllers {
            let navigationController = UINavigationController(rootViewController: viewController)
            tabs.append(navigationController)
        }
        
        let tabBarController = UITabBarController()
        tabBarController.viewControllers = tabs
        tabBarController.tabBar.barStyle = UIBarStyle.Default
        tabBarController.tabBar.opaque = true
        tabBarController.tabBar.translucent = false
        
        return tabBarController
    }
}