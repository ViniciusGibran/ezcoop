//
//  HttpHelper.swift
//  Pods
//
//  Created by Livetouch-Mini01 on 16/06/2016.
//
//

import Foundation
import Security

public enum CertificateMode {
    case None
    case PublicKey
}

public class HttpHelper: NSObject, NSURLSessionDelegate {
    
    //MARK: - Constants
    
    static public let UTF8  = NSUTF8StringEncoding
    static public let ISO   = NSISOLatin1StringEncoding
    
    //MARK: - Variables
    
    //Basic Properties
    
    private var url         : String!
    private var contentType : String!
    private var timeout     : NSTimeInterval!
    private var encoding    : NSStringEncoding!
    
    private var parameters  : [String: AnyObject]!
    private var json        : String!
    
    private var header      : [String: String]!
    
    private var addDefaultHttpParams    : Bool = false
    
    //Basic Authorization
    
    private var username    : String!
    private var password    : String!
    
    //Certificates
    
    private var certificateMode     : CertificateMode = .None
    
    private var certificate         : NSData?
    private var certificatePassword : String?
    
    private var trustAllSSL         : Bool = false
    
    private var hostDomain          : String?
    
    //Response
    
    private var responseData    : NSData!
    
    private var responseError   : NSError?
    
    //Semaphore
    
    ///Faz com que a requisição fique síncrona.
    private var semaphore       : dispatch_semaphore_t!
    
    //MARK: - Inits
    
    public init(contentType: String = "application/x-www-form-urlencoded", timeout: NSTimeInterval = 60, encoding: NSStringEncoding = NSUTF8StringEncoding) {
        super.init()
        
        self.contentType = contentType
        self.timeout = timeout
        self.encoding = encoding
        
        self.header = [:]
        
        self.parameters = [:]
        self.json = ""
    }
    
    //MARK: - Setters
    
    public func addHeader(key key: String, andValue value: String) {
        header[key] = value
    }
    
    public func setBasicAuth(username username: String, andPassword password: String) {
        self.username = username
        self.password = password
    }
    
    public func setCertificate(certificate: NSData?, withPassword password: String? = nil) {
        if (certificate != nil) {
            self.certificateMode = .PublicKey
        }
        
        self.certificate = certificate
        self.certificatePassword = password
    }
    
    public func setTrustAll(trustAllSSL: Bool) {
        self.trustAllSSL = trustAllSSL
    }
    
    public func setHostDomain(hostDomain: String) {
        self.hostDomain = hostDomain
    }
    
    //MARK: - Getters
    
    public func getData() -> NSData {
        return responseData
    }
    
    public func getJson() -> String {
        if (responseData == nil) {
            return ""
        }
        
        if let json = String(data: responseData, encoding: NSUTF8StringEncoding) {
            return json
        }
        
        return ""
    }
    
    //MARK: - Conversions
    
    private func getBase64BasicAuth(username: String, password: String) throws -> String {
        let basicAuthCredentials = "\(username):\(password)"
        
        if let basicData = basicAuthCredentials.dataUsingEncoding(NSUTF8StringEncoding) {
            let base64EncodedCredential = basicData.base64EncodedStringWithOptions(NSDataBase64EncodingOptions.Encoding64CharacterLineLength)
            let authValue = "Basic \(base64EncodedCredential)"
            return authValue
        }
        
        throw Exception.DomainException(message: "Erro ao formatar Basic Authentication.")
    }
    
    private func getStringFromDictionary(dictionary: [String: AnyObject]) -> String {
        var resource = ""
        
        for (key, value) in dictionary {
            resource = resource.isEmpty ? "" : resource + "&"
            resource += key + "=" + "\(value)"
        }
        
        return resource
    }
    
    private func getDictionaryFromString(body: String) throws -> [String: AnyObject] {
        guard let bodyData = body.dataUsingEncoding(NSUTF8StringEncoding) else {
            throw Exception.DomainException(message: "Erro ao converter o json em dados.")
        }
        
        do {
            guard let dictionary = try NSJSONSerialization.JSONObjectWithData(bodyData, options: .AllowFragments) as? [String:AnyObject] else {
                throw Exception.DomainException(message: "Json inválido! Não foi possível transformar em dicionário.")
            }
            
            return dictionary
            
        } catch {
            throw Exception.DomainException(message: "Erro ao formatar os dados de envio..")
        }
    }
    
    //MARK: - Requests
    
    public func get(url: String, withParameters parameters: [String: String] = [:]) throws {
        updateUrl(url, withParameters: parameters)
        
        try sendHttpRequest("get")
    }
    
    public func delete(url: String, withParameters parameters: [String: String] = [:]) throws {
        updateUrl(url, withParameters: parameters)
        
        try sendHttpRequest("delete")
    }
    
    public func post(url: String, withBody body: String) throws {
        updateUrl(url)
        
        self.json = body
        
        try sendHttpRequest("post")
    }
    
    public func post(url: String, withParameters parameters: [String: AnyObject]) throws {
        updateUrl(url)
        
        self.parameters = parameters
        
        try sendHttpRequest("post")
    }
    
    public func update(url: String, withBody body: String) throws {
        updateUrl(url)
        
        self.json = body
        
        try sendHttpRequest("update")
    }
    
    public func update(url: String, withParameters parameters: [String: AnyObject]) throws {
        updateUrl(url)
        
        self.parameters = parameters
        
        try sendHttpRequest("update")
    }
    
    private func sendHttpRequest(requestMethod: String) throws {
        
        guard let nsurl = NSURL(string: url) else {
            throw Exception.DomainException(message: "URL inválida.")
        }
        
        let request = NSMutableURLRequest(URL: nsurl, cachePolicy: .UseProtocolCachePolicy, timeoutInterval: timeout)
        request.HTTPMethod = requestMethod
        
        if let username = username where username.isNotEmpty {
            if let password = password where password.isNotEmpty {
                let encodedBasicAuth = try getBase64BasicAuth(username, password: password)
                
                request.setValue(encodedBasicAuth, forHTTPHeaderField: "Authorization")
            }
        }
        
        if (header.count > 0) {
            for (key, value) in header {
                request.setValue(value, forHTTPHeaderField: key)
            }
        }
        
        if (requestMethod.equalsIgnoreCase("post") || requestMethod.equalsIgnoreCase("update")) {
            if (self.parameters.isEmpty && StringUtils.isEmpty(json)) {
                throw Exception.DomainException(message: "Requisição http (\(requestMethod.lowercaseString) sem parâmetros).")
            }
            
            if (request.valueForHTTPHeaderField("Content-Type") == nil) {
                request.setValue(contentType, forHTTPHeaderField: "Content-Type")
            }
            
            let params = StringUtils.isNotEmpty(json) ? try getDictionaryFromString(json) : parameters
            
            do {
                let data = try NSJSONSerialization.dataWithJSONObject(params, options: .PrettyPrinted)
                
                request.HTTPBody = data
                
            } catch {
                throw Exception.DomainException(message: "Erro ao formatar os dados de envio.")
            }
        }
        
        semaphore = dispatch_semaphore_create(0)
        
        let configuration = NSURLSessionConfiguration.defaultSessionConfiguration()
        
        let session = NSURLSession(configuration: configuration, delegate: self, delegateQueue: nil)
        
        let dataTask = session.dataTaskWithRequest(request, completionHandler: { (data: NSData?, response: NSURLResponse?, error: NSError?) in
            if let error = error {
                self.responseError = error
                LogUtils.log("Http Error: \(error.localizedDescription)")
            }
            
            if let data = data {
                self.responseData = data
            }
            
            dispatch_semaphore_signal(self.semaphore)
        })
        
        dataTask.resume()
        
        dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER)
        
        if let error = responseError {
            if (error.code == -1022) {
                throw Exception.AppSecurityTransportException
            }
            throw Exception.IOException
        }
        
        if (responseData == nil) {
            throw Exception.IOException
        }
    }
    
    //MARK: - Url Parameters
    
    private func updateUrl(url: String, withParameters parameters: [String: String] = [:]) {
        self.url = url
        
        let queryString = getUrlParams(parameters, withDefaultHttpParams: addDefaultHttpParams)
        if (StringUtils.isNotEmpty(queryString)) {
            self.url = self.url + "?" + queryString
        }
    }
    
    private func getDefaultHttpParameters() -> [String: String] {
        var params : [String: String] = [:]
        
        let so = "iOS"
        let soVersion = "\(DeviceUtils.getVersion())"
        let width = "\(DeviceUtils.getScreenWidth() * DeviceUtils.getScreenScale())"
        let height = "\(DeviceUtils.getScreenHeight() * DeviceUtils.getScreenScale())"
        let deviceName = DeviceUtils.getName()
        let appVersion = AppUtils.getVersion()
        
        params["device.so"] = so
        params["device.so_version"] = soVersion
        params["device.width"] = width
        params["device.height"] = height
        params["device.imei"] = DeviceUtils.getUUID()
        params["device.name"] = deviceName
        params["app.version"] = appVersion
        params["app_version"] = appVersion
        params["app.version_code"] = ""
//        params.put("app.version_code", String.valueOf(AndroidUtils.getAppVersionCode()));
        params["so.version"] = soVersion
        
        return params
    }
    
    private func getUrlParams(parameters: [String: String], withDefaultHttpParams addDefaultHttpParams: Bool) -> String {
        
        var parameters = parameters
        let defaultMap : [String: String] = addDefaultHttpParams ? getDefaultHttpParameters() : [:]
        
        if (!parameters.isEmpty) {
            for (key, value) in defaultMap {
                parameters[key] = value
            }
        } else {
            parameters = defaultMap
        }
        
        let urlParams = getStringFromDictionary(parameters)
        
        return urlParams
    }
    
    //MARK: - Certificate Handlers
    
    public func shoultTrustProctectionSpace(protectionSpace: NSURLProtectionSpace, withCertificate certificate: NSData) -> Bool {
        
//        guard let serverTrust = protectionSpace.serverTrust else {
//            return false
//        }
//        
//        let cfData = certificate as CFData
//        
//        guard let secCert = SecCertificateCreateWithData(nil, cfData) else {
//            return false
//        }
//        
//        let cfArray = CFArrayCreate(nil, secCert as! UnsafeMutablePointer<UnsafePointer<Void>>, 1, nil)
//        SecTrustSetAnchorCertificates(serverTrust, cfArray)
//        
//        var trustResult = SecTrustResultType()
//        
//        guard errSecSuccess == SecTrustEvaluate(serverTrust, &trustResult) else {
//            LogUtils.log("SecTrustEvaluate is not errSecSuccess")
//            return false
//        }
//        
//        if (trustResult == UInt32(kSecTrustResultRecoverableTrustFailure)) {
//            let errDataRef = SecTrustCopyExceptions(serverTrust)
//            SecTrustSetExceptions(serverTrust, errDataRef)
//            
//            SecTrustEvaluate(serverTrust, &trustResult)
//        }
//        
//        return trustResult == UInt32(kSecTrustResultProceed) || trustResult == UInt32(kSecTrustResultUnspecified)
        
        return false
    }
    
    public func loadCertificate(certificate: NSData, withPassword password: String) -> AnyObject {
        
//        NSData* p12data = self.certificate;
//        
//        const void *keys[] = {kSecImportExportPassphrase};
//        const void *values[] = {(__bridge CFStringRef)self.certificatePassword};
//        
//        CFDictionaryRef optionsDictionary = CFDictionaryCreate(NULL, keys, values, 1, NULL, NULL);
//        CFArrayRef p12Items;
//        OSStatus result = SecPKCS12Import((__bridge CFDataRef) p12data, optionsDictionary, &p12Items);
//        
//        if (result == noErr) {
//            CFDictionaryRef identityDict = CFArrayGetValueAtIndex(p12Items, 0);
//            SecIdentityRef identityApp = (SecIdentityRef) CFDictionaryGetValue(identityDict, kSecImportItemIdentity);
//            SecCertificateRef certRef;
//            SecIdentityCopyCertificate(identityApp, &certRef);
//            SecCertificateRef certArray[1] = {certRef};
//            CFArrayRef myCerts = CFArrayCreate(NULL, (void *) certArray, 1, NULL);
//            CFRelease(certRef);
//            
//            NSURLCredential *credential = [NSURLCredential credentialWithIdentity:identityApp certificates:nil persistence:NSURLCredentialPersistenceNone];
//            CFRelease(myCerts);
//            return credential;
//            
//        } else {
//            // Certificate is invalid or password is invalid given the certificate
//            [LTLogUtils log:@"Invalid certificate or password"];
//            return nil;
//        }
        
        return String()
    }
    
    //MARK: - URL Session Delegate
    
    public func URLSession(session: NSURLSession, didBecomeInvalidWithError error: NSError?) {
        if let error = error {
            responseError = error
            LogUtils.log(error.localizedDescription)
            
            dispatch_semaphore_signal(semaphore)
        }
    }
    
    public func URLSession(session: NSURLSession, didReceiveChallenge challenge: NSURLAuthenticationChallenge, completionHandler: (NSURLSessionAuthChallengeDisposition, NSURLCredential?) -> Void) {
        
        if let hostDomain = hostDomain where hostDomain.isNotEmpty {
            if !hostDomain.equalsIgnoreCase(challenge.protectionSpace.host) {
                completionHandler(.RejectProtectionSpace, nil)
            }
        }
        
        if trustAllSSL {
            if let serverTrust = challenge.protectionSpace.serverTrust {
                let credential = NSURLCredential(forTrust: serverTrust)
                completionHandler(.UseCredential, credential)
            }
        } else if let certificate = certificate where certificateMode == .PublicKey {
            if shoultTrustProctectionSpace(challenge.protectionSpace, withCertificate: certificate) {
                if let certificatePassword = certificatePassword {
                    let credential = loadCertificate(certificate, withPassword: certificatePassword)
                    completionHandler(.UseCredential, credential as? NSURLCredential)
                    
                } else {
                    if let serverTrust = challenge.protectionSpace.serverTrust {
                        let credential = NSURLCredential(forTrust: serverTrust)
                        completionHandler(.UseCredential, credential)
                    }
                }
            } else {
                completionHandler(.PerformDefaultHandling, nil)
            }
        } else {
            completionHandler(.CancelAuthenticationChallenge, nil)
        }
    }
}