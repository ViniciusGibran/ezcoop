//
//  UITableView+Livetouch.swift
//  Pods
//
//  Created by Livetouch-Mini01 on 24/06/2016.
//
//

import UIKit

public extension UITableView {
    
    //MARK: - Adapter
    
    public func setAdapter(adapter: BaseAdapter) {
        self.dataSource = adapter
        self.delegate = adapter
    }
    
    //MARK: - Cell Reuse
    
    public func registerCell(nibName: String) {
        self.registerNib(UINib(nibName: nibName, bundle: nil), forCellReuseIdentifier: nibName)
    }
    
    //MARK: - Automatic Height
    
    public func setMinimumHeight(height: CGFloat) {
        self.rowHeight = UITableViewAutomaticDimension
        self.estimatedRowHeight = height
    }
    
    //MARK: - Footer
    
    public func removeFooter() {
        self.tableFooterView = nil
    }
}
