//
//  NSData+Livetouch.swift
//  AprovadorCorporativo
//
//  Created by livetouch PR on 10/6/15.
//  Copyright © 2015 Livetouch Brasil. All rights reserved.
//

import Foundation
import UIKit

public extension NSData {
    
    func toString(encoding: NSStringEncoding = NSUTF8StringEncoding) -> String {
        let string = String(data: self, encoding: encoding)
        return string ?? ""
    }
    
    func toBase64() -> String{
        return self.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))
    }
}