//
//  Array+Livetouch.swift
//  PedidoFacilV2-iOS
//
//  Created by livetouch PR on 7/7/15.
//  Copyright (c) 2015 livetouch. All rights reserved.
//

import Foundation

public extension Array {
    
    mutating func removeObject<U: Equatable>(object: U) {
        var index: Int?
        for (idx, objectToCompare) in self.enumerate() {
            if let to = objectToCompare as? U {
                if object == to {
                    index = idx
                    break
                }
            }
        }
        
        if(index != nil) {
            self.removeAtIndex(index!)
        }
    }
}

public extension Array where Element : AnyObject {
    
    public func toArrayDescription() -> [AnyObject] {
        return Array.toArrayDescription(self)
    }
    
    static public func toArrayDescription(array: [AnyObject]) -> [AnyObject] {
        var arrayRetorno: [AnyObject] = []
        for i in array {
            if (ClassUtils.isBasicClass(i)) {
                arrayRetorno.append(i)
            } else if let array = i as? [AnyObject] {
                arrayRetorno.append(array)
            } else if let object = i as? NSObject {
                arrayRetorno.append(object.toDictionary())
            }
        }
        
        return arrayRetorno
    }
}