//
//  NSObject+Livetouch.swift
//  Pods
//
//  Created by Guilherme Politta on 17/06/16.
//
//

import Foundation
import ObjectiveC

private var queueAssociationKey: UInt8 = 0

public extension NSObject {
    
    private(set) public var queue: NSOperationQueue? {
        get {
            return objc_getAssociatedObject(self, &queueAssociationKey) as? NSOperationQueue
        }
        set(newValue) {
            objc_setAssociatedObject(self, &queueAssociationKey, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
        }
    }
    
    public var maxConcurrentOperationCount : Int {
        get {
            assert(queue != nil, "Você deve implementar o método setupQueue: antes ober o maxConcurrentOperationCount")
            
            return queue!.maxConcurrentOperationCount
        }
        set {
            assert(queue != nil, "Você deve implementar o método setupQueue: antes de mudar o maxConcurrentOperationCount")
            
            queue?.maxConcurrentOperationCount = newValue
        }
    }
    
    public final func setupQueue() {
        self.queue = NSOperationQueue()
        self.queue?.maxConcurrentOperationCount = 5
    }
    
    public func startTask(baseTask: BaseTask, withActivityIndicator activityIndicator: UIActivityIndicatorView? = nil) -> NSBlockOperation {
        
        assert(queue != nil, "Antes de chamar uma task pela primeira vez na classe é necessário chamar o metodo setupQueue:")

        let blockOperation = TaskManager.startTask(baseTask, withActivityIndicator: activityIndicator)
        queue?.addOperation(blockOperation)
        return blockOperation
    }
    
    public func cancelTasks() {
        queue?.cancelAllOperations()
    }
    
    public func cancelTask(operation: NSBlockOperation?) {
        if let operation = operation {
            operation.cancel()
        }
    }
 
    //MARK: - Bus
    
    public func registerNotification(notificationName: String, withSelector selector: Selector) {
        NotificationUtils.registerNotification(notificationName, withSelector: selector, fromObserver: self)
    }
    
    public func unregisterNotification() {
        NotificationUtils.unregisterNotificationFromObserver(self)
    }
    
    public func postNotification(notificationName: String, withObject object: AnyObject? = nil) {
        NotificationUtils.postNotification(notificationName, withObject: object)
    }
    
    public func postNotification(notification: NSNotification) {
        NotificationUtils.postNotification(notification)
    }
    
    //MARK: - Log
    
    public func log(message: String) {
        LogUtils.log(message)
    }
    
    //MARK: - Conversoes
    
    public func toJsonString() throws -> String {
        let result : AnyObject = toDictionary()
        
        let data = try NSJSONSerialization.dataWithJSONObject(result, options: .PrettyPrinted)
        return data.toString()
    }
    
    public func toDictionary() -> [String:AnyObject] {
        return NSObject.toDictionary(self)
    }
    
    public static func toDictionary(object: AnyObject) -> [String:AnyObject] {
        var dict: [String:AnyObject] = [:]
        let otherSelf = Mirror(reflecting: object)
        
        for child in otherSelf.children {
            if let key = child.label {
                if (ClassUtils.isBasicClass(child.value)) {
                    dict[key] = (child.value as! AnyObject)
                } else {
                    if let array = child.value as? [AnyObject] {
                        if (array.count > 0) {
                            dict[key] = toArrayDescription(array)
                        }
                    } else if let object = child.value as? NSObject {
                        dict[key] = object.toDictionary()
                    }
                }
            }
        }
        
        return dict
    }
    
    private static func toArrayDescription(array: NSArray) -> [AnyObject] {
        var arrayRetorno: [AnyObject] = []
        for i in array {
            if (ClassUtils.isBasicClass(i)) {
                arrayRetorno.append(i)
            } else {
                if (i is NSArray) {
                    arrayRetorno.append(i as! NSArray)
                } else if (i is NSObject) {
                    arrayRetorno.append((i as! NSObject).toDictionary())
                }
            }
        }
        
        return arrayRetorno
    }
}