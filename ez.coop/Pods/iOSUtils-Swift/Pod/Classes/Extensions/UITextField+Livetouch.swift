//
//  UITextField+Livetouch.swift
//  NutrabemV2-iOS
//
//  Created by Livetouch Brasil on 9/10/15.
//  Copyright (c) 2015 Marco Velloni. All rights reserved.
//

import Foundation

public extension UITextField {
    
    //MARK: - Padding
    
    public func setLeftPadding(padding: CGFloat) {
        let view = UIView(frame: CGRectMake(0, 0, padding, 0))
        view.backgroundColor = UIColor.clearColor()
        
        self.leftView = view
        self.leftViewMode = UITextFieldViewMode.Always
    }
    
    public func setRightPadding(padding: CGFloat) {
        let view = UIView(frame: CGRectMake(0, 0, padding, 0))
        view.backgroundColor = UIColor.clearColor()
        
        self.rightView = view
        self.rightViewMode = UITextFieldViewMode.Always
    }
    
    //MARK: - Picker View
    
    public func setPickerView(pickerView: UIPickerView) {
        self.inputView = pickerView
    }
    
    public func setToolbar(toolbar: UIToolbar) {
        self.inputAccessoryView = toolbar
    }
}