//
//  String+Livetouch.swift
//  PedidoFacilV2-iOS
//
//  Created by livetouch PR on 7/3/15.
//  Copyright (c) 2015 livetouch PR. All rights reserved.
//

import Foundation

public extension String {
    
    //MARK: - Variables
    
    public var length : Int {
        return self.characters.count
    }
    
    public var isNotEmpty : Bool {
        return !self.isEmpty
    }
    
    public var integerValue : Int {
        return (self as NSString).integerValue
    }
    
    public var floatValue : Float {
        return (self as NSString).floatValue
    }
    
    public var doubleValue : Double {
        return (self as NSString).doubleValue
    }
    
    public var UTF8String: UnsafePointer<Int8> {
        return (self as NSString).UTF8String
    }
    
    //MARK: - Characters
    
    public subscript (i: Int) -> Character {
        return self[self.startIndex.advancedBy(i)]
    }
    
    public subscript (i: Int) -> String {
        return String(self[i] as Character)
    }
    
    public subscript (r: Range<Int>) -> String {
        let start = startIndex.advancedBy(r.startIndex)
        let end = start.advancedBy(r.endIndex - r.startIndex)
        return self[Range(start ..< end)]
    }
    
    public func charAt(i: Int) -> Character {
        return self[self.startIndex.advancedBy(i)]
    }
    
    //MARK: - Contains
    
    public func indexOfChar(searchChar: Character, atStartIndex start: Int) -> Int {
        let substring = String(self.characters.startIndex.advancedBy(start)...self.characters.endIndex)
        let characters = Array(substring.characters)
        
        for (index, character) in characters.enumerate() {
            if (character == searchChar) {
                return index
            }
        }
        
        return -1
    }
    
    public func indexOfString(subString: String, atStartIndex start: Int) -> Int {
        var start = start
        
        if (start < 0) {
            start = 0
        }
        
        let subCount = subString.length
        let count = self.length
        
        if (subCount > 0) {
            if (subCount + start > count) {
                return -1;
            }
            
            let firstChar = subString.charAt(0)
            
            while true {
                let i = indexOfChar(firstChar, atStartIndex: start)
                
                if (i == -1 || subCount + i > count) {
                    return -1
                }
                
                var o1 = i + 1
                var o2 = 1
                
                while (o2 < subCount && charAt(o1) == subString.charAt(o2)) {
                    // Intentionally empty
                    
                    o1 += 1
                    o2 += 1
                }
                
                if (o2 == subCount) {
                    return i
                }
                
                start = i + 1
            }
        }
        
        return start < count ? start : count;
    }
    
    public func containsStringIgnoreCase(find: String) -> Bool {
        return self.uppercaseString.containsString(find.uppercaseString)
    }
    
    public func containsInArray(array: [String]) -> Bool {
        for element in array {
            if (self.containsStringIgnoreCase(element)) {
                return true
            }
        }
        return false
    }
    
    func beginsWith(string: String) -> Bool {
        guard let range = rangeOfString(string, options:[.AnchoredSearch, .CaseInsensitiveSearch]) else {
            return false
        }
        
        return range.startIndex == startIndex
    }
    
    //MARK: - Equals
    
    public func equalsIgnoreCase(string: String) -> Bool {
        return self.uppercaseString == string.uppercaseString
    }
    
    //MARK: - Replace
    
    public func replace(oldString: String, withString newString: String) -> String {
        return self.stringByReplacingOccurrencesOfString(oldString, withString: newString)
    }
    
    public func replaceFirstOccurrence(target: String, withString newString: String) -> String {
        if let range = self.rangeOfString(target) {
            return self.stringByReplacingCharactersInRange(range, withString: newString)
        }
        return self
    }
    
    public func replaceInRange(range: NSRange, withString newString: String) -> String {
        return (self as NSString).stringByReplacingCharactersInRange(range, withString: newString)
    }
    
    //MARK: - Substring
    
    public func substringFromIndex(index:Int) -> String {
        return self.substringFromIndex(self.startIndex.advancedBy(index))
    }
    
    public func substringToIndex(index: Int) -> String {
        return self.substringToIndex(self.startIndex.advancedBy(index))
    }
    
    public func substringFromIndex(startIndex: Int, toIndex endIndex: Int) -> String {
        let length = self.length
        if (endIndex >= length || (endIndex - startIndex) >= length) {
            return ""
        }
        return self.substringWithRange(self.startIndex.advancedBy(startIndex)...self.startIndex.advancedBy(endIndex))
    }
    
    //MARK: - Insert
    
    public func insertString(string: String, atIndex index: Int) -> String {
        let prefix = self.characters.prefix(index)
        let suffix = self.characters.suffix(self.characters.count - index)
        return  String(prefix) + string + String(suffix)
    }
    
    //MARK: - Path Component
    
    public func stringByAppendingPathComponent(path: String) -> String {
        return (self as NSString).stringByAppendingPathComponent(path)
    }
    
    //MARK: - Trim
    
    public func trim() -> String {
        return self.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
    }
    
    public func removeExcessiveSpaces() -> String {
        let components = self.componentsSeparatedByCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        let filtered = components.filter({!$0.isEmpty})
        return filtered.joinWithSeparator(" ")
    }
    
    //MARK: - Split
    
    public func split(separator: String) -> [String] {
        return self.componentsSeparatedByString(separator)
    }
    
    //MARK: - Number
    
    public func isNumber() -> Bool {
        if (self != "") {
            let notDigits = NSCharacterSet.decimalDigitCharacterSet().invertedSet
            if (self.rangeOfCharacterFromSet(notDigits) == nil) {
                return true
            }
        }
        return false
    }
    
    //MARK: - Size
    
    public func sizeWithAttributes(attrs: [String: AnyObject]) -> CGSize {
        return (self as NSString).sizeWithAttributes(attrs)
    }
    
    //MARK: - Encoding
    
    func stringByAddingPercentEncodingForRFC3986() -> String? {
        let unreserved = "-._~/?"
        let allowed = NSMutableCharacterSet.alphanumericCharacterSet()
        allowed.addCharactersInString(unreserved)
        return stringByAddingPercentEncodingWithAllowedCharacters(allowed)
    }
    
    //MARK: - Conversoes
    
    public func toJsonDictionary() throws -> [NSObject : AnyObject] {
        guard let data = self.dataUsingEncoding(NSUTF8StringEncoding) else {
            throw NSError(domain: "StringDomain", code: 1, userInfo: [NSLocalizedDescriptionKey : "Erro ao transformar string em dicionario"])
        }
        
        guard let jsonObject = try NSJSONSerialization.JSONObjectWithData(data, options: .AllowFragments) as? [NSObject : AnyObject] else {
            
            throw NSError(domain: "StringDomain", code: 1, userInfo: [NSLocalizedDescriptionKey : "Erro ao transformar string em dicionario"])
        }
        
        return jsonObject
    }
}