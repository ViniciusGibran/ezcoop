//
//  UIImageView+Livetouch.swift
//  HelloLibIOS
//
//  Created by Livetouch on 06/07/16.
//  Copyright © 2016 Livetouch. All rights reserved.
//

import UIKit
import Kingfisher

public extension UIImageView {

    public func setImageWithURL(URL: String,
                                   placeholderImage: Image? = nil,
                                   optionsInfo: KingfisherOptionsInfo? = nil,
                                   progressBlock: DownloadProgressBlock? = nil,
                                   completionHandler: CompletionHandler? = nil) -> RetrieveImageTask? {
    
        if let url = NSURL(string: URL) {
            return kf_setImageWithURL(url, placeholderImage: placeholderImage, optionsInfo: optionsInfo, progressBlock: progressBlock, completionHandler: completionHandler)
        }
        
        LogUtils.log("<UIImageView.setImageWithURL> URL não encontrada, erro ao carregar a imagem")
        
        return nil
    }
    
}