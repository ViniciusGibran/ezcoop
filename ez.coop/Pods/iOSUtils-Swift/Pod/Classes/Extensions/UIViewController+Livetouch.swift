//
//  UIViewController+Livetouch.swift
//  Pods
//
//  Created by Livetouch-Mini01 on 17/06/2016.
//
//

import Foundation

private var menuContainerAssociationKey: UInt8 = 1

public extension UIViewController {
    
    //MARK: - Menu
    
    private(set) public var menuContainerViewController: SideMenuContainerViewController? {
        get {
            var containerView : UIViewController? = self
            while (!(containerView is SideMenuContainerViewController) && containerView != nil) {
                containerView = containerView?.parentViewController
                if (containerView == nil) {
                    containerView = containerView?.splitViewController
                }
            }
            return containerView as? SideMenuContainerViewController
        }
        
        set(newValue) {
            objc_setAssociatedObject(self, &menuContainerAssociationKey, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
        }
    }
    
    //MARK: - Navigation Controller
    
    public func pushViewController(viewController: UIViewController, animated: Bool = true) {
        navigationController?.pushViewController(viewController, animated: animated)
    }
    
    public func popViewController(animated: Bool = true) {
        navigationController?.popViewControllerAnimated(animated)
    }
    
    //MARK: - Open View Controller
    
    func openViewController(viewController: UIViewController, animated: Bool = true) {
        guard let navigationController = menuContainerViewController?.getCenterViewController() as? UINavigationController else {
            return
        }
        
        navigationController.viewControllers.removeAll()
        navigationController.setViewControllers([viewController], animated: animated)
        menuContainerViewController?.setMenuState(.Closed)
    }
}