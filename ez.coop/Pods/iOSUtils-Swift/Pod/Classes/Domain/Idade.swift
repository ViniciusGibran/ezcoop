//
//  Idade.swift
//  Pods
//
//  Created by Livetouch-Mini01 on 04/07/2016.
//
//

import UIKit

public class Idade: NSObject {
    
    //MARK: - Variables
    
    private var days    : Int
    private var months  : Int
    private var years   : Int
    
    //MARK: - Inits
    
    public init(days: Int, months: Int, years: Int) {
        self.days = days
        self.months = months
        self.years = years
    }
    
    //MARK: - Getters
    
    public func getDays() -> Int {
        return days
    }
    
    public func getMonths() -> Int {
        return months
    }
    
    public func getYears() -> Int {
        return years
    }
    
    //MARK: - Helpers
    
    public func toString() -> String {
        return "\(years) Years, \(months) Months, \(days) Days"
    }
}
