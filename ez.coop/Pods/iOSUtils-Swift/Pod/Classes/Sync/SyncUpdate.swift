//
//  SyncUpdate.swift
//  Pods
//
//  Created by Livetouch-Mini01 on 12/04/2016.
//
//

import UIKit

public class SyncUpdate: NSObject {
    
    //MARK: - Variables
    
    public var hasUpdate    : Bool
    
    public var appVersion   : Int
    public var serverVersion: Int
    
    //MARK: - Inits
    
    override public init() {
        hasUpdate = false
        appVersion = -1
        serverVersion = -1
    }
    
    //MARK: Class Methods
    
    public static func No() -> SyncUpdate {
        let syncUpdate = SyncUpdate()
        syncUpdate.hasUpdate = false
        
        return syncUpdate
    }
    
    public static func Yes(appVersion appVersion: Int, serverVersion: Int) -> SyncUpdate {
        let syncUpdate = SyncUpdate()
        syncUpdate.hasUpdate = true
        syncUpdate.appVersion = appVersion
        syncUpdate.serverVersion = serverVersion
        
        return syncUpdate
    }
}
