//
//  SyncMode.swift
//  Pods
//
//  Created by Livetouch-Mini01 on 08/04/2016.
//
//

import Foundation

public enum SyncMode {
    case ONLINE_FIRST
    case OFFLINE_FIRST
    case VERSION
}