//
//  SyncHelper.swift
//  Pods
//
//  Created by Livetouch-Mini01 on 08/04/2016.
//
//

import Foundation

public protocol SyncHelper {
    
    associatedtype T
    
    func hasUpdate() throws -> SyncUpdate
    
    func setUpdated(update: SyncUpdate) throws
    
    func getDomainClass() -> AnyClass
    
    func getListFromWS(params: [String: AnyObject]) throws -> [T]
    
    func getListFromDB(params: [String: AnyObject]) throws -> [T]
    
    func save(list: [T], params: [String: AnyObject])
    
    func getSyncMode() -> SyncMode
    
    func deleteAllBeforeRefresh(params: [String: AnyObject])
    
    func postDeleteWs(list: [T], params: [String: AnyObject]) throws -> [Int]
    
    func postSaveWs(list: [T], params: [String: AnyObject]) throws -> [Int]
}