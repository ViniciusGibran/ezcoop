//
//  CameraUtils.swift
//  Pods
//
//  Created by Guilherme Politta on 5/07/16.
//
//

import UIKit
import Foundation
import CoreLocation
import MobileCoreServices

public protocol CameraUtilsDelegate {
    func userDidTakePhoto(photo: UIImage)
    func userDidCancel()
    func userDidSelectImage(image: UIImage)
}

public class CameraUtils: NSObject, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    //MARK: - Public Variables
    
    var delegate: CameraUtilsDelegate?
    
    var imagePickerController: UIImagePickerController?
    
    //MARK: - Private Variables
    
    static var openCamera   : CameraUtils?
    
    //MARK: - UIImagePickerControllerDelegate
    
    public func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        let mediaType = info.getStringWithKey(UIImagePickerControllerMediaType)
        
        if (mediaType == kUTTypeImage as String) {
            guard let image = info[UIImagePickerControllerOriginalImage] as? UIImage else {
                AlertUtils.alert("Ocorreu um erro ao obter a imagem")
                return
            }
            
            if (picker.sourceType == .Camera) {
                self.delegate?.userDidTakePhoto(image)
            } else {
                self.delegate?.userDidSelectImage(image)
            }
            
        } else {
            AlertUtils.alert("Somente imagens são aceitas")
        }
        
        CameraUtils.openCamera = nil
        picker.dismissViewControllerAnimated(true, completion: nil)
    }
    
    public func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        self.delegate?.userDidCancel()
        
        CameraUtils.openCamera = nil
        picker.dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    //MARK: - Camera
    
    /**
     *  Verifies wheter the device's camera is available or not.
     *
     *  @return YES if the device's camera is available and NO otherwise.
     *
     */
    public static func isCameraAvailable() -> Bool {
        return UIImagePickerController.isSourceTypeAvailable(.Camera)
    }
    
    public static func getCamera<T where T: UIImagePickerControllerDelegate, T: UINavigationControllerDelegate>(delegate: T) -> UIImagePickerController? {
        
        if (!isCameraAvailable()) {
            return nil
        }
        
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = delegate
        imagePicker.sourceType = .Camera
        imagePicker.cameraCaptureMode = .Photo
        imagePicker.allowsEditing = false
        
        return imagePicker
    }
    
    public static func openCameraWithDelegate(delegate: CameraUtilsDelegate, presentInViewController presentIn: UIViewController) -> CameraUtils? {
        let cameraUtils = CameraUtils()
        cameraUtils.delegate = delegate
        
        if let ipc = CameraUtils.getCamera(cameraUtils) {
            presentIn.presentViewController(ipc, animated: true, completion: nil)
        } else {
            return nil
        }
        
        CameraUtils.openCamera = cameraUtils
        
        return cameraUtils
    }
    
    //MARK: - Photo Library
    
    /**
     *  Verifies wheter the device's photo library is available or not.
     *
     *  @return YES if the device's photo library is available and NO otherwise.
     *
     */
    public static func isPhotoLibraryAvailable() -> Bool {
        return UIImagePickerController.isSourceTypeAvailable(.PhotoLibrary)
    }
    
    public static func getPhotoLibrary<T where T: UIImagePickerControllerDelegate, T: UINavigationControllerDelegate>(delegate: T) -> UIImagePickerController? {
        if (!isPhotoLibraryAvailable()) {
            return nil
        }
        
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = delegate
        imagePicker.sourceType = .PhotoLibrary
        imagePicker.allowsEditing = false
        
        return imagePicker
        
    }
    
    public static func openPhotoLibraryWithDelegate(delegate: CameraUtilsDelegate, presentInViewController presentIn: UIViewController) -> CameraUtils? {
        let cameraUtils = CameraUtils()
        cameraUtils.delegate = delegate
        
        if let ipc = CameraUtils.getPhotoLibrary(cameraUtils) {
            presentIn.presentViewController(ipc, animated: true, completion: nil)
        } else {
            return nil
        }
        
        CameraUtils.openCamera = cameraUtils
        
        return cameraUtils
        
    }
}