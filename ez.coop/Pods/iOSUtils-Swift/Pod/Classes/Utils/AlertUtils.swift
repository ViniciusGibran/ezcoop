//
//  AlertUtils.swift
//  Pods
//
//  Created by Livetouch-Mini01 on 15/06/2016.
//
//

import UIKit

public class AlertUtils {
    
    static public func alert(title: String? = nil, message: String? = nil, cancelText: String? = nil, okText: String? = nil, onViewController viewController: UIViewController? = nil, withHandler handler: ((action: UIAlertAction) -> Void)? = nil) {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        
        if let cancelText = cancelText {
            alertController.addAction(UIAlertAction(title: cancelText, style: UIAlertActionStyle.Cancel, handler: handler))
        }
        
        if let okText = okText {
            alertController.addAction(UIAlertAction(title: okText, style: UIAlertActionStyle.Default, handler: handler))
        }
        
        if (alertController.actions.isEmpty) {
            alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Cancel, handler: handler))
        }
        
        
        if let viewController = viewController {
            viewController.presentViewController(alertController, animated: true, completion: nil)
        } else {
            alertController.show()
        }
    }
    
    static public func actionSheet(title: String, withMessage message: String? = nil, andActions actions: [UIAlertAction], onViewController viewController: UIViewController? = nil) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.ActionSheet)
        for action in actions {
            alertController.addAction(action)
        }
        
        if let viewController = viewController {
            viewController.presentViewController(alertController, animated: true, completion: nil)
        } else {
            alertController.show()
        }
    }
}