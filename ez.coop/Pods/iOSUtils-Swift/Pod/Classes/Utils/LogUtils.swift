//
//  LogUtils.swift
//  Pods
//
//  Created by Livetouch-Mini01 on 15/06/2016.
//
//

import UIKit

public enum LogTag {
    case Log
    case Debug
    case Error
}

public class LogUtils: NSObject {
    
    static public func log(message: String) {
        if message.isEmpty {
            return
        }
        
        print("\(message)\n")
    }
    
    static public func debug(message: String) {
        if message.isEmpty {
            return
        }
        
        NSLog("Debug: %@", message)
    }
    
    static public func sql(message: String) {
        log("+------SQL------+\n\(message)\n+---------------+")
    }
    
    static public func sqlError(message: String) {
        log("\nSQL Error:\n\(message)\n")
    }
}