//
//  NavigationControllerUtils.swift
//  Pods
//
//  Created by Livetouch-Mini01 on 06/07/2016.
//
//

import UIKit

public class NavigationControllerUtils: NSObject {
    
    //MARK: - Pop Gesture Recognizer
    
    static public func enableInteractivePopGesture(viewController: UIViewController?) {
        guard let navigationController = viewController?.navigationController else {
            return
        }
        
        if let interactivePopGestureRecognizer = navigationController.interactivePopGestureRecognizer {
            interactivePopGestureRecognizer.enabled = true
        }
    }
    
    static public func disableInteractivePopGesture(viewController: UIViewController?) {
        guard let navigationController = viewController?.navigationController else {
            return
        }
        
        if let interactivePopGestureRecognizer = navigationController.interactivePopGestureRecognizer {
            interactivePopGestureRecognizer.enabled = false
        }
    }
}
