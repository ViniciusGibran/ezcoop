//
//  OrientationUtils.swift
//  Pods
//
//  Created by Livetouch BR on 6/23/16.
//
//

import UIKit

public class OrientationUtils: NSObject {

    static public func getOrientation() -> UIInterfaceOrientation{
        return UIApplication.sharedApplication().statusBarOrientation
    }
    
    static public func isPortrait() -> Bool{
        let orientation = self.getOrientation()
        return orientation == UIInterfaceOrientation.Portrait
    }
    
    static public func isUpsideDown() -> Bool{
        let orientation = self.getOrientation()
        return orientation == UIInterfaceOrientation.PortraitUpsideDown
    }
    
    static public func isLandscape() -> Bool{
        let orientation = self.getOrientation()
        return orientation == UIInterfaceOrientation.LandscapeLeft || orientation == UIInterfaceOrientation.LandscapeRight
    }
}
