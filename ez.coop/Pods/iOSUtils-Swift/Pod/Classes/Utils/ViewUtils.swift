//
//  ViewUtils.swift
//  Pods
//
//  Created by Livetouch-Mini01 on 06/07/2016.
//
//

import UIKit

public class ViewUtils: NSObject {
    
    static public func shine(view: UIView?) {
        guard let view = view else {
            return
        }
        
        CATransaction.begin()
        
        let duration = 1.0
        
        view.layer.shadowColor = UIColor.whiteColor().CGColor
        view.layer.shadowOffset = CGSizeZero
        view.layer.masksToBounds = false
        
        let shadowRadius = CABasicAnimation(keyPath: "shadowRadius")
        shadowRadius.fromValue = NSNumber(float: 0.0)
        shadowRadius.toValue = NSNumber(float: 6.0)
        shadowRadius.duration = duration
        shadowRadius.autoreverses = true
        
        let shadowOpacity = CABasicAnimation(keyPath: "shadowOpacity")
        shadowOpacity.fromValue = NSNumber(float: 0.6)
        shadowOpacity.toValue = NSNumber(float: 1.0)
        shadowOpacity.duration = duration
        shadowOpacity.autoreverses = true
        
        view.layer.addAnimation(shadowRadius, forKey: "shadowRadius")
        view.layer.addAnimation(shadowOpacity, forKey: "shadowOpacity")
        
        CATransaction.commit()
    }
    
    static public func setGradientEffect(view: UIView?, withTopColor topColor: UIColor, andBottomColor bottomColor: UIColor) {
        guard let view = view else {
            return
        }
        
        let theViewGradient = CAGradientLayer()
        theViewGradient.colors = [topColor.CGColor, bottomColor.CGColor]
        theViewGradient.frame = view.bounds
        
        view.layer.insertSublayer(theViewGradient, atIndex: 0)
    }
}
