//
//  StringUtils.swift
//  Pods
//
//  Created by Livetouch on 30/06/16.
//
//

import UIKit

public class StringUtils: NSObject {
    
    /**
     * The maximum size to which the padding constant(s) can expand.
     */
    static private let PAD_LIMIT : Int = 8192
    
    //MARK: - Empty
    
    static public func isEmpty(str:String?) -> Bool {
        guard let str = str where !str.equalsIgnoreCase("nil") else {
            return true
        }
        
        return str.isEmpty
    }
    
    static public func isNotEmpty(str:String?) -> Bool {
        return !isEmpty(str)
    }
    
    //MARK: - Split
    
    static public func split(retorno: String?, withSeparator separator: String) -> [String] {
        guard let retorno = retorno else {
            return []
        }
        
        return retorno.split(separator)
    }
    
    //MARK: - Padding
    
    static public func leftPad(str: String?, toSize size: Int, withCharacter padChar: Character) throws -> String {
        guard let str = str else {
            return ""
        }
        
        let pads = size - str.length
        if (pads < 1) {
            return str
        }
        
        if (pads > PAD_LIMIT) {
            return try leftPad(str, toSize: size, withString: String(padChar))
        }
        
        return padding(pads, withCharacter: padChar) + str
    }
    
    static public func leftPad(str: String?, toSize size: Int, withString padStr: String) throws -> String {
        guard let str = str else {
            return ""
        }
        
        if (str.length > size) {
            throw Exception.RunTimeException(message: "Texto [\(str)] excedeu o tamanho: \(size)")
        }
        
        var padStr = padStr
        
        if (isEmpty(padStr)) {
            padStr = " "
        }
        
        let padLen = padStr.length
        let strLen = str.length
        let pads = size - strLen
        
        if (pads < 1) {
            return str
        }
        
        if (padLen == 1 && pads <= PAD_LIMIT) {
            return try leftPad(str, toSize: size, withCharacter: padStr.charAt(0))
        }
        
        if (pads == padLen) {
            return padStr + str
            
        } else if (pads < padLen) {
            return padStr.substringFromIndex(0, toIndex: pads) + str
            
        } else {
            var padding : [String] = []
            let padChars = padStr.characters.map { String($0) }
            
            for i in 0 ..< pads {
                padding[i] = padChars[i % padLen]
            }
            
            return padding.joinWithSeparator("") + str
        }
    }
    
    static public func rightPad(str: String?, toSize size: Int, withCharacter padChar: Character) throws -> String {
        guard let str = str else {
            return ""
        }
        
        let pads = size - str.length
        if (pads < 1) {
            return str
        }
        
        if (pads > PAD_LIMIT) {
            return try rightPad(str, toSize: size, withString: String(padChar))
        }
        
        return str + padding(pads, withCharacter: padChar)
    }
    
    static public func rightPad(str: String?, toSize size: Int, withString padStr: String) throws -> String {
        guard let str = str else {
            return ""
        }
        
        if (str.length > size) {
            throw Exception.RunTimeException(message: "Texto [\(str)] excedeu o tamanho: \(size)")
        }
        
        var padStr = padStr
        
        if (isEmpty(padStr)) {
            padStr = " "
        }
        
        let padLen = padStr.length
        let strLen = str.length
        let pads = size - strLen
        
        if (pads < 1) {
            return str
        }
        
        if (padLen == 1 && pads <= PAD_LIMIT) {
            return try rightPad(str, toSize: size, withCharacter: padStr.charAt(0))
        }
        
        if (pads == padLen) {
            return str + padStr
            
        } else if (pads < padLen) {
            return str + padStr.substringFromIndex(0, toIndex: pads)
            
        } else {
            var padding : [String] = []
            let padChars = padStr.characters.map { String($0) }
            
            for i in 0 ..< pads {
                padding[i] = padChars[i % padLen]
            }
            
            return str + padding.joinWithSeparator("")
        }
    }
    
    //TODO: Testar.
    static private func padding(repeatNumber: Int, withCharacter char: Character) -> String {
        var pad = String(char)
        
        while (pad.length < repeatNumber) {
            pad = pad + pad
        }
        
        return pad.substringToIndex(repeatNumber)
    }
    
    //MARK: - Types
    
    static private func getIntegerFromString(s: String) -> NSNumber? {
        let formatter = NSNumberFormatter()
        return formatter.numberFromString(s)?.integerValue
    }
    
    static public func isInteger(s: String?) -> Bool {
        if isEmpty(s) {
            return false
        }
        
        guard let s = s else {
            return false
        }
        
        if let _ = getIntegerFromString(s) {
            return true
        }
        
        return false
    }
    
    static private func getDoubleFromString(s: String) -> NSNumber? {
        let formatter = NSNumberFormatter()
        return formatter.numberFromString(s)?.doubleValue
    }
    
    static public func isDouble(s: String?) -> Bool {
        if isEmpty(s) {
            return false
        }
        
        guard let s = s else {
            return false
        }
        
        if let _ = getDoubleFromString(s) {
            return true
        }
        
        return false
    }
    
    //MARK: - Equals
    
    static public func equals(s1: String?, withString s2: String?) -> Bool {
        guard let s1 = s1 where isNotEmpty(s1) else {
            return false
        }
        
        guard let s2 = s2 where isNotEmpty(s2) else {
            return false
        }
        
        return s1 == s2
    }
    
    static public func equalsAny(s: String?, withArray strings: [String]?) -> Bool {
        guard let s = s where isNotEmpty(s) else {
            return false
        }
        
        guard let strings = strings where strings.count > 0 else {
            return false
        }
        
        for s2 in strings {
            let ok = equals(s, withString: s2)
            if ok {
                return true
            }
        }
        
        return false
    }
    
    static public func equalsIgnoreCase(s1: String?, withString s2: String?) -> Bool {
        guard let s1 = s1 where isNotEmpty(s1) else {
            return false
        }
        
        guard let s2 = s2 where isNotEmpty(s2) else {
            return false
        }
        
        return s1.equalsIgnoreCase(s2)
    }
    
    static public func equalsIgnoreCaseAny(s: String?, withArray strings: [String]?) -> Bool {
        guard let s = s where isNotEmpty(s) else {
            return false
        }
        
        guard let strings = strings where strings.count > 0 else {
            return false
        }
        
        for s2 in strings {
            let ok = equalsIgnoreCase(s, withString: s2)
            if ok {
                return true
            }
        }
        
        return false
    }
    
    static public func notEquals(s1: String?, withString s2: String) -> Bool {
        return !equals(s1, withString: s2)
    }
    
    //MARK: - Types
    
    static public func isLetters(str: String?) -> Bool {
        guard let str = str where StringUtils.isNotEmpty(str) else {
            return false
        }
        
        let notLetters = NSCharacterSet.lowercaseLetterCharacterSet().invertedSet
        if let _ = str.lowercaseString.rangeOfCharacterFromSet(notLetters) {
            return false
        }
        
        return true
    }
    
    static public func isNotLetters(str: String?) -> Bool {
        return !isLetters(str)
    }
    
    static public func isDigits(str: String?) -> Bool {
        guard let str = str where StringUtils.isNotEmpty(str) else {
            return false
        }
        
        let notDigits = NSCharacterSet.decimalDigitCharacterSet().invertedSet
        if let _ = str.rangeOfCharacterFromSet(notDigits) {
            return false
        }
        
        return true
    }
    
    static public func isNotDigits(str: String?) -> Bool {
        return !isDigits(str)
    }
    
    static public func isAlphaNumeric(str: String?) -> Bool {
        guard let str = str where StringUtils.isNotEmpty(str) else {
            return false
        }
        
        let notAlphaNumeric = NSCharacterSet.alphanumericCharacterSet().invertedSet
        if let _ = str.rangeOfCharacterFromSet(notAlphaNumeric) {
            return false
        }
        
        return true
    }
    
    static public func isNotAlphaNumeric(str: String?) -> Bool {
        return !isAlphaNumeric(str)
    }
    
    //MARK: - Cases
    
    static public func toUpperCase(str: String?) -> String {
        return str != nil ? str!.uppercaseString : ""
    }
    
    static public func toLowerCase(str: String?) -> String {
        return str != nil ? str!.lowercaseString : ""
    }
    
    static public func capitalize(str: String?) -> String {
        guard let str = str where isNotEmpty(str) else {
            return ""
        }
        
        return String(str.charAt(0)).uppercaseString + str.substringFromIndex(1)
    }
    
    static public func capitalizeAll(str: String?) -> String {
        return str != nil ? str!.capitalizedString : ""
    }
    
    //MARK: - Trim
    
    static public func trim(str: String?) -> String? {
        return str?.trim()
    }
    
    static public func trimAll(str: String?) -> String? {
        return str?.removeExcessiveSpaces()
    }
    
    //MARK: - Contains
    
    static public func indexOfAny(str: String?, fromArray searchChars: [Character]?) -> Int {
        guard let str = str where isNotEmpty(str) else {
            return -1
        }
        
        guard let searchChars = searchChars where searchChars.count > 0 else {
            return -1
        }
        
        for i in 0 ..< str.length {
            let ch = str.charAt(i)
            
            for j in 0 ..< searchChars.count {
                if (searchChars[j] == ch) {
                    return i
                }
            }
        }
        
        return -1
    }
    
    static public func contains(str: String?, fromQuery query: String?) -> Bool {
        guard let str = str where isNotEmpty(str) else {
            return false
        }
        
        guard let query = query where isNotEmpty(query) else {
            return false
        }
        
        return str.containsString(query)
    }
    
    static public func containsAny(str: String?, fromCharacters searchChars: [Character]?) -> Bool {
        guard let str = str where isNotEmpty(str) else {
            return false
        }
        
        guard let searchChars = searchChars where searchChars.count > 0 else {
            return false
        }
        
        for i in 0 ..< str.length {
            let ch = str.charAt(i)
            
            for j in 0 ..< searchChars.count {
                if (searchChars[j] == ch) {
                    return true
                }
            }
        }
        
        return false
    }
    
    static public func containsAny(str: String?, inString searchChars: String?) -> Bool {
        guard let searchChars = searchChars where isNotEmpty(searchChars) else {
            return false
        }
        
        return containsAny(str, fromCharacters: Array(searchChars.characters))
    }
    
    static public func containsAny(str: String?, inStrings searchChars: [String]?) -> Bool {
        guard let str = str where isNotEmpty(str) else {
            return false
        }
        
        guard let searchChars = searchChars where searchChars.count > 0 else {
            return false
        }
        
        for searchChar in searchChars {
            let ok = containsAny(str, fromCharacters: Array(searchChar.characters))
            if ok {
                return true
            }
        }
        
        return false
    }
    
    //MARK: - Replacing
    
    //TODO: Testar
    static public func replaceOnce(text: String?, inQuery repl: String?, withReplacement with: String?) -> String {
        return replace(text, inQuery: repl, withReplacement: with, atMaximum: 1)
    }
    
    //TODO: Testar
    static public func replace(text: String?, inQuery repl: String?, withReplacement with: String?, atMaximum max: Int = -1) -> String {
        guard let text = text else {
            return ""
        }
        
        guard let repl = repl where isNotEmpty(repl) else {
            return text
        }
        
        guard let with = with else {
            return text
        }
        
        var max = max
        
        if (max == 0) {
            return text
        }
        
        var buf = ""
        
        var start = 0
        var end = text.indexOfString(repl, atStartIndex: start)
        
        while (end != -1) {
            buf.appendContentsOf(text.substringFromIndex(start, toIndex: end) + with)
            
            start = end + repl.length
            
            max -= 1
            if (max == 0) {
                break
            }
            
            end = text.indexOfString(repl, atStartIndex: start)
        }
        
        buf.appendContentsOf(text.substringFromIndex(start))
        return buf
    }
    
    //MARK: - Bytes
    
    static public func getLengthUTF8StringInBytes(s: String?) throws -> Int {
        if let s = s {
            if let data = s.dataUsingEncoding(NSUTF8StringEncoding) {
                return data.length
            }
            
            throw Exception.RunTimeException(message: "Unsupported Encoding")
        }
        
        return 0
    }
}