//
//  Prefs.swift
//  Pods
//
//  Created by Ricardo Lecheta on 6/30/14.
//  Copyright (c) 2014 Ricardo Lecheta. All rights reserved.
//

import Foundation

public class Prefs {
    
    //MARK: - Synchronize
    
    static public func synchornize() {
        let prefs = NSUserDefaults.standardUserDefaults()
        prefs.synchronize()
    }
    
    //MARK: - String
    
    static public func setString(value: String, forKey key: String) {
        let prefs = NSUserDefaults.standardUserDefaults()
        prefs.setValue(value, forKey: key)
        prefs.synchronize()
    }
    
    static public func getString(key: String) -> String {
        let prefs = NSUserDefaults.standardUserDefaults()
        if let s = prefs.stringForKey(key) {
            return s
        }
        return ""
    }
    
    //MARK: - Int
    
    static public func setInt(value: Int, forKey key: String) {
        let prefs = NSUserDefaults.standardUserDefaults()
        prefs.setInteger(value, forKey: key)
        prefs.synchronize()
    }

    static public func getInt(key: String) -> Int! {
        let prefs = NSUserDefaults.standardUserDefaults()
        let i = prefs.integerForKey(key)
        return i
    }
    
    //MARK: - Float
    
    static public func setFloat(value: Float, forKey key: String) {
        let prefs = NSUserDefaults.standardUserDefaults()
        prefs.setFloat(value, forKey: key)
        prefs.synchronize()
    }
    
    static public func getFloat(key: String) -> Float {
        let prefs = NSUserDefaults.standardUserDefaults()
        let f = prefs.floatForKey(key)
        return f
    }
    
    //MARK: - Double
    
    static public func setDouble(value: Double, forKey key: String) {
        let prefs = NSUserDefaults.standardUserDefaults()
        prefs.setDouble(value, forKey: key)
        prefs.synchronize()
    }
    
    static public func getDouble(key: String) -> Double {
        let prefs = NSUserDefaults.standardUserDefaults()
        let d = prefs.doubleForKey(key)
        return d
    }
    
    //MARK: - Boolean
    
    static public func setBoolean(value: Bool, forKey key: String) {
        let prefs = NSUserDefaults.standardUserDefaults()
        prefs.setBool(value, forKey: key)
        prefs.synchronize()
    }
    
    static public func getBoolean(key: String) -> Bool {
        let prefs = NSUserDefaults.standardUserDefaults()
        let b = prefs.boolForKey(key)
        return b
    }
    
    //MARK: - Object
    
    static public func setObject(value: AnyObject, forKey key: String) {
        let prefs = NSUserDefaults.standardUserDefaults()
        let objectData = NSKeyedArchiver.archivedDataWithRootObject(value)
        prefs.setObject(objectData, forKey: key)
        prefs.synchronize()
    }
    
    static public func getObject(key: String) -> AnyObject? {
        let prefs = NSUserDefaults.standardUserDefaults()
        if let data = prefs.objectForKey(key) as? NSData {
            let o = NSKeyedUnarchiver.unarchiveObjectWithData(data)
            return o
        }
        return nil
    }
    
    //MARK: - Json
    
    static public func setJson(object: AnyObject, forKey key: String) throws -> String {
        var json: String!
        if (object is NSArray) {
            json = try (object as! NSArray).toJsonString()
        } else if (object is NSDictionary) {
            json = try (object as! NSDictionary).toJsonString()
        } else if (object is NSObject) {
            json = try (object as! NSObject).toJsonString()
        } else {
            throw NSError(domain: "PrefsDomain", code: 1, userInfo: [NSLocalizedDescriptionKey : "Não foi possível transformar o objeto em json"])
        }
        
        Prefs.setString(json, forKey: key)
        return json
    }
    
    static public func getJson(cls: AnyClass, forKey key: String) throws -> AnyObject {
        let json = Prefs.getString(key)
        let o = try JSON.fromJson(string: json, cls: cls)
        return o
    }
    
    //MARK: - Clear
    
    static public func clearAll() {
        if let appDomain = NSBundle.mainBundle().bundleIdentifier {
            let prefs = NSUserDefaults.standardUserDefaults()
            prefs.removePersistentDomainForName(appDomain)
        }
    }
}