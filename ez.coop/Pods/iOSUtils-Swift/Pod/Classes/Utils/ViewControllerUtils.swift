//
//  ViewControllerUtils.swift
//  Pods
//
//  Created by Livetouch BR on 6/23/16.
//
//

import UIKit

public class ViewControllerUtils: NSObject {
    
    static public func getAvailableScreenHeight(viewController:UIViewController) -> CGFloat {
        let statusBarHeight = StatusBarUtils.getHeight()
        let navigationBarHeight = NavigationBarUtils.getHeight(viewController)
        let tabBarHeight = NavigationBarUtils.getHeight(viewController)
        
        guard let keyWindow = UIApplication.sharedApplication().keyWindow else {
            return 0.0
        }
        
        var availableHeight = keyWindow.frame.height
        availableHeight -= statusBarHeight + navigationBarHeight + tabBarHeight
        
        return availableHeight
    }
}
