//
//  GPSUtils.swift
//  Pods
//
//  Created by Guilherme Politta on 5/07/16.
//
//

import UIKit
import CoreLocation

public class GPSUtils: NSObject, CLLocationManagerDelegate {

    //MARK: Public Variables
    
    public var timeout: NSTimeInterval = 15
    
    //MARK: Private Variables
    
    var locationManager: CLLocationManager?
    var currentLocation: CLLocation?
    var error: NSError?
    var cancelTimeout: Bool = false
    
    var semaphore: dispatch_semaphore_t?
    var timeoutTimer: dispatch_time_t?
    
    //MARK: - Current Location
    
    public func getCurrentLocation() throws -> CLLocation {
        self.semaphore = dispatch_semaphore_create(0)
        
        self.performSelectorOnMainThread(#selector(startLocationManager), withObject: nil, waitUntilDone: true)
        
        timeoutTimer = dispatch_time(DISPATCH_TIME_NOW, (Int64(self.timeout) * Int64(NSEC_PER_SEC)))
        
        dispatch_after(self.timeoutTimer!, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
            self.requestTimeout()
        })
        
        dispatch_semaphore_wait(self.semaphore!, DISPATCH_TIME_FOREVER)
        
        if let error = self.error {
            throw error
        }
        
        if let location = self.currentLocation {
            return location
        }
        
        throw  NSError(domain: "GPSUtilsDomain", code: 2, userInfo: [NSLocalizedDescriptionKey : "Não foi possivel obter a localização do usuário"])
    }
    
    public func startLocationManager() {
        self.locationManager = GPSUtils.startGPS(self, requestAlways: false)
    }
    
    public func requestTimeout() {
        if (self.cancelTimeout) {
            return
        }
        
        self.stopTimeout()
        
        self.error = NSError(domain: "GPSUtilsDomain", code: 2, userInfo: [NSLocalizedDescriptionKey : "Não foi possivel obter a localização do usuário"])
        
        if let semaphore = semaphore {
            dispatch_semaphore_signal(semaphore)
        }
    }
    
    public func stopTimeout() {
        self.cancelTimeout = true
        GPSUtils.stopGPS(self.locationManager)
    }
    
    //MARK: - CLLocationManagerDelegate
    
    public func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        GPSUtils.stopGPS(self.locationManager)
        
        self.currentLocation = locations.last
        
        if let semaphore = semaphore {
            dispatch_semaphore_signal(semaphore)
        }
    }
    
    public func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        self.stopTimeout()
        
        self.error = error;
        
        if let semaphore = semaphore {
            dispatch_semaphore_signal(semaphore)
        }
    }
    
    
    public func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        if (status == .Denied || status == .Restricted) {
            self.stopTimeout()
            
            self.error = NSError(domain: "GPSUtilsDomain", code: 1, userInfo: [NSLocalizedDescriptionKey : "Usuário negou permissão de localização"])
            
            if let semaphore = semaphore {
                dispatch_semaphore_signal(semaphore)
            }
        }
    }
    
    //MARK: - Start/Stop
    
    public static func startGPS(delegate: CLLocationManagerDelegate, requestAlways: Bool) -> CLLocationManager {
        let locationManager = CLLocationManager()
        locationManager.delegate = delegate
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        
        if (requestAlways) {
            locationManager.requestAlwaysAuthorization()
        } else {
            locationManager.requestWhenInUseAuthorization()
        }
        
        locationManager.startUpdatingLocation()
        
        return locationManager
    }
    
    public static func stopGPS(locationManager: CLLocationManager?) {
        if let locationManager = locationManager {
            locationManager.delegate = nil
            locationManager.stopUpdatingLocation()
        }
    }
    
    //MARK: - Convert
    
    public static func convert(value: Double) -> String {
        var latitude = fabs(value)
        
        let degree = Int(latitude)
        latitude *= 60
        latitude -= Double(degree * 60)
        let minute = Int(latitude)
        latitude *= 60
        latitude -= Double(minute * 60)
        let second = Int(latitude * 1000)
        
        let string = "\(degree)/1,\(minute)/1,\(second)/1000,"
        
        return string
    }
    
    //MARK: - Reference
    
    public static func latitudeRef(latitude: Double) -> String {
        return latitude < 0 ? "S" : "N"
    }
    
    public static func longitudeRef(longitude: Double) -> String {
        return longitude < 0.0 ? "W" : "E"
    }
}
