//
//  ContraintUtils.swift
//  Pods
//
//  Created by Livetouch-Mini01 on 06/07/2016.
//
//

import UIKit

public class ConstraintUtils: NSObject {
    
    //MARK: - Multiplier
    
    static public func changeConstraintMultiplier(constraint: NSLayoutConstraint, toValue value: CGFloat, inView view: UIView) -> NSLayoutConstraint {
        let newConstraint = NSLayoutConstraint(item: constraint.firstItem, attribute: constraint.firstAttribute, relatedBy: constraint.relation, toItem: constraint.secondItem, attribute: constraint.secondAttribute, multiplier: value, constant: constraint.constant)
        
        newConstraint.priority = constraint.priority;
        
        if (DeviceUtils.isIOS8()) {
            NSLayoutConstraint.deactivateConstraints([constraint])
            NSLayoutConstraint.activateConstraints([newConstraint])
        } else {
            view.removeConstraint(constraint)
            view.addConstraint(newConstraint)
        }
        
        return newConstraint
    }
}
