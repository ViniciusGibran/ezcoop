//
//  DateUtils.swift
//  Pods
//
//  Created by Livetouch-Mini01 on 04/07/2016.
//
//

import UIKit

import SwiftDate

public class DateUtils: NSObject {
    
    
//    public static final String[] MESES_ABREVIADOS = new String[]{"Jan", "Fev", "Mar", "Abr", "Mai", "Jun", "Jul", "Ago", "Set", "Out", "Nov", "Dez"};
//    
//    public static final String[] MESES_COMPLETOS = new String[]{"Janeiro", "Fevereiro", "MarÃ§o", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro",
//    "Outubro", "Novembro", "Dezembro"};
//    
//    public static final int[] ULTIMA_DIA_MES = new int[]{31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
//    
//    public static final String DATE = "dd/MM/yyyy";
//    
//    // nao tem diferenca entre dia e noite
//    public static final String DATE_TIME_AM_PM = "dd/MM/yyyy hh:mm:ss";
//    
//    // formato ate 25h
//    public static final String DATE_TIME_24h = "dd/MM/yyyy HH:mm:ss";
//    
//    private static final String TAG = "DateUtils";
//    
    
    //MARK: - String
    
    static public func getDate(date: NSDate? = NSDate(), withPattern pattern: String = "dd/MM/yyyy") -> String {
        return toString()
    }
    
    static public func toString(date: NSDate? = NSDate(), withPattern pattern: String = "dd/MM/yyyy", andLocale localeIdentifier: String? = nil) -> String {
        guard let date = date else {
            return ""
        }
        
        let formatter = NSDateFormatter()
        formatter.dateFormat = pattern
        
        if let localeIdentifier = localeIdentifier {
            formatter.locale = NSLocale(localeIdentifier: localeIdentifier)
        } else {
            formatter.locale = NSLocale.currentLocale()
        }
        
        return formatter.stringFromDate(date)
    }
    
    //MARK: - Date
    
    static public func getDate(year: Int, andMonth month: Int, andDay day: Int) -> NSDate {
        let date = NSDate(refDate: NSDate(), year: year, month: month, day: day, region: Region.LocalRegion())
        return date
    }
    
    //TODO: Terminar.
//    static public func toDate(dateString: String, withPattern pattern: String = "dd/MM/yyyy") -> NSDate? {
//        
//        let date = NSDate.date(fromString: dateString, format: .Custom(pattern))
//        return date
//        
//        let date = NSDate(refDate: NSDate(), era: nil, year: nil, month: nil, day: nil, yearForWeekOfYear: nil, weekOfYear: nil, weekday: nil, hour: nil, minute: nil, second: nil, nanosecond: nil, region: Region.LocalRegion())
//        return date
//    }
    
    //MARK: - Comparison
    
    static public func compareTo(dateA: NSDate?, andDate dateB: NSDate?) -> Int {
        // - : menor (dateA < dateB)
        // 0 : igual (dateA = dateB)
        // + : maior (dateA > dateB)
        
        let sA = toString(dateA, withPattern: "yyyyMMdd")
        let sB = toString(dateB, withPattern: "yyyyMMdd")
        
        let result = sA.compare(sB)
        if (result == .OrderedAscending) {
            return -1
        } else if (result == .OrderedDescending) {
            return 1
        } else {
            return 0
        }
    }
    
    static public func equals(dateA: NSDate?, andDate dateB: NSDate?) -> Bool {
        return compareTo(dateA, andDate: dateB) == 0
    }
    
    //MARK: - Formatting
    
    static public func toOnlyDate(components: NSDateComponents) {
        components.hour = 0
        components.minute = 0
        components.second = 0
        components.nanosecond = 0
    }
    
    static public func zeroTime(date: NSDate) -> NSDate? {
        let zeroTimeDate = date.startOf(.Day, inRegion: Region.LocalRegion())
        return zeroTimeDate
    }
    
    static public func maxTime(date: NSDate) -> NSDate? {
        let maxTimeDate = date.endOf(.Day, inRegion: Region.LocalRegion())
        return maxTimeDate
    }
    
    //MARK: - Getters
    
    static public func getDay(date: NSDate) -> Int {
        return date.day
    }
    
    static public func getMonth(date: NSDate) -> Int {
        return date.month
    }
    
    static public func getFullMonthString(date: NSDate, withLocaleIdentifier localeIdentifier: String = "pt_BR") -> String {
        let formatter = NSDateFormatter()
        formatter.locale = NSLocale(localeIdentifier: "pt_BR")
        
        let months = formatter.monthSymbols
        let index = date.month - 1
        
        guard index < months.count else {
            return ""
        }
        
        return months[index]
    }
    
    static public func getYear(date: NSDate) -> Int {
        return date.year
    }
    
    static public func getHoursDifference(startDate: NSDate?, fromDate endDate: NSDate?) -> Int {
//        guard let startDate = startDate else {
//            return 0
//        }
//        
//        guard let endDate = endDate else {
//            return 0
//        }
//        
//        
//        
//        let result = endDate - startDate (CalendarType(amount: 0, calendarUnit: NSCalendarUnit.Hour))
        
        return 0
    }
    
    
    
//    
//    /**
//     * http://www.exampledepot.com/egs/java.util/CompDates.html
//     * <p>
//     * Retorna a quantidade de horas de diferenÃ§a entre a data informada e a
//     * data atual
//     *
//     * @return
//     */
//    public static long getDiferencaHoras(Date dataInicial, Date dataFinal) {
//    if (dataInicial == null || dataFinal == null) {
//    return 0;
//    }
//    
//    Calendar c1 = Calendar.getInstance();
//    c1.setTime(dataInicial);
//    
//    Calendar c2 = Calendar.getInstance();
//    c2.setTime(dataFinal);
//    
//    // Determine which is earlier
//    // boolean b = c1.after(c2); // false
//    // b = c1.before(c2); // true
//    
//    // Get difference in milliseconds
//    long diffMillis = c2.getTimeInMillis() - c1.getTimeInMillis();
//    
//    // Get difference in seconds
//    // long diffSecs = diffMillis/(1000);
//    
//    // Get difference in minutes
//    // long diffMins = diffMillis/(60*1000);
//    
//    // Get difference in hours
//    long diffHours = diffMillis / (60 * 60 * 1000);
//    
//    // Get difference in days
//    // long diffDays = diffMillis/(24*60*60*1000);
//    
//    return diffHours;
//    }
//    
//    /**
//     * http://www.exampledepot.com/egs/java.util/CompDates.html
//     * <p>
//     * Retorna a quantidade de horas de diferenÃ§a entre a data informada e a
//     * data atual
//     *
//     * @return
//     */
//    public static long getDiferencaDias(Date dataInicial, Date dataFinal) {
//    if (dataInicial == null || dataFinal == null) {
//    return 0;
//    }
//    
//    Calendar c1 = Calendar.getInstance();
//    c1.setTime(dataInicial);
//    
//    Calendar c2 = Calendar.getInstance();
//    c2.setTime(dataFinal);
//    
//    // Determine which is earlier
//    // boolean b = c1.after(c2); // false
//    // b = c1.before(c2); // true
//    
//    // Get difference in milliseconds
//    long diffMillis = c2.getTimeInMillis() - c1.getTimeInMillis();
//    
//    // Get difference in seconds
//    // long diffSecs = diffMillis/(1000);
//    
//    // Get difference in minutes
//    // long diffMins = diffMillis/(60*1000);
//    
//    // Get difference in hours
//    // long diffHours = diffMillis/(60*60*1000);
//    
//    // Get difference in days
//    long diffDays = diffMillis / (24 * 60 * 60 * 1000);
//    
//    return diffDays;
//    }
//    
//    /**
//     * Faz a soma de 1 dia na data especificada, corrigindo o problema com
//     * horÃ¡rio de verÃ£o.<br>
//     * <p>
//     * Se por acaso o calendar do Java (ao somar 1 dia), continuar no mesmo dia
//     * se for horÃ¡rio de verÃ£o (ex: horario de verÃ£o perde 1 hora, e volta
//     * para as 23h) O algoritmo forÃ§a a troca de dia
//     *
//     * @return
//     * @author Ricardo R. Lecheta
//     * @since v2.0, 14/1/2009
//     */
//    public static Date addDiaHorarioVerao(Date date, int dias) {
//    Calendar c = Calendar.getInstance();
//    c.setTime(date);
//    
//    int dia1 = c.get(Calendar.DAY_OF_MONTH);
//    c.add(Calendar.DATE, dias);
//    int dia2 = c.get(Calendar.DAY_OF_MONTH);
//    if (dia1 == dia2) {
//    // Adiciona novamente o dia porque o HorÃ¡rio de VerÃ£o nÃ£o troca
//    // de dia
//    // Ex: 10/10/2009, fica para 10/10/2009 - 23:00:00
//    c.add(Calendar.DATE, dias);
//    }
//    
//    // zera as horas
//    c.set(Calendar.HOUR_OF_DAY, 0);
//    c.set(Calendar.MINUTE, 0);
//    c.set(Calendar.SECOND, 0);
//    c.set(Calendar.MILLISECOND, 0);
//    c.set(Calendar.HOUR_OF_DAY, 0);
//    
//    Date data = c.getTime();
//    return data;
//    }
//    
//    /**
//     * Faz a soma de 1 dia na data especificada, corrigindo o problema com
//     * horÃ¡rio de verÃ£o.<br>
//     * <p>
//     * Se por acaso o calendar do Java (ao somar 1 dia), continuar no mesmo dia
//     * se for horÃ¡rio de verÃ£o (ex: horario de verÃ£o perde 1 hora, e volta
//     * para as 23h) O algoritmo forÃ§a a troca de dia
//     *
//     * @return
//     * @author Ricardo R. Lecheta
//     * @since v2.0, 14/1/2009
//     */
//    public static Date addDia(Date date, int dias) {
//    Calendar c = Calendar.getInstance();
//    c.setTime(date);
//    
//    c.add(Calendar.DATE, dias);
//    
//    // zera as horas
//    c.set(Calendar.HOUR_OF_DAY, 0);
//    c.set(Calendar.MINUTE, 0);
//    c.set(Calendar.SECOND, 0);
//    c.set(Calendar.MILLISECOND, 0);
//    c.set(Calendar.HOUR_OF_DAY, 0);
//    
//    Date data = c.getTime();
//    return data;
//    }
//    
//    public static String getMesDesc(int mes) {
//    return MESES_COMPLETOS[mes - 1];
//    }
//    
//    public static String getMesDescAbrev(int mes) {
//    return MESES_ABREVIADOS[mes - 1];
//    }
//    
//    public static boolean isMaior(Date dateA, Date dateB, String pattern) {
//    return compareTo(dateA, dateB, pattern) > 0;
//    }
//    
//    public static boolean isIgual(Date dateA, Date dateB, String pattern) {
//    return compareTo(dateA, dateB, pattern) == 0;
//    }
//    
//    public static boolean isMenor(Date dateA, Date dateB, String pattern) {
//    return compareTo(dateA, dateB, pattern) < 0;
//    }
//    
//    public static boolean isMaior(Date dateA, Date dateB) {
//    return compareTo(dateA, dateB) > 0;
//    }
//    
//    public static boolean isIgual(Date dateA, Date dateB) {
//    return compareTo(dateA, dateB) == 0;
//    }
//    
//    public static boolean isMenor(Date dateA, Date dateB) {
//    return compareTo(dateA, dateB) < 0;
//    }
//    
//    /**
//     * Retorna o numero de dias uteis no mes que ocorreram antes desta Data
//     * <p>
//     * Jan = 1
//     *
//     * @return
//     */
//    public static int getDiasUteisAteDia(Date dateFim) {
//    int diaFim = getDia(dateFim);
//    int mes = getMes(dateFim);
//    
//    Calendar c = Calendar.getInstance();
//    c.set(Calendar.MONTH, mes - 1);
//    int dias = 0;
//    for (int i = 1; i < diaFim; i++) {
//    c.set(Calendar.DAY_OF_MONTH, i);
//    if (isDiaSemana(c.getTime())) {
//    dias++;
//    }
//    
//    }
//    return dias;
//    }
//    
//    /**
//     * Retorna o numero de dias uteis no mes que ainda tem depois desta Data
//     *
//     * @param dataInicio
//     * @return
//     */
//    public static int getDiasUteisDepoisDia(Date dataInicio) {
//    int diaInicio = getDia(dataInicio);
//    int mes = getMes(dataInicio);
//    
//    Calendar c = Calendar.getInstance();
//    c.set(Calendar.MONTH, mes - 1);
//    int dias = 0;
//    int diasMes = ULTIMA_DIA_MES[mes - 1];
//    for (int i = diaInicio + 1; i <= diasMes; i++) {
//    c.set(Calendar.DAY_OF_MONTH, i);
//    if (isDiaSemana(c.getTime())) {
//    dias++;
//    }
//    
//    }
//    return dias;
//    }
//    
//    /**
//     * Retorna o numero de dias uteis no mes
//     *
//     * @param mes Jan = 1
//     * @return
//     */
//    public static int getDiasUteis(int mes) {
//    Calendar c = Calendar.getInstance();
//    c.set(Calendar.MONTH, mes - 1);
//    int dias = 0;
//    int diasMes = ULTIMA_DIA_MES[mes - 1];
//    for (int i = 1; i <= diasMes; i++) {
//    c.set(Calendar.DAY_OF_MONTH, i);
//    if (isDiaSemana(c.getTime())) {
//    dias++;
//    }
//    
//    }
//    return dias;
//    }
//    
//    /**
//     * Ã‰ domingo?
//     *
//     * @param date
//     * @return
//     */
//    public static boolean isDomingo(Date date) {
//    Calendar c = Calendar.getInstance();
//    c.setTime(date);
//    int dia = c.get(Calendar.DAY_OF_WEEK);
//    
//    boolean b = dia == Calendar.SUNDAY;
//    
//    return b;
//    }
//    
//    /**
//     * Ã‰ sÃ¡bado?
//     *
//     * @param date
//     * @return
//     */
//    public static boolean isSabado(Date date) {
//    Calendar c = Calendar.getInstance();
//    c.setTime(date);
//    int dia = c.get(Calendar.DAY_OF_WEEK);
//    
//    boolean b = dia == Calendar.SATURDAY;
//    
//    return b;
//    }
//    
//    /**
//     * Verifica se a data Ã© de 2Âª-feira a 6Âª-feira
//     *
//     * @param date
//     * @return
//     */
//    public static boolean isDiaSemana(Date date) {
//    Calendar c = Calendar.getInstance();
//    c.setTime(date);
//    int dia = c.get(Calendar.DAY_OF_WEEK);
//    
//    switch (dia) {
//    case Calendar.MONDAY:
//    case Calendar.TUESDAY:
//    case Calendar.WEDNESDAY:
//    case Calendar.THURSDAY:
//    case Calendar.FRIDAY:
//    return true;
//    }
//    
//    return false;
//    }
//    
//    public static String toStringFromDateString(String dataString, String fromPattern, String toPattern) {
//    try {
//    Date data1 = toDate(dataString, fromPattern);
//    
//    String s = toString(data1, toPattern);
//    
//    return s;
//    } catch (Exception e) {
//    logError(TAG, e.getMessage(), e);
//    return dataString;
//    }
//    }
//    
//    // Data Feriado Motivaï¿½ï¿½o
//    // 1ï¿½ de janeiro Confraternizaï¿½ï¿½o Universal social
//    // 21 de abril Tiradentes cï¿½vica
//    // 1 de maio Dia do Trabalho social
//    // 7 de setembro Independï¿½ncia do Brasil cï¿½vica
//    // 12 de outubro Nossa Senhora Aparecida religiosa (catï¿½lica)
//    // 2 de novembro Finados religiosa (catï¿½lica)
//    // 15 de novembro Proclamaï¿½ï¿½o da Repï¿½blica cï¿½vica
//    // 25 de dezembro Natal religiosa (cristï¿½)
//    // from http://pt.wikipedia.org/wiki/Feriados_no_Brasil
//    private static final Date[] FERIADOS = new Date[]{
//    new Date(0, 0, 1),
//    new Date(0, 3, 21),
//    new Date(0, 4, 1),
//    new Date(0, 8, 7),
//    new Date(0, 9, 12),
//    new Date(0, 10, 2),
//    new Date(0, 10, 15),
//    new Date(0, 11, 25)
//    };
//    
//    /**
//     * Retorna se ï¿½ um feriado fixo
//     *
//     * @return
//     */
//    public static boolean isFeriadoFixo(Date date) {
//    Calendar calendar = Calendar.getInstance();
//    for (Date feriado : FERIADOS) {
//    calendar.setTime(feriado);
//    int diaFeriado = calendar.get(Calendar.DAY_OF_MONTH);
//    int mesFeriado = calendar.get(Calendar.MONTH);
//    calendar.setTime(date);
//    int dia = calendar.get(Calendar.DAY_OF_MONTH);
//    int mes = calendar.get(Calendar.MONTH);
//    if (dia == diaFeriado && mes == mesFeriado) {
//    return true;
//    }
//    }
//    return false;
//    }
//    
//    /**
//     * Retorna um array de meses entre duas datas
//     */
//    
//    public static List<String> getMesesEntre(Date dataIni, Date dataFim) {
//    List<String> lista = new ArrayList<String>();
//    if (dataIni == null || dataFim == null || dataIni.after(dataFim)) {
//    return null;
//    }
//    Calendar calendar = new GregorianCalendar(new Locale("pt", "BR"));
//    calendar.setTime(dataIni);
//    String dataIniStr = DateUtils.toString(dataIni, "yyyyMM");
//    String dataFimStr = DateUtils.toString(dataFim, "yyyyMM");
//    while (!dataIniStr.equals(dataFimStr)) {
//    lista.add(DateUtils.toString(calendar.getTime(), "yyyyMM"));
//    calendar.add(Calendar.MONTH, 1);
//    dataIniStr = DateUtils.toString(calendar.getTime(), "yyyyMM");
//    }
//    lista.add(dataFimStr);
//    return lista;
//    }
//    
//    public static Date getDate(int year, int month, int day) {
//    Calendar c = Calendar.getInstance();
//    c.set(Calendar.YEAR, year);
//    c.set(Calendar.MONTH, month);
//    c.set(Calendar.DAY_OF_MONTH, day);
//    return c.getTime();
//    }
//    
//    /**
//     * Hoje-1 = -1
//     * <p>
//     * 1 Semana = -7
//     * 1 Mï¿½s 	= -30
//     * 6 Mï¿½ses	= -30*6
//     * 12 Mï¿½ses	= -30*12
//     *
//     * @param day
//     * @return
//     */
//    public static Date getDateAddDays(int day) {
//    Calendar c = Calendar.getInstance();
//    c.add(Calendar.DAY_OF_MONTH, day);
//    Date d = c.getTime();
//    return d;
//    
//    }
//    
//    /**
//     * dd/MM/yyyy
//     *
//     * @param date
//     * @return
//     */
//    public static boolean isDate(String date) {
//    Date dt = DateUtils.toDate(date);
//    boolean ok = dt != null;
//    return ok;
//    }
//    
//    public static boolean isDate(String date, String format) {
//    Date dt = DateUtils.toDate(date, format);
//    boolean ok = dt != null;
//    return ok;
//    }
//    
//    public static Idade getIdade(Date birthDate) {
//    try {
//    int years = 0;
//    int months = 0;
//    int days = 0;
//    //create calendar object for birth day
//    Calendar birthDay = Calendar.getInstance();
//    birthDay.setTimeInMillis(birthDate.getTime());
//    //create calendar object for current day
//    long currentTime = System.currentTimeMillis();
//    Calendar now = Calendar.getInstance();
//    now.setTimeInMillis(currentTime);
//    //Get difference between years
//    years = now.get(Calendar.YEAR) - birthDay.get(Calendar.YEAR);
//    int currMonth = now.get(Calendar.MONTH) + 1;
//    int birthMonth = birthDay.get(Calendar.MONTH) + 1;
//    //Get difference between months
//    months = currMonth - birthMonth;
//    //if month difference is in negative then reduce years by one and calculate the number of months.
//    if (months < 0) {
//    years--;
//    months = 12 - birthMonth + currMonth;
//    if (now.get(Calendar.DATE) < birthDay.get(Calendar.DATE))
//    months--;
//    } else if (months == 0 && now.get(Calendar.DATE) < birthDay.get(Calendar.DATE)) {
//    years--;
//    months = 11;
//    }
//    //Calculate the days
//    if (now.get(Calendar.DATE) > birthDay.get(Calendar.DATE))
//    days = now.get(Calendar.DATE) - birthDay.get(Calendar.DATE);
//    else if (now.get(Calendar.DATE) < birthDay.get(Calendar.DATE)) {
//    int today = now.get(Calendar.DAY_OF_MONTH);
//    now.add(Calendar.MONTH, -1);
//    days = now.getActualMaximum(Calendar.DAY_OF_MONTH) - birthDay.get(Calendar.DAY_OF_MONTH) + today;
//    } else {
//    days = 0;
//    if (months == 12) {
//    years++;
//    months = 0;
//    }
//    }
//    return new Idade(days, months, years);
//    } catch (Exception e) {
//    LogUtil.logError(TAG,e.getMessage(), e);
//    return new Idade(0,0,0);
//    }
//    }
}
