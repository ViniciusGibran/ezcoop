//
//  PrintScreenUtils.swift
//  Pods
//
//  Created by Livetouch-Mini01 on 06/07/2016.
//
//

import UIKit

public class PrintScreenUtils: NSObject {
    
    //MARK: - Save
    
    static public func printScreen(view: UIView?, toJPGFile filename: String) {
        guard let view = view else {
            return
        }
        
        guard let image = getPrintScreen(view) else {
            return
        }
        
        var filename = filename
        
        if (!StringUtils.contains(filename, fromQuery: ".jpg")) {
            filename.appendContentsOf(".jpg")
        }
        
        if let data = UIImageJPEGRepresentation(image, 1.0) {
            do {
                try data.writeToFile(filename, options: NSDataWritingOptions.DataWritingWithoutOverwriting)
            } catch {
                LogUtils.log("Erro ao salvar print screen no arquivo \(filename).")
            }
        }
    }
    
    static public func printScreen(view: UIView?, toPNGFile filename: String) {
        guard let view = view else {
            return
        }
        
        guard let image = getPrintScreen(view) else {
            return
        }
        
        var filename = filename
        
        if (!StringUtils.contains(filename, fromQuery: ".png")) {
            filename.appendContentsOf(".png")
        }
        
        if let data = UIImagePNGRepresentation(image) {
            do {
                try data.writeToFile(filename, options: NSDataWritingOptions.DataWritingWithoutOverwriting)
            } catch {
                LogUtils.log("Erro ao salvar print screen no arquivo \(filename).")
            }
        }
    }
    
    //MARK: - Get
    
    static public func getPrintScreen(view: UIView) -> UIImage? {
        let screenSize = UIScreen.mainScreen().applicationFrame.size
        let colorSpaceRef = CGColorSpaceCreateDeviceRGB()
        let bitmapInfo = CGImageAlphaInfo.None
        
        guard let ctx = CGBitmapContextCreate(nil, Int(screenSize.width), Int(screenSize.height), 8, 4 * Int(screenSize.width), colorSpaceRef, bitmapInfo.rawValue) else {
            return nil
        }
        
        CGContextTranslateCTM(ctx, 0.0, screenSize.height)
        CGContextScaleCTM(ctx, 1.0, -1.0)
        
        view.layer.renderInContext(ctx)
        
        guard let cgImage = CGBitmapContextCreateImage(ctx) else {
            return nil
        }
        
        let image = UIImage(CGImage: cgImage)
        return image
    }
}
