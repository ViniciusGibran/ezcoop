//
//  EmailUtils.swift
//  Pods
//
//  Created by Livetouch-Mini01 on 21/06/2016.
//
//

import UIKit

public class EmailUtils: NSObject {
    
    static public func sendEmailTo(destinationEmail: String, withSubject subject: String = "", andBody body: String = "") {
        
        var email = "mailto:\(destinationEmail)"
        
        if subject.isNotEmpty {
            email = "\(email)?subject=\(subject)"
        }
        
        if body.isNotEmpty {
            email = "\(email)&body=\(body)"
        }
        
        if let addedPercent = email.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet()) {
            if let nsurl = NSURL(string: addedPercent) {
                UIApplication.sharedApplication().openURL(nsurl)
            }
        }
    }
    
    static public func isValid(string: String?, withStrictRules strict: Bool = false) -> Bool {
        let emailRegex = strict ? "^[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}$" : "^.+@.+\\.[A-Za-z]{2}[A-Za-z]*$"
        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegex)
        
        return emailTest.evaluateWithObject(string)
    }
}
