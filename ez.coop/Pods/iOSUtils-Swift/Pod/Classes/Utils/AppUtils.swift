//
//  AppUtils.swift
//  Pods
//
//  Created by Livetouch-Mini01 on 17/06/2016.
//
//

import UIKit

public class AppUtils: NSObject {
    
    //MARK: - Information
    
    static public func getName() -> String {
        let bundle = NSBundle.mainBundle()
        if let displayName = bundle.objectForInfoDictionaryKey("CFBundleDisplayName") as? String {
            return displayName
        }
        return ""
    }
    
    static public func getVersion() -> String {
        let bundle = NSBundle.mainBundle()
        if let version = bundle.objectForInfoDictionaryKey("CFBundleShortVersionString") as? String {
            return version
        }
        return ""
    }
    
    static public func getViewControllerBasedStatusBarAppearance() throws -> Bool {
        let bundle = NSBundle.mainBundle()
        if let vcBased = bundle.objectForInfoDictionaryKey("UIViewControllerBasedStatusBarAppearance") as? Bool {
            return vcBased
        }
        throw Exception.DomainException(message: "Adicione o atributo 'UIViewControllerBasedStatusBarAppearance' no seu Info.plist.")
    }
    
    //MARK: - App Store
    
    static public func openAppStore(appLink: String) {
        let appStorePrefix = "itms://itunes.apple.com/br/app/"
        
        var url = appLink
        url = url.stringByReplacingOccurrencesOfString(appStorePrefix, withString: "")
        
        if let nsurl = NSURL(string: "\(appStorePrefix)\(url)") {
            UIApplication.sharedApplication().openURL(nsurl)
        }
    }
    
    //MARK: - State
    
    static public func getState() -> UIApplicationState {
        return UIApplication.sharedApplication().applicationState
    }
    
    static public func isInBackground() -> Bool {
        let state = getState()
        return state == .Background
    }
    
    static public func isRunning() -> Bool {
        let state = getState()
        return state == .Active
    }
}