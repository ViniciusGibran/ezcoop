//
//  KeyboardUtils.swift
//  Pods
//
//  Created by Livetouch BR on 6/22/16.
//
//

import UIKit

public class KeyboardUtils: NSObject {
    
    //MARK: - Show
    
    static public func show(view: UIKeyInput?) {
        guard let view = view as? UIView else {
            return
        }
        
        let selector = #selector(view.becomeFirstResponder)
        
        if (view.respondsToSelector(selector)) {
            view.performSelector(selector)
        }
    }
    
    static public func hide(view: UIView?) {
        guard let view = view else {
            return
        }
        
        view.endEditing(true)
    }
    
    //MARK: - Sizes
    
    static public func getKeyboardSizeFromNotification(notification: NSNotification) -> CGSize {
        guard let userInfo = notification.userInfo else {
            return CGSizeMake(0.0, 0.0)
        }
        
        guard let keyboardSize = userInfo[UIKeyboardFrameEndUserInfoKey]?.CGRectValue.size else {
            return CGSizeMake(0.0, 0.0)
        }
        
        return keyboardSize
    }
    
    static public func getKeyboardWidthFromNotification(notification: NSNotification) -> CGFloat {
        let size = getKeyboardSizeFromNotification(notification)
        return size.width
    }
    
    static public func getKeyboardHeightFromNotification(notification: NSNotification) -> CGFloat {
        let size = getKeyboardSizeFromNotification(notification)
        return size.height
    }
    
}
