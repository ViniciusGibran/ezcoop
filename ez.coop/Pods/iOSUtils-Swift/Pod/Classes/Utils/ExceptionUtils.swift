//
//  ExceptionUtils.swift
//  Pods
//
//  Created by Livetouch BR on 6/22/16.
//
//

import UIKit

public class ExceptionUtils: NSObject {
    
    //MARK: - Get Messages
    
    static public func getExceptionMessage(exception: Exception) -> String {
        if let appDelegate = UIApplication.sharedApplication().delegate as? BaseAppDelegate {
            let filename = appDelegate.getConfig().getErrorMessageFilename()
            
            var fileData = try? FileUtils.getBundleFile(filename, ofType: nil)
            if (fileData == nil) {
                fileData = try? FileUtils.getLibFile(filename, ofType: nil)
            }
            
            do {
                guard let fileData = fileData else {
                    return "Arquivo '\(filename)' não se encontra no projeto."
                }
                
                let json = try NSJSONSerialization.JSONObjectWithData(fileData, options: .AllowFragments) as! [String: AnyObject]
                
                switch exception {
                    case .IOException:
                        return json.getStringWithKey("io_error")
                    case .GenericException:
                        return json.getStringWithKey("generic_error")
                    default:
                        return ""
                }
            } catch {
                return "Arquivo '\(filename)' está inválido."
            }
        } else {
            return "AppDelegate não é subclasse de BaseAppDelegate."
        }
    }
    
    static public func getDBExceptionMessage(exception: ErrorType) -> String {
        var errorMessage = ""
        
        switch exception {
            case Exception.NotImplemented:
                errorMessage = "AppDelegate não é subclasse de BaseAppDelegate."
                
            case SQLException.DatabaseHelperNotFound:
                errorMessage = "Não foi encontrado uma subclasse de DatabaseHelper."
                
            case SQLException.NotImplemented(let message):
                errorMessage = "Não foi implementado o método '\(message)' na subclasse de DatabaseHelper."
                
            default:
                break
        }
        
        return errorMessage
    }
    
    static public func getIOExceptionMessage() -> String {
        return getExceptionMessage(.IOException)
    }
    
    static public func getGenericMessage() -> String {
        return getExceptionMessage(.GenericException)
    }
    
    static public func getAppTransportSecurityMessage() -> String {
        return "Configure o atributo 'NSAppTransportSecurity' no seu Info.plist."
    }
    
    //MARK: - Alert Messages
    
    static public func alertException(exception: Exception) {
        let message = getExceptionMessage(exception)
        if message.isNotEmpty {
            AlertUtils.alert(message)
        }
    }
    
    static public func alertIOException() {
        alertException(.IOException)
    }
    
    static public func alertGenericException() {
        alertException(.GenericException)
    }
    
    static public func alertAppTransportSecurityException() {
        AlertUtils.alert(getAppTransportSecurityMessage())
    }
}