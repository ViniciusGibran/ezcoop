//
//  NetworkUtils.swift
//  Pods
//
//  Created by Livetouch-Mini01 on 21/06/2016.
//
//

import UIKit

public class NetworkUtils: NSObject {
    
    static public func isAvailable() -> Bool {
        do {
            let reachability = try Reachability.reachabilityForInternetConnection()
            let remoteHostStatus = reachability.currentReachabilityStatus
            
            if (remoteHostStatus == .NotReachable) {
                return true
            } else if (remoteHostStatus == .ReachableViaWiFi) {
                return true
            } else if (remoteHostStatus == .ReachableViaWWAN) {
                return true
            }
            
            return false
            
        } catch {
            return false
        }
    }
    
    static public func isWiFiAvailable() -> Bool {
        do {
            let reachability = try Reachability.reachabilityForInternetConnection()
            let remoteHostStatus = reachability.currentReachabilityStatus
            
            if (remoteHostStatus == .ReachableViaWiFi) {
                return true
            }
            
            return false
            
        } catch {
            return false
        }
    }
    
    static public func isMobileAvailable() -> Bool {
        do {
            let reachability = try Reachability.reachabilityForInternetConnection()
            let remoteHostStatus = reachability.currentReachabilityStatus
            
            if (remoteHostStatus == .ReachableViaWWAN) {
                return true
            }
            
            return false
            
        } catch {
            return false
        }
    }
}
