//
//  ClassUtils.swift
//  Pods
//
//  Created by Guilherme Politta on 6/07/16.
//
//

import UIKit

public class ClassUtils: NSObject {
    
    //MARK: - Basica Class
    
    static public func isBasicClass(any : Any) -> Bool {
        if any is String || any is NSNumber || any is Int || any is Float || any is Double || any is UInt || any is Bool || any is NSDictionary || any is NSString {
            return true
        }
        
        return false
    }
    
    static public func isBasicClassString(string: String) -> Bool {
        if string.equalsIgnoreCase("String") || string.equalsIgnoreCase("NSNumber") || string.equalsIgnoreCase("Int") || string.equalsIgnoreCase("Float") || string.equalsIgnoreCase("Double") || string.equalsIgnoreCase("UInt") || string.equalsIgnoreCase("Bool") || string.equalsIgnoreCase("NSDictionary") || string.equalsIgnoreCase("NSString") {
            return true
        }
        
        return false
    }

    //MARK: - Class From String
    
    static public func swiftClassFromString(className: String) -> NSObject? {
        var result: NSObject? = nil
        if className == "NSObject" {
            return NSObject()
        }
        if let anyobjectype : AnyObject.Type = swiftClassTypeFromString(className) {
            if let nsobjectype : NSObject.Type = anyobjectype as? NSObject.Type {
                let nsobject: NSObject = nsobjectype.init()
                result = nsobject
            }
        }
        return result
    }
    
    static public func swiftClassTypeFromString(className: String, namespace: String? = nil) -> AnyClass? {
        if className.hasPrefix("_Tt") {
            return NSClassFromString(className)
        }
        var classStringName = className
        if className.rangeOfString(".", options: NSStringCompareOptions.CaseInsensitiveSearch) == nil {
            if let namespace = namespace {
                classStringName = "\(namespace).\(className)"
                if let cls = NSClassFromString(classStringName) {
                    return cls
                } else {
                    let appName = getCleanAppName()
                    classStringName = "\(appName).\(className)"
                }
            }
        }
        return NSClassFromString(classStringName)
    }
    
    static public func getCleanAppName(forObject: NSObject? = nil)-> String {
        var bundle = NSBundle.mainBundle()
        if forObject != nil {
            bundle = NSBundle(forClass: forObject!.dynamicType)
        }
        
        var appName = bundle.infoDictionary?["CFBundleName"] as? String ?? ""
        if appName == "" {
            appName = (bundle.bundleIdentifier!).characters.split(isSeparator: {$0 == "."}).map({ String($0) }).last ?? ""
        }
        let cleanAppName = appName
            .stringByReplacingOccurrencesOfString(" ", withString: "_", options: NSStringCompareOptions.CaseInsensitiveSearch, range: nil)
            .stringByReplacingOccurrencesOfString("-", withString: "_", options: NSStringCompareOptions.CaseInsensitiveSearch, range: nil)
        return cleanAppName
    }
    
    //MARK: - String from Class
    
    static public func classNameAsString(obj: Any) -> String {
        if #available(iOS 9, *) {
            return String(reflecting: obj.dynamicType)
        } else {
            return String(obj.dynamicType).componentsSeparatedByString("__").last!
        }
    }
    
}
