//
//  TelefoneUtils.swift
//  Pods
//
//  Created by Livetouch-Mini01 on 06/07/2016.
//
//

import UIKit

public class TelefoneUtils: NSObject {
    
    //MARK: - Mask
    
    static public func unmask(phone: String?) -> String {
        guard let phone = phone where StringUtils.isNotEmpty(phone) else {
            return ""
        }
        
        var unmasked = phone
        unmasked = StringUtils.replace(unmasked, inQuery: " ", withReplacement: "")
        unmasked = StringUtils.replace(unmasked, inQuery: "_", withReplacement: "")
        return unmasked
    }
    
    static public func mask(phone: String?) -> String {
        guard let phone = phone where StringUtils.isNotEmpty(phone) else {
            return ""
        }
        
        let text = NSMutableString(string: phone)
        
        if (text.length > 1) {
            text.insertString(" ", atIndex: 2)
        }
        
        if ((text.length > 7) && (text.length < 12)) {
            text.insertString("_", atIndex: 7)
            
        } else {
            if (text.length >= 12) {
                text.insertString("_", atIndex: 8)
            }
        }
        
        if (text.length > 13) {
            return ""
        }
        
        return text as String
    }
    
    //MARK: - Validation
    
    static public func isValid(phone: String?) -> Bool {
        guard let phone = phone else {
            return false
        }
        
        let set = NSCharacterSet(charactersInString: "").invertedSet
        
        let components = phone.componentsSeparatedByCharactersInSet(set)
        let numberFiltered = components.joinWithSeparator("")
        
        return StringUtils.equals(phone, withString: numberFiltered)
    }
}
