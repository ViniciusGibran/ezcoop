//
//  TabBarUtils.swift
//  Pods
//
//  Created by Livetouch BR on 6/23/16.
//
//

import UIKit

public class TabBarUtils: NSObject {

    static public func getHeight(viewController:UIViewController) -> CGFloat{
        
        guard let tabBarController = viewController.tabBarController else {
            return 0.0
        }
        
        let height = tabBarController.tabBar.frame.size.height
        
        return height
    }
    
    static public func setTextColor(color:UIColor){
        UITabBarItem.appearance().setTitleTextAttributes([NSForegroundColorAttributeName: color], forState: .Normal)
    }

    static public func setSelectedTextColor(color:UIColor){
        UITabBarItem.appearance().setTitleTextAttributes([NSForegroundColorAttributeName: color], forState: .Selected)
    }
    
    static public func setBackgroundColor(color:UIColor){
        UITabBar.appearance().barTintColor = color
    }
    
    static public func setSelectedBackgroundColor(color:UIColor, onNumberOfTabs numberOfTabs:CGFloat){
        UITabBar.appearance().selectionIndicatorImage = ImageUtils.getImageFromColor(color, whitRect: CGRectMake(0, 0, DeviceUtils.getScreenWidth() / numberOfTabs , 49))
    }
}
