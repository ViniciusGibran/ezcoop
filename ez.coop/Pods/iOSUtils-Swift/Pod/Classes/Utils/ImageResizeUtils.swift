//
//  ImageResizeUtils.swift
//  Pods
//
//  Created by Guilherme Politta on 7/07/16.
//
//

import UIKit


//Nao esta sendo usando esse enum ainda, mas no futuro quero fazer
//com que voce possa escolher uma dessas opcoes, entao vou deixar aqui
public enum ResizeMode {
    case Automatic
    case FitToWidth
    case FitToHeight
}

public class ImageResizeUtils: NSObject {
    
    public static func imageWithImage(image: UIImage, maxWidthOrHeight max: CGFloat) -> UIImage {
        let newSize = getNewSizeForImage(image, maxWidthOrHeight: max)
        
        return imageWithImage(image, scaledToSize: newSize)
    }
    
    public static func imageWithImage(image: UIImage, scaledToSize newSize: CGSize) -> UIImage {
        UIGraphicsBeginImageContext(newSize)
        image.drawInRect(CGRectMake(0, 0, newSize.width, newSize.height))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage
    }
    
    public static func getNewSizeForImage(image: UIImage, maxWidthOrHeight max: CGFloat) -> CGSize {
        var size = CGSizeZero
        
        if (image.size.width > image.size.height) {
            size.width = max
            let propotion = image.size.height / image.size.width
            size.height = max * propotion
        } else {
            size.height = max
            let propotion = image.size.width / image.size.height
            size.width = max * propotion
        }
        
        return size
    }
}
