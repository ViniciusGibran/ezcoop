//
//  ImageUtils.swift
//  Pods
//
//  Created by Livetouch BR on 6/23/16.
//
//

import UIKit

public class ImageUtils: NSObject {
    
    static public func getOriginalImageWithName(name:String) -> UIImage?{
        guard let image = UIImage(named: name) else {
            return nil
        }
        
        return image.imageWithRenderingMode(.AlwaysOriginal)
    }
    
    static public func changeTintOfImageView(imageView:UIImageView, toColor color:UIColor){
        guard let image = imageView.image else {
            return
        }
        
        imageView.image = image.imageWithRenderingMode(.AlwaysTemplate)
        imageView.tintColor = color
    }
    
    static public func getImageFromColor(color:UIColor) -> UIImage{
        return self.getImageFromColor(color, whitRect: CGRectMake(0.0, 0.0, 1.0, 1.0))
    }

    static public func getImageFromColor(color:UIColor, whitRect rect:CGRect) -> UIImage{
        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()
        
        CGContextSetFillColorWithColor(context, color.CGColor)
        CGContextFillRect(context, rect)
        
        let image:UIImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return image
    }

    static public func resizeImage(image:UIImage, withMaxWidthOrHeight max:CGFloat) -> UIImage{
        let newSize = self.getNewSizeForImage(image, withMaxWidthOrHeight: max)
        
        return self.resizeImage(image, toSize: newSize)
    }

    static public func resizeImage(image:UIImage, toSize newSize:CGSize) -> UIImage{
        UIGraphicsBeginImageContext(newSize)
        image.drawInRect(CGRectMake(0, 0, newSize.width, newSize.height))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage
    }
    
    static public func getNewSizeForImage(image:UIImage, withMaxWidthOrHeight max:CGFloat) -> CGSize{
        var size:CGSize = CGSizeMake(0, 0)
        
        if(image.size.width > image.size.height){
            let propotion = image.size.height / image.size.width
            size.width = max
            size.height = max * propotion
        }else{
            let propotion = image.size.width / image.size.height
            size.height = max
            size.width = max * propotion
        }
        
        return size
    }
}
