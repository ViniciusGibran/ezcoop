//
//  StatusBarUtils.swift
//  Pods
//
//  Created by Livetouch-Mini01 on 20/06/2016.
//
//

import Foundation

public class StatusBarUtils: NSObject {
    
    //MARK: - Sizes
    
    static public func getHeight() -> CGFloat {
        let height = UIApplication.sharedApplication().statusBarFrame.size.height
        return height
    }
    
    //MARK: - Show & Hide
    
    static public func show() {
        UIApplication.sharedApplication().statusBarHidden = false
    }
    
    static public func hide() {
        UIApplication.sharedApplication().statusBarHidden = true
    }
    
    static public func isHidden() -> Bool {
        return UIApplication.sharedApplication().statusBarHidden
    }
    
    //MARK: - Text Color
    
    static public func setTextColorToDefault() {
        UIApplication.sharedApplication().setStatusBarStyle(.Default, animated: true)
    }
    
    static public func setTextColorToWhite() {
        UIApplication.sharedApplication().setStatusBarStyle(.LightContent, animated: true)
    }
    
    //MARK: - Background Color
    
    static public func setBackgroundToColor(color: UIColor) {
        let view = UIView(frame: CGRectMake(0, 0, DeviceUtils.getScreenWidth(), 20))
        view.backgroundColor = color
        
        let window = UIApplication.sharedApplication().keyWindow
        if let window = window {
            window.rootViewController?.view.addSubview(view)
        }
    }
}
