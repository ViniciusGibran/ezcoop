//
//  NavigationBarUtils.swift
//  Pods
//
//  Created by Livetouch-Mini01 on 21/06/2016.
//
//

import UIKit

public class NavigationBarUtils: NSObject {
    
    //MARK: - Get
    
    static public func get(viewController: UIViewController) -> UINavigationBar? {
        guard let navigationController = viewController.navigationController else {
            return nil
        }
        
        return navigationController.navigationBar
    }
    
    //MARK: - Information
    
    static public func getHeight(viewController: UIViewController) -> CGFloat {
        guard let navigationBar = get(viewController) else {
            return 0.0
        }
        
        NavigationBarUtils.show(viewController)
        
        let height = navigationBar.frame.height
        return height
    }
    
    //MARK: - Show/Hide
    
    /**
     *  Método para mostrar a UINavigationBar no UIViewController passado.
     *
     *  - parameter viewController O UIViewController em que se deseja mostrar a navigation bar.
     *
     */
    static public func show(viewController: UIViewController) {
        guard let navigationBar = get(viewController) else {
            return
        }
        
        navigationBar.hidden = false
    }
    
    static public func hide(viewController: UIViewController) {
        guard let navigationBar = get(viewController) else {
            return
        }
        
        navigationBar.hidden = true
    }
    
    static public func hideBorder(viewController: UIViewController) {
        guard let navigationBar = get(viewController) else {
            return
        }
        
        navigationBar.setBackgroundImage(UIImage(), forBarPosition: .Any, barMetrics: .Default)
        navigationBar.shadowImage = UIImage()
    }
    
    //MARK: - Layout
    
    static public func setTranslucent(viewController: UIViewController) {
        guard let navigationBar = get(viewController) else {
            return
        }
        
        navigationBar.translucent = true
        navigationBar.opaque = false
    }
    
    static public func setOpaque(viewController: UIViewController) {
        guard let navigationBar = get(viewController) else {
            return
        }
        
        navigationBar.translucent = false
        navigationBar.opaque = true
    }
    
    static public func setTitle(title: String, forViewController viewController: UIViewController) {
        viewController.navigationItem.title = title
    }
    
    //MARK: - Back Button
    
    static public func setBackBarButtonWithTitle(title: String, forViewController viewController: UIViewController) {
        let backButton = UIBarButtonItem(title: title, style: .Plain, target: nil, action: nil)
        viewController.navigationItem.backBarButtonItem = backButton
    }
    
    static public func setBackBarButtonWithImage(image: UIImage?, forViewController viewController: UIViewController) {
        guard let navigationBar = get(viewController) else {
            return
        }
        
        navigationBar.backIndicatorImage = image
        navigationBar.backIndicatorTransitionMaskImage = image
        
        viewController.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
    }
    
    //MARK: - Left Button
    
    static public func setLeftBarButton(object: AnyObject?, withTarget target: UIViewController, andAction action: Selector) {
        var leftButton : UIBarButtonItem?
        
        if let title = object as? String {
            leftButton = UIBarButtonItem(title: title, style: .Plain, target: target, action: action)
        } else if let image = object as? UIImage {
            leftButton = UIBarButtonItem(image: image, style: .Plain, target: target, action: action)
        }
        
        if let leftButton = leftButton {
            target.navigationItem.leftBarButtonItem = leftButton
        }
    }
    
    static public func setLeftSystemButton(systemItem: UIBarButtonSystemItem, onTarget target: UIViewController, andAction action: Selector) {
        let leftButton = UIBarButtonItem(barButtonSystemItem: systemItem, target: target, action: action)
        target.navigationItem.leftBarButtonItem = leftButton
    }
    
    static public func setLeftImage(image: UIImage?, onViewController viewController: UIViewController) {
        guard let image = image else {
            return
        }
        
        let leftButton = UIBarButtonItem(image: image.imageWithRenderingMode(.AlwaysOriginal), style: .Plain, target: nil, action: nil)
        leftButton.enabled = false
        
        viewController.navigationItem.leftBarButtonItem = leftButton
    }
    
    //MARK: - Right Button
    
    static public func setRightBarButton(object: AnyObject?, withTarget target: UIViewController, andAction action: Selector) {
        var rightButton : UIBarButtonItem?
        
        if let title = object as? String {
            rightButton = UIBarButtonItem(title: title, style: .Plain, target: target, action: action)
        } else if let image = object as? UIImage {
            rightButton = UIBarButtonItem(image: image, style: .Plain, target: target, action: action)
        }
        
        if let rightButton = rightButton {
            target.navigationItem.rightBarButtonItem = rightButton
        }
    }
    
    static public func setRightSystemButton(systemItem: UIBarButtonSystemItem, onTarget target: UIViewController, andAction action: Selector) {
        let rightButton = UIBarButtonItem(barButtonSystemItem: systemItem, target: target, action: action)
        target.navigationItem.rightBarButtonItem = rightButton
    }
    
    static public func setRightImage(image: UIImage?, onViewController viewController: UIViewController) {
        guard let image = image else {
            return
        }
        
        let rightButton = UIBarButtonItem(image: image.imageWithRenderingMode(.AlwaysOriginal), style: .Plain, target: nil, action: nil)
        rightButton.enabled = false
        
        viewController.navigationItem.rightBarButtonItem = rightButton
    }
}