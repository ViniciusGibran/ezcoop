//
//  iOSUtils.swift
//  Pods
//
//  Created by Livetouch on 21/06/16.
//
//

import UIKit
import SystemConfiguration
import AudioToolbox

/*
 *  Não adicionar novos métodos nesta classe.
 *  Além de ser uma classe muito genérica (iOSUtils), já existem classes que fazem algumas dessas funcionalidades.
 *  Ex.: AppUtils, NetworkUtils, OrientationUtils etc.
 *
 *  Os métodos aqui implementados que não se encontram em outras classes, devem ser passados para outras classes. Caso necessário, crie uma nova classe. O nome fica a seu critério e risco.
 */

public class iOSUtils: NSObject {
    
    public static func getUDID() throws -> String {
        throw Exception.DomainException(message: "Usar DeviceUtils.getUUID()")
//        let newUniqueID = CFUUIDCreate(kCFAllocatorDefault)
//        let newUniqueIDString = CFUUIDCreateString(kCFAllocatorDefault, newUniqueID);
//        let guid = newUniqueIDString as String
//        
//        return guid.lowercaseString
    }
    
    public static func getVersionCode() throws -> String {
        throw Exception.DomainException(message: "usar AppUtils.getVersion()")
//        if let version = NSBundle.mainBundle().infoDictionary?["CFBundleShortVersionString"] as? String {
//            return version
//        }
//        return ""
    }
    
    /**
    * Retorna se existe conexão com a internet ou não
    */
    public class func isNetworkAvailable() -> Bool {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(sizeofValue(zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        let defaultRouteReachability = withUnsafePointer(&zeroAddress) {
            SCNetworkReachabilityCreateWithAddress(nil, UnsafePointer($0))
        }
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        return (isReachable && !needsConnection)
    }
    
    public static func vibrar() throws {
        throw Exception.DomainException(message: "Usar DeviceUtils.vibrate()")
//        AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
    }
    
    public static func isHorizontal() throws -> Bool {
        throw Exception.DomainException(message: "Usar OrientationUtils.isLandscape()")
//        return (UIDevice.currentDevice().orientation == UIDeviceOrientation.LandscapeLeft) || (UIDevice.currentDevice().orientation == UIDeviceOrientation.LandscapeRight)
    }
    
    public static func isVertical() throws -> Bool {
        throw Exception.DomainException(message: "Usar OrientationUtils.isPortrait()")
//        return (UIDevice.currentDevice().orientation == UIDeviceOrientation.Portrait) || (UIDevice.currentDevice().orientation == UIDeviceOrientation.PortraitUpsideDown)
    }
    
    

}
