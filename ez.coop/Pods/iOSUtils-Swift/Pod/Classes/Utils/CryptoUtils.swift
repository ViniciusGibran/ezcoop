//
//  CryptoUtils.swift
//  Pods
//
//  Created by Livetouch-Mini01 on 29/06/2016.
//
//

import UIKit

import CryptoSwift

public class CryptoUtils: NSObject {
    
    //MARK: - AES
    
    static public func toAES(stringToEncrypt: String?, withKey key: String, andBlockMode blockMode: BlockMode = .ECB) -> String {
        guard let stringToEncrypt = stringToEncrypt else {
            return ""
        }
        
        do {
            guard let input = stringToEncrypt.dataUsingEncoding(NSUTF8StringEncoding) else {
                return ""
            }
            
            let encrypted = try input.encrypt(AES(key: Array(key.utf8), blockMode: blockMode))
            
            if let result = String(data: encrypted, encoding: NSUTF8StringEncoding) {
                return result
            }
        } catch {
            LogUtils.log("AES Error.")
        }
        
        return ""
    }
    
    static public func fromAES(stringToDecrypt: String?, withKey key: String, andBlockMode blockMode: BlockMode = .ECB) -> String {
        guard let stringToDecrypt = stringToDecrypt else {
            return ""
        }
        
        do {
            guard let input = stringToDecrypt.dataUsingEncoding(NSUTF8StringEncoding) else {
                return ""
            }
            
            let decrypted = try input.decrypt(AES(key: Array(key.utf8), blockMode: blockMode))
            
            if let result = String(data: decrypted, encoding: NSUTF8StringEncoding) {
                return result
            }
        } catch {
            LogUtils.log("AES Error.")
        }
        
        return ""
    }
    
    static public func toSHA256(stringToEncrypt: String?, withKey key: String) -> String {
        guard let stringToEncrypt = stringToEncrypt else {
            LogUtils.log("SHA-256 Error. String nula.")
            return ""
        }
        
        let encrypted = stringToEncrypt.sha256()
        return encrypted
    }
}
