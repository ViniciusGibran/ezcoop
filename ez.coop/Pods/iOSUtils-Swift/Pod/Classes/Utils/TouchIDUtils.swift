//
//  TouchIDUtils.swift
//  Pods
//
//  Created by Livetouch-Mini01 on 04/07/2016.
//
//

import UIKit

import LocalAuthentication

public class TouchIDUtils: NSObject {
    
    ///Não esquecer importar o módulo LocalAuthentication para usar o método scanFingerprint.
    
    //MARK: - Evaluation
    
    static private func canEvaluateWithContext(context: LAContext) throws -> Bool {
        var error : NSError?
        
        if !context.canEvaluatePolicy(LAPolicy.DeviceOwnerAuthenticationWithBiometrics, error: &error) {
            if let error = error as? LAError {
                throw error
            }
            
            return false
        }
        
        return true
    }
    
    static public func evaluateWithContext(context: LAContext, withTitle title: String = "Testing Touch ID", onSuccess: (() -> Void), andOnError onError: ((error: LAError) -> Void)?) {
        context.evaluatePolicy(LAPolicy.DeviceOwnerAuthenticationWithBiometrics, localizedReason: title, reply: { (success: Bool, error:NSError?) in
            
            if success {
                dispatch_async(dispatch_get_main_queue(), {
                    onSuccess()
                })
            } else if let error = error as? LAError {
                onError?(error: error)
            }
        })
    }
    
    //MARK: - Hardware
    
    /**
     *  Retorna se o dispositivo possui a funcionalidade Touch ID.
     */
    static public func isAvailable() -> Bool {
        do {
            return try canEvaluateWithContext(LAContext())
            
        } catch let error as LAError {
            if (error == LAError.TouchIDNotAvailable) {
                return false
            }
        } catch {}
        
        return true
    }
    
    //MARK: - Register
    
    /**
     *  Retorna se o usuário já registrou alguma digital no dispositivo.
     */
    static public func isFingerprintEnrolled() -> Bool {
        do {
            return try canEvaluateWithContext(LAContext())
            
        } catch let error as LAError {
            if (error == LAError.TouchIDNotEnrolled) {
                return false
            }
        } catch {}
        
        return true
    }
    
    //MARK: - Scan
    
    static public func scanFingerprint(onSuccess: (() -> Void), andOnError onError: ((error: LAError) -> Void)? = nil) {
        let context = LAContext()
        
        do {
            if (try canEvaluateWithContext(context)) {
                evaluateWithContext(context, onSuccess: onSuccess, andOnError: onError)
            }
        } catch {
            if let error = error as? LAError {
                onError?(error: error)
            }
        }
    }
}
