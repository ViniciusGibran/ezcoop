//
//  ImageMetadataUtils.swift
//  Pods
//
//  Created by Guilherme Politta on 6/07/16.
//
//

import UIKit
import SimpleExif
import CoreLocation

public class ImageMetadataUtils: NSObject {

    //MARK: - Helpers
    
    public func getCurrentLocation() throws -> CLLocation {
        let gpsUtils = GPSUtils()
        let location = try gpsUtils.getCurrentLocation()
        
        return location
    }
    
    //MARK: - Location
    
    public func writeCurrentLocationInfoOnImage(image: UIImage) throws -> NSData {
        return try writeLocationInfo(getCurrentLocation(), onImage: image)
    }
    
    public func writeLocationInfo(location: CLLocation, onImage image: UIImage) throws -> NSData {
        let container: ExifContainer = ExifContainer()
        container.addLocation(location)
        
        guard let imageData = image.addExif(container) where imageData.length > 0 else {
            throw NSError(domain: "ImageMetadataUtilsDomain", code: 0, userInfo: [NSLocalizedDescriptionKey : "Erro ao adicionar informações de GPS na imagem"])
        }
        
        return imageData
    }
    
    //MARK: - Other Metadata
    
    public func writeMetadatas(metadatas: [String], onImage image: UIImage) throws -> NSData {
        let metadata = metadatas.joinWithSeparator("\n")
        
        let container: ExifContainer = ExifContainer()
        container.addUserComment(metadata)
        
        guard let imageData = image.addExif(container) where imageData.length > 0 else {
            throw NSError(domain: "ImageMetadataUtilsDomain", code: 0, userInfo: [NSLocalizedDescriptionKey : "Erro ao adicionar metadados na imagem"])
        }
        
        return imageData
    }
    
    //MARK: - Location+Other Metadata
    
    public func writeCurrentLocationAndOthersMetadatas(metadatas: [String], onImage image: UIImage) throws -> NSData {
        return try writeLocation(getCurrentLocation(), andOthersMetadatas: metadatas, onImage: image)
    }
    
    public func writeLocation(location: CLLocation, andOthersMetadatas metadatas: [String], onImage image: UIImage) throws -> NSData {
        
        let metadata = metadatas.joinWithSeparator("\n")
        
        let container: ExifContainer = ExifContainer()
        container.addUserComment(metadata)
        container.addLocation(location)
        
        guard let imageData = image.addExif(container) where imageData.length > 0 else {
            throw NSError(domain: "ImageMetadataUtilsDomain", code: 0, userInfo: [NSLocalizedDescriptionKey : "Erro ao adicionar metadados na imagem"])
        }
        
        return imageData
    }
}
