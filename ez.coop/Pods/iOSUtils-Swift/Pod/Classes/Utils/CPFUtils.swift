//
//  CPFUtils.swift
//  Pods
//
//  Created by Livetouch-Mini01 on 21/06/2016.
//
//

import UIKit

public class CPFUtils: NSObject {
    
    //MARK: - Mask
    
    static public func unmask(string: String?) -> String {
        guard let string = string else {
            return ""
        }
        
        return string.stringByReplacingOccurrencesOfString(".", withString: "").stringByReplacingOccurrencesOfString("-", withString: "").stringByReplacingOccurrencesOfString("/", withString: "").stringByReplacingOccurrencesOfString("(", withString: "").stringByReplacingOccurrencesOfString(")", withString: "")
    }
    
    static public func mask(string: String?) -> String {
        guard let string = string where string.isNotEmpty else {
            return ""
        }
        
        let cpf = unmask(string)
        if (cpf.length != 11) {
            return ""
        }
        
        let cpfMasked = cpf.substringFromIndex(0, toIndex: 2) + "." + cpf.substringFromIndex(3, toIndex: 5) + "." + cpf.substringFromIndex(6, toIndex: 8) + "-" + cpf.substringFromIndex(9, toIndex: 10)
        
        return cpfMasked
    }
    
    static public func isMasked(string: String?) -> Bool {
        if let string = string {
            return string.containsString(".")
        }
        return false
    }
    
    //MARK: - Validation
    
    static public func isValid(string: String?) -> Bool {
        guard let string = string where string.isNotEmpty else {
            return false
        }
        
        let cpf = CPFUtils.unmask(string)
        if (cpf.length != 11) {
            return false
        }
        
        var primeiroDigitoVerificador = ""
        var segundoDigitoVerificador = ""
        
        var j = 0
        var sum = 0
        var result = 0
        
        for weight in 10.stride(to: 1, by: -1) {
            let substring = cpf[cpf.startIndex.advancedBy(j)]
            let segment = String(substring).utf8
            
            sum += weight * ("\(segment)".integerValue)
            j += 1
        }
        
        result = 11 - (sum % 11)
        if (result == 10 || result == 11) {
            primeiroDigitoVerificador = "0"
        } else {
            primeiroDigitoVerificador.append(UnicodeScalar(result + 48))
        }
        
        let penultimoDigito = cpf.substringWithRange(cpf.startIndex.advancedBy(9)...cpf.endIndex.advancedBy(-2))
        if (!primeiroDigitoVerificador.equalsIgnoreCase(penultimoDigito)) {
            return false
        }
        
        j = 0
        sum = 0
        
        for weight in 11.stride(to: 1, by: -1) {
            let substring = cpf[cpf.startIndex.advancedBy(j)]
            let segment = String(substring).utf8
            
            sum += weight * ("\(segment)".integerValue)
            j += 1
        }
        
        result = 11 - (sum % 11)
        if (result == 10 || result == 11) {
            segundoDigitoVerificador = "0"
        } else {
            segundoDigitoVerificador.append(UnicodeScalar(result + 48))
        }
        
        let ultimoDigito = cpf.substringWithRange(cpf.startIndex.advancedBy(10)...cpf.endIndex.advancedBy(-1))
        if (!segundoDigitoVerificador.equalsIgnoreCase(ultimoDigito)) {
            return false
        }
        
        return true
    }
}
