//
//  PlacaUtils.swift
//  Pods
//
//  Created by Livetouch-Mini01 on 06/07/2016.
//
//

import UIKit

public class PlacaUtils: NSObject {
    
    //MARK: - Mask
    
    static public func unmask(placa: String?) -> String {
        return StringUtils.replace(placa, inQuery: "-", withReplacement: "")
    }
    
    static public func mask(placa: String?) -> String {
        guard let placa = placa where StringUtils.isNotEmpty(placa) else {
            return ""
        }
        
        let unmasked = unmask(placa)
        if (unmasked.length != 7) {
            return ""
        }
        
        let masked = "\(placa.substringFromIndex(0, toIndex: 3))-\(placa.substringFromIndex(3, toIndex: 7))"
        return StringUtils.toUpperCase(masked)
    }
    
    static public func isMasked(placa: String?) -> Bool {
        guard let placa = placa where StringUtils.isNotEmpty(placa) else {
            return false
        }
        
        if (placa.length != 8) {
            return false
        }
        
        return StringUtils.contains(placa, fromQuery: "-")
    }
    
    //MARK: - Validation
    
    static public func isValid(placa: String?) -> Bool {
        guard let placa = placa where StringUtils.isNotEmpty(placa) else {
            return false
        }
        
        let unmasked = unmask(placa)
        if (unmasked.length != 7) {
            return false
        }
        
        let letras = placa.substringFromIndex(0, toIndex: 3)
        if (StringUtils.isNotLetters(letras)) {
            return false
        }
        
        let numeros = placa.substringFromIndex(3, toIndex: 7)
        if (StringUtils.isNotDigits(numeros)) {
            return false
        }
        
        return true
    }
}
