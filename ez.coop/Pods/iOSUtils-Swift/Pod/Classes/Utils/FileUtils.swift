//
//  FileUtils.swift
//  Pods
//
//  Created by Livetouch-Mini01 on 15/06/2016.
//
//

import UIKit

public class FileUtils: NSObject {
    
    /*
     *  Para o uso dessa classe é importante saber as diferenças entre o Main Bundle e o diretório Documents.
     *  O Main Bundle contém os arquivos que são enviados junto com o projeto. Arquivos no Main Bundle são read-only (possuem somente permissão de leitura). Até o iOS 5, atualizações no aplicativo causavam substituição de todos os arquivos no Main Bundle. A partir do iOS 6, atualizações no aplicativo causam substituição somente dos arquivos alterados no Main Bundle.
     *  O diretório Documents contém os arquivos baixados pelo aplicativo, por exemplo pela câmera ou pela Internet. Arquivos no Documents são read-write (possuem permissão de leitura e escrita). Um aplicativo recém-instalado já cria um diretório Documents para uso, começando com espaço utilizado de 0 (zero) MB. Atualizações no aplicativo não afetam os arquivos armazenados no Documents.
     */
    
    //MARK: - Paths
    
    static public func getDocumentsPath(domainMask: NSSearchPathDomainMask = .UserDomainMask) throws -> String {
        let documentPaths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, domainMask, true)
        if documentPaths.isEmpty {
            throw Exception.FileNotFoundException
        }
        
        let documentDirectory = documentPaths[0]
        return documentDirectory
    }
    
    static public func getPathOfDocumentFile(filename: String, withDomainMask domainMask: NSSearchPathDomainMask = .UserDomainMask) throws -> String {
        if filename.isEmpty {
            throw Exception.FileNotFoundException
        }
        
        let documentPath = try getDocumentsPath(domainMask)
        
        let filePath = documentPath.stringByAppendingPathComponent(filename)
        return filePath
    }
    
    static public func getPathOfBundleFile(filename: String, ofType type: String?) throws -> String {
        if let path = NSBundle.mainBundle().pathForResource(filename, ofType: type) {
            return path
        }
        throw Exception.FileNotFoundException
    }
    
    static public func getPathOfPodBundleFile(filename: String, ofType type: String?, atPod podBundle: NSBundle) throws -> String {
        if let path = podBundle.pathForResource(filename, ofType: type) {
            return path
        }
        throw Exception.FileNotFoundException
    }
    
    static public func getResourcePathOfFile(filename: String) throws -> String {
        guard let resourcePath = NSBundle.mainBundle().resourcePath else {
            throw Exception.FileNotFoundException
        }
        
        let path = resourcePath.stringByAppendingPathComponent(filename)
        return path
    }
    
    //MARK: - Copying Files
    
    static public func copyFileFromBundleToDocuments(filename: String) throws {
        let fileManager = NSFileManager.defaultManager()
        
        let filePath = try getPathOfDocumentFile(filename)
        
        let exists = fileManager.fileExistsAtPath(filePath)
        if exists {
            try removeFileFromDocuments(filename)
        }
        
        let filePathFromApp = try getResourcePathOfFile(filename)
        
        do {
            try fileManager.copyItemAtPath(filePathFromApp, toPath: filePath)
            
        } catch {
            LogUtils.log("Failed to copy '\(filename)' from bundle to documents.")
            throw Exception.FileNotFoundException
        }
    }
    
    static public func copyFilesFromBundleToDocuments(filenames: [String]) throws {
        if filenames.isEmpty {
            throw Exception.FileNotFoundException
        }
        
        for filename in filenames {
            try copyFileFromBundleToDocuments(filename)
        }
    }
    
    //MARK: - Bundle
    
    //MARK: Get
    
    static public func getBundleFile(filename: String, ofType type: String?) throws -> NSData {
        let filePath = try getPathOfBundleFile(filename, ofType: type)
        if let fileData = NSData(contentsOfFile: filePath) {
            return fileData
        }
        throw Exception.FileNotFoundException
    }
    
    //MARK: Read
    
    static public func readBundleFileWithName(filename: String, ofType type: String?, withEncoding encoding: NSStringEncoding = NSUTF8StringEncoding) throws -> String {
        let path = try getPathOfBundleFile(filename, ofType: type)
        return try readBundleFileAtPath(path)
    }
    
    static public func readBundleFileAtPath(filePath: String, withEncoding encoding: NSStringEncoding = NSUTF8StringEncoding) throws -> String {
        do {
            let text = try String(contentsOfFile: filePath, encoding: encoding)
            return text
            
        } catch {
            throw Exception.GenericException
        }
    }
    
    //MARK: - Lib
    
    static public func getLibBundle() -> NSBundle {
        return NSBundle(forClass: self.classForCoder())
    }
    
    //MARK: Get
    
    static public func getLibFile(filename: String, ofType type: String?) throws -> NSData {
        let libBundle = getLibBundle()
        let filePath = try getPathOfPodBundleFile(filename, ofType: type, atPod: libBundle)
        if let fileData = NSData(contentsOfFile: filePath) {
            return fileData
        }
        throw Exception.FileNotFoundException
    }
    
    //MARK: - Documents
    
    //MARK: Exists
    
    static public func fileExistsInDocuments(filename: String) -> Bool {
        do {
            let path = try getPathOfDocumentFile(filename)
            return NSFileManager.defaultManager().fileExistsAtPath(path)
            
        } catch {
            return false
        }
    }
    
    static public func filePathExistsInDocuments(filePath: String) -> Bool {
        return NSFileManager.defaultManager().fileExistsAtPath(filePath)
    }
    
    //MARK: Get
    
    static public func getDocumentsFile(filename: String) throws -> NSData {
        let path = try getPathOfDocumentFile(filename)
        let data = try getDocumentsFileAtPath(path)
        return data
    }
    
    static public func getDocumentsFileAtPath(filePath: String) throws -> NSData {
        if let data = NSData(contentsOfFile: filePath) {
            return data
        }
        throw Exception.GenericException
    }
    
    //MARK: Read
    
    static public func readDocumentsFile(filename: String, withEncoding encoding: NSStringEncoding = NSUTF8StringEncoding) throws -> String {
        let path = try getPathOfDocumentFile(filename, withDomainMask: .AllDomainsMask)
        let text = try readDocumentsFileAtPath(path, withEncoding: encoding)
        return text
    }
    
    static public func readDocumentsFileAtPath(filePath: String, withEncoding encoding: NSStringEncoding = NSUTF8StringEncoding) throws -> String {
        do {
            let text = try String(contentsOfFile: filePath, encoding: encoding)
            return text
        } catch {
            throw Exception.FileNotFoundException
        }
    }
    
    //MARK: Write
    
    static public func writeInDocuments(text: String, onFile filename: String, withEncoding encoding: NSStringEncoding = NSUTF8StringEncoding) throws {
        let path = try getPathOfDocumentFile(filename, withDomainMask: .AllDomainsMask)
        try writeInDocuments(text, onFileAtPath: path, withEncoding: encoding)
    }
    
    static public func writeInDocuments(text: String, onFileAtPath filePath: String, withEncoding encoding: NSStringEncoding = NSUTF8StringEncoding) throws {
        do {
            try text.writeToFile(filePath, atomically: false, encoding: encoding)
        } catch {
            throw Exception.GenericException
        }
    }
    
    //MARK: Append
    
    static public func appendText(text: String, onFile filename: String, ofType type: String?) throws {
        var fullFileName = ""
        if let type = type {
            fullFileName = "\(filename).\(type)"
        } else {
            fullFileName = filename
        }
        
        let currentText = try readDocumentsFile(fullFileName)
        let newText = "\(currentText)\n\(text)"
        try writeInDocuments(newText, onFile: fullFileName)
    }
    
    static public func appendText(text: String, onFileAtPath filePath: String) throws {
        let currentText = try readDocumentsFileAtPath(filePath)
        let newText = "\(currentText)\n\(text)"
        try writeInDocuments(newText, onFileAtPath: filePath)
    }
    
    //MARK: Remove
    
    static public func removeFileFromDocumentsAtPath(filePath: String) throws {
        do {
            try NSFileManager.defaultManager().removeItemAtPath(filePath)
            
        } catch {
            LogUtils.log("Failed to remove '\(filePath)' from documents.")
            throw Exception.GenericException
        }
    }
    
    static public func removeFileFromDocuments(filename: String) throws {
        let filePath = try getPathOfDocumentFile(filename)
        try removeFileFromDocumentsAtPath(filePath)
    }
    
    static public func removeFilesFromDocuments(filenames: [String]) throws {
        if filenames.isEmpty {
            throw Exception.FileNotFoundException
        }
        
        for filename in filenames {
            try removeFileFromDocuments(filename)
        }
    }
    
    //MARK: - Directories
    
    static public func createDirectoriesAtPath(path: String) throws {
        let exists = fileExistsInDocuments(path)
        if !exists {
            do {
                try NSFileManager.defaultManager().createDirectoryAtPath(path, withIntermediateDirectories: true, attributes: nil)
            } catch {
                throw Exception.GenericException
            }
        }
    }
}