//
//  DeviceUtils.swift
//  Pods
//
//  Created by Livetouch-Mini01 on 17/06/2016.
//
//

import Foundation

import AudioToolbox

public class DeviceUtils: NSObject {
    
    //MARK: - Helpers
    
    static private func getDigitsOnlyPhoneNumber(phoneNumber: String) -> String {
        let numberArray = phoneNumber.componentsSeparatedByCharactersInSet(NSCharacterSet.decimalDigitCharacterSet().invertedSet)
        let number = numberArray.joinWithSeparator("")
        return number
    }
    
    //MARK: - Actions
    
    static public func call(phoneNumber: String) {
        let number = getDigitsOnlyPhoneNumber(phoneNumber)
        if let nsurl = NSURL(string: "tel://\(number)") {
            UIApplication.sharedApplication().openURL(nsurl)
        }
    }
    
    static public func sms(phoneNumber: String) {
        let number = getDigitsOnlyPhoneNumber(phoneNumber)
        if let nsurl = NSURL(string: "sms://\(number)") {
            UIApplication.sharedApplication().openURL(nsurl)
        }
    }
    
    static public func openSettings() {
        if let nsurl = NSURL(string: UIApplicationOpenSettingsURLString) {
            UIApplication.sharedApplication().openURL(nsurl)
        }
    }
    
    static public func vibrate() {
        AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
    }
    
    //MARK: - Information
    
    /**
     * Retorna o UDID para identificar o celular
     */
    static public func getUUID() -> String {
        let uuid = NSUUID().UUIDString
        return uuid
    }
    
    static public func getPlatformCode() -> String {
        var systemInfo = utsname()
        uname(&systemInfo)
        
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let platform = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8 where value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        
        return platform
    }
    
    static public func getName() -> String {
        let model = getPlatformCode()
        let version = "\(getVersion())"
        let uniqueIdentifier = getUUID()
        
        var name = "\(model)_\(version)_\(uniqueIdentifier)"
        name = StringUtils.replace(name, inQuery: ",", withReplacement: ".")
        name = StringUtils.replace(name, inQuery: "-", withReplacement: "_")
        
        return name
    }
    
    //MARK: - Clipboard
    
    static public func copyToClipboard(text: String) {
        let pasteboard = UIPasteboard.generalPasteboard()
        pasteboard.string = text
    }
    
    //MARK: - Sizes
    
    static public func getScreenWidth() -> CGFloat {
        return UIScreen.mainScreen().bounds.width
    }
    
    static public func getScreenHeight() -> CGFloat {
        return UIScreen.mainScreen().bounds.height
    }
    
    //MARK: - Scales
    
    static public func getScreenScale() -> CGFloat {
        return UIScreen.mainScreen().scale
    }
    
    static public func isNormalDisplay() -> Bool {
        let version = getVersion()
        if (version < 4.0) {
            return false
        }
        
        let scale = getScreenScale()
        return scale == 1.0
    }
    
    static public func isRetinaDisplay() -> Bool {
        let version = getVersion()
        if (version < 4.0) {
            return false
        }
        
        let scale = getScreenScale()
        return scale == 2.0
    }
    
    static public func isHDDisplay() -> Bool {
        let version = getVersion()
        if (version < 4.0) {
            return false
        }
        
        let scale = getScreenScale()
        return scale == 3.0
    }
    
    //MARK: - Model
    
    static public func getModel() -> String  {
        return UIDevice.currentDevice().model
    }
    
    static public func isSimulator() -> Bool {
        return TARGET_OS_SIMULATOR != 0
    }
    
    static public func isIphone() -> Bool {
        let idiom = UIDevice.currentDevice().userInterfaceIdiom
        return idiom == .Phone
    }
    
    static public func isIpad() -> Bool {
        let idiom = UIDevice.currentDevice().userInterfaceIdiom
        return idiom == .Pad
    }
    
    static public func isIphone4() -> Bool {
        if isIpad() {
            return false
        }
        
        let screenHeight = getScreenHeight()
        return screenHeight == 480.0
    }
    
    static public func isIphone5() -> Bool {
        if isIpad() {
            return false
        }
        
        let screenHeight = getScreenHeight()
        return screenHeight == 568.0
    }
    
    static public func isIphone6() -> Bool {
        if isIpad() {
            return false
        }
        
        let screenHeight = getScreenHeight()
        return screenHeight == 667.0
    }
    
    static public func isIphone6Plus() -> Bool {
        if isIpad() {
            return false
        }
        
        let screenHeight = getScreenHeight()
        return screenHeight == 736.0
    }
    
    //MARK: - Version
    
    static public func getVersion() -> Float {
        return UIDevice.currentDevice().systemVersion.floatValue
    }
    
    static public func isIOS8() -> Bool {
        let version = getVersion()
        return version >= 8.0
    }
    
    static public func isIOS9() -> Bool {
        let version = getVersion()
        return version >= 9.0
    }
    
    static public func isIOS10() -> Bool {
        let version = getVersion()
        return version >= 10.0
    }
}