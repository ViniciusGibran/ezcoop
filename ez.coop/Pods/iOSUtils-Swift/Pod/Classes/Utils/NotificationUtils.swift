//
//  BusUtils.swift
//  Pods
//
//  Created by Livetouch-Mini01 on 20/06/2016.
//
//

import Foundation
import UIKit

public class NotificationUtils {
    
    //MARK: - Register
    
    static public func registerNotification(notificationName: String, withSelector selector: Selector, fromObserver observer: AnyObject) {
        NSNotificationCenter.defaultCenter().addObserver(observer, selector: selector, name: notificationName, object: nil)
    }
    
    //MARK: - Unregister
    
    static public func unregisterNotificationFromObserver(observer: AnyObject) {
        NSNotificationCenter.defaultCenter().removeObserver(observer)
    }
    
    //MARK: - Post
    
    static public func postNotification(notificationName: String, withObject object: AnyObject? = nil) {
        NSNotificationCenter.defaultCenter().postNotificationName(notificationName, object: object)
    }
    
    static public func postNotification(notification: NSNotification) {
        NSNotificationCenter.defaultCenter().postNotification(notification)
    }
}
