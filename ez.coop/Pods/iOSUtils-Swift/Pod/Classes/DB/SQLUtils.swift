//
//  SQLUtils.swift
//  SugarIOS
//
//  Created by Livetouch on 08/06/16.
//  Copyright © 2016 Livetouch. All rights reserved.
//

import UIKit

public class SQLUtils<T:Entity> : NSObject {
    
    static public func getDatabaseHelper() throws -> DatabaseHelper {
        if let baseAppDelegate = UIApplication.sharedApplication().delegate as? BaseAppDelegate {
            return try baseAppDelegate.getConfig().getDatabaseHelper()
        }
        
        throw Exception.NotImplemented
    }
    
    static public func getSQLDropTable(cls: AnyClass) -> String {
        let table = toSQLNameClass(cls);
        let drop = "drop table if exists \(table);"
        return drop
    }
    
    static public func getSQLCreateDatabase() throws -> [String] {
        
        let classes = try getDatabaseHelper().getDomainClasses()
        
        var sql : [String] = []
        
        for cls in classes {
            let sqlCreate = getSQLCreateTable(cls)
            sql.append(sqlCreate)
        }
        
        return sql
    }
    
    static public func getSQLDropDatabase() throws -> [String] {
        
        let classes = try getDatabaseHelper().getDomainClasses()
        
        var sql : [String] = []
        
        for c in classes {
            sql.append(getSQLDropTable(c))
        }
        
        return sql
    }
    
    static public func getSQLCreateTable(cls: AnyClass) -> String {
        
        let fields = ReflectionUtils.getFields(cls)
        
        let table = toSQLNameClass(cls)
        
        var sql = "create table if not exists \(table) (_id integer primary key autoincrement "
        
        var count = 0
        
        for f in fields {
            count+=1
            //Ignora o id, pois cria automaticamente (_id integer primary key autoincrement)
            if("id".equalsIgnoreCase(f.name)) {
                continue
            }
            sql.appendContentsOf("\(",")\(toSQLName(f.name)) \(getFieldType(f)) ")
        }
        
        sql.appendContentsOf(");")
        
        return sql
    }
    
    static public func getPatternFields(fields: [Field]) -> String {
        var pattern = ""
        var count = 0
        
        for f in fields {
            count+=1
            //Ignora o id, pois cria automaticamente (_id integer primary key autoincrement)
            if("id".equalsIgnoreCase(f.name)) {
                continue
            }
            pattern.appendContentsOf("\(count == 1 ? "" : ", ")\(toSQLName(f.name))\(getFieldType(f))\(count == fields.count ? ")" : "")")
        }
        
        return pattern
    }
    
    
    
    static public func toSQLNameClass(cls: AnyClass) -> String {
        let name = ReflectionUtils.getClassName(cls)
        return toSQLName(name)
    }
    
    static public func toSQLName(name: String) -> String {
        
        var count = 0;
        var nomePattern = ""
        let array = Array(name.characters)
        for character in array {
            let str = String(character)
            if str.lowercaseString != str && count > 0 {
                nomePattern.appendContentsOf("_\(str.lowercaseString)")
            } else {
                nomePattern.appendContentsOf(str.lowercaseString)
            }
            count+=1
        }
        return nomePattern
    }
    
    static public func getFieldType(field:Field) -> String {
        let type = field.type

        if("Int".equalsIgnoreCase(type)) {
            return "integer"
        } else if("String".equalsIgnoreCase(type)) {
            return "text"
        } else if("NSDate".equalsIgnoreCase(type)) {
            return "date"
        } else if("Double".equalsIgnoreCase(type)) {
            return "double"
        } else if("Long".equalsIgnoreCase(type)) {
            return "long"
        } else if("Bool".equalsIgnoreCase(type)) {
            return "integer"
        }
        return ""
    }
    
    // Salva um novo carro ou atualiza se já existe id
    static public func save(entity: T) throws -> Int {
        
        let id = entity.getId()
        
        let fields = ReflectionUtils.getFields(entity.classForCoder)
        
        if(id == 0 || id == -1) {
            // Insert
//            let sql = "insert or replace into carro (nome,desc,url_foto,url_info,url_video,latitude, longitude, tipo) VALUES (?,?,?,?,?,?,?,?);"
            var sql = "insert or replace into \(SQLUtils.toSQLNameClass(entity.classForCoder)) ("
            
            // (coluna_a, coluna_b, coluna_c)
            var first = true
            for f in fields {
                if("id".equalsIgnoreCase(f.name)) {
                    continue
                }
                if(!first) {
                    sql.appendContentsOf(",");
                }
                sql.appendContentsOf(SQLUtils.toSQLName(f.name))
                first = false
            }
            
            // VALUES
            sql.appendContentsOf(") VALUES (")
            first = true
            for f in fields {
                if("id".equalsIgnoreCase(f.name)) {
                    continue
                }
                if(!first) {
                    sql.appendContentsOf(",");
                }
                sql.appendContentsOf("?")
                first = false
            }
            sql.appendContentsOf(");")
     
            var params : [NSObject] = []
            for f in fields {
                if("id".equalsIgnoreCase(f.name)) {
                    continue
                }
                var value = ReflectionUtils.getFieldValue(entity, fieldName: f.name)
                
                if (f.type.equalsIgnoreCase("Bool")) {
                    value = value.isEqual("true") ? 1 : 0
                }
                
                params.append(value)
            }
            
            let cid = try execSql(sql, withParameters:params)
            
            let id = Int(cid)
            
            entity.id = id
            
            return id
            
        } else {
            let id = entity.getId()
            
            // Update
            var sql = "update \(SQLUtils.toSQLNameClass(entity.classForCoder)) set "
            
            var first = true
            for f in fields {
                if("id".equalsIgnoreCase(f.name)) {
                    continue
                }
                if(!first) {
                    sql.appendContentsOf(",");
                }
                var value = ReflectionUtils.getFieldValue(entity, fieldName: f.name);
                if (f.type.equalsIgnoreCase("Bool")) {
                    value = value.isEqual("true") ? 1 : 0
                    sql.appendContentsOf("\(SQLUtils.toSQLName(f.name)) = \(value)")
                } else {
                    sql.appendContentsOf("\(SQLUtils.toSQLName(f.name)) = '\(value)'")
                }
                first = false
            }
            
            sql.appendContentsOf(" where _id = \(id)" )
            
            try execSql(sql)
            
            return id
        }
    }
    
    static public func find(cls: AnyClass) -> [T] {
        return find(cls, query:"", args: [], orderBy: nil)
    }
    
    static private func isBasicClassString(string: String) -> Bool {
        if string.equalsIgnoreCase("String") || string.equalsIgnoreCase("NSNumber") || string.equalsIgnoreCase("Int") || string.equalsIgnoreCase("Float") || string.equalsIgnoreCase("Double") || string.equalsIgnoreCase("UInt") || string.equalsIgnoreCase("Bool") || string.equalsIgnoreCase("NSDictionary") || string.equalsIgnoreCase("NSString") {
            return true
        }
        
        return false
    }
    
    static public func find(cls: AnyClass, query: String?, args: [NSObject]?, orderBy: String? = nil) -> [T] {
        
        var list : [T] = []

        var sql = "select * from \(SQLUtils.toSQLNameClass(cls))"
        
        if let q = query {
            if (StringUtils.isNotEmpty(q)) {
                sql += " where \(q)"
            }
        }

        if let o = orderBy {
            if (StringUtils.isNotEmpty(o)) {
                sql += " order by \(o)"
            }
        }
        
        var db : DatabaseHelper!
        do {
            db = try getDatabaseHelper()
        } catch {
            log(ExceptionUtils.getDBExceptionMessage(error))
            return []
        }

        let stmt = db.query(sql, withParameters: args)
        
        let fields = ReflectionUtils.getFields(cls)
        
        while (db.nextRow(stmt)) {
            
            let clz: NSObject.Type = cls as! NSObject.Type
            let entity = clz.init() as! T
            
            entity.id = db.getInt(stmt, index: 0)
            
            var idx = 1;
            for f in fields {
                var value : AnyObject?
                
                if f.type.equalsIgnoreCase("Int") {
                    value = db.getInt(stmt, index: Int32(idx))
                    
                } else if (f.type.equalsIgnoreCase("Float")) {
                    value = db.getFloat(stmt, index: Int32(idx))
                    
                } else if (f.type.equalsIgnoreCase("Double")) {
                    value = db.getDouble(stmt, index: Int32(idx))
                    
                } else if (f.type.containsStringIgnoreCase("String")) {
                    value = db.getString(stmt, index: Int32(idx))
                    
                } else if (f.type.equalsIgnoreCase("NSDate")) {
                    value = db.getString(stmt, index: Int32(idx))
                }
                
                guard let newValue = value else {
                    idx += 1
                    continue
                }
                
                ReflectionUtils.setFieldValue(entity, fieldName: f.name, value: newValue)
                
                idx += 1
            }
            
            list.append(entity)
        }
        
        db.closeStatement(stmt)
        
        return list
    }

    static public func findById(cls: AnyClass, id: Int) -> T? {
        let list = find(cls, query:"_id=?",args: [id])
        if(list.count > 0) {
            return list[0]
        }
        return nil
    }
    
    static public func findAllById(cls: AnyClass, id: Int) -> [T]? {
        let list = find(cls, query:"_id=?",args: [id])
        if(list.count > 0) {
            return list
        }
        return nil
    }
    
    static public func findAll(cls: AnyClass) -> [T] {
        return find(cls)
    }
    
    static public func findAllOrderBy(cls:AnyClass, orderBy: String) -> [T] {
        return find(cls, query: nil, args: nil, orderBy: "nome")
    }
    
    // Deleta uma entity pelo id
    static public func delete(entity: T) throws -> Int {

        let id = entity.getId()
        
        if(id == 0 || id == -1) {
            log("id inválido: [\(id)]")
        } else {
            //Delete
            let sql = "delete from \(SQLUtils.toSQLNameClass(entity.classForCoder)) where _id = \(entity.getId());"
            let count = try execSql(sql)
            
            log("delete ok")
            
            return Int(count)
        }
        
        return -1
    }
    
    static public func deleteAll(cls: AnyClass, whereClause clause: String? = nil, withArguments args: [NSObject]? = nil) throws -> Int {
        
        let table = SQLUtils.toSQLNameClass(cls)
        var sql = "delete from \(table)"
        
        if let query = clause {
            sql += " where \(query)"
        }
        
        return try execSql(sql, withParameters: args)
    }
    
    static public func deleteById(cls: AnyClass, id: Int) throws -> Bool {
        try deleteAll(cls, whereClause: "_id = ?", withArguments: [id])
        return true
    }
    
    static public func printTable(cls: AnyClass) -> Bool {
        LogUtils.log("### start print table \(cls) ###")
        let list: [T] = findAll(cls)
        for entity in list {
            // TODO json
            let json = String(entity)
            LogUtils.log("id [\(entity.getId())] : \(json)")
        }
        LogUtils.log("### end print table \(cls) ###")
        return true
    }
    
    static public func createDatabase() {
        do {
            let sqls = try getSQLCreateDatabase()
            for sql in sqls {
                try execSql(sql)
            }
        } catch {
            log(ExceptionUtils.getDBExceptionMessage(error))
        }
    }
    
    static public func dropDatabase() {
        do {
            let sqls = try getSQLDropDatabase()
            for sql in sqls {
                try execSql(sql)
            }
        } catch {
            log(ExceptionUtils.getDBExceptionMessage(error))
        }
    }

    static public func log(s: String) {
        LogUtils.sql(s)
    }
    
    static public func execSql(sql: String, withParameters params: [AnyObject]? = nil) throws -> Int {
        do {
            return try getDatabaseHelper().execSql(sql, withParameters: params)
        } catch {
            throw error
        }
    }
    
    /**
     * Retorna a lista de ids no formato id1,id2,id3
     * @param ids
     * @return
     */
    static public func toSQLIn(ids: [Int]) -> String {
        var sb : String = ""
        var first = true
        for id in ids {
            if(!first) {
                sb.appendContentsOf(",")
            }
            sb.appendContentsOf(String(id))
            first = false
        }
        return sb
    }
}
