//
//  SQLiteUtils.swift
//  SugarIOS
//
//  Created by Livetouch on 14/06/16.
//  Copyright © 2016 Livetouch. All rights reserved.
//

import Foundation

import sqlite3

public class SQLiteUtils {
    
    //MARK: - Int
    
    static public func bindInt(stmt: COpaquePointer, atIndex index: Int, withInteger integer: Int) {
        sqlite3_bind_int(stmt, Int32(index), Int32(integer))
    }
    
    //MARK: - Double
    
    static public func bindDouble(stmt: COpaquePointer, atIndex index: Int, withDouble double: Double) {
        sqlite3_bind_double(stmt, Int32(index), double)
    }
    
    //MARK: - Text
    
    static public func bindText(stmt: COpaquePointer, atIndex index: Int, withString string: String) {
        sqlite3_bind_text(stmt, Int32(index), string.UTF8String, -1, nil)
    }
    
    static public func getText(stmt: COpaquePointer, atIndex index: Int) -> String {
        let result = sqlite3_column_text(stmt, Int32(index))
        
        let columnText = UnsafePointer<Int8>(result)
        let length = Int(strlen(columnText))
        
        if let string = NSString(bytes: columnText, length: length, encoding: NSUTF8StringEncoding) {
            return string as String
        }
        
        return ""
    }
    
    static public func setValue(value: AnyObject, withName name: String, andObject object: NSObject) {
        object.setValue(value, forKey: name)
    }
}