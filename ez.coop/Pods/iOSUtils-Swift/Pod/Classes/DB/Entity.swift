//
//  Entity.swift
//  SugarIOS
//
//  Created by Livetouch on 08/06/16.
//  Copyright © 2016 Livetouch. All rights reserved.
//

import UIKit

public class Entity : NSObject {
    
    //MARK: - Variables
    
    public var id : Int = -1
    
    //MARK: - Getters
    
    public func getId() -> Int {
        return id
    }
    
    //MARK: - Database
    
    public func save() {
        do {
            try SQLUtils.save(self)
        } catch {
            Entity.logDbException(error)
        }
    }
    
    public func delete() {
        do {
            try SQLUtils.delete(self)
        } catch {
            Entity.logDbException(error)
        }
    }
    
    static public func deleteAll() {
        do {
            try SQLUtils.deleteAll(self)
        } catch {
            Entity.logDbException(error)
        }
    }
    
    static public func deleteAllWithQuery(query: String, args: [NSObject]) {
        do {
            try SQLUtils.deleteAll(self, whereClause: query, withArguments: args)
        } catch {
            Entity.logDbException(error)
        }
    }
    
    static public func deleteById(id:Int) -> Bool {
        do {
            try SQLUtils.deleteById(self, id: id)
            return true
        } catch {
            Entity.logDbException(error)
            return false
        }
    }

    static public func findById<T:Entity>(id:Int) -> T? {
        return SQLUtils.findById(self, id: id)
    }
    
    static public func findAll<T:Entity>() -> [T] {
        return SQLUtils.findAll(self)
    }
    
    static public func findAllWithQuery<T:Entity>(query:String, andArguments args: [String]) -> [T] {
        return SQLUtils.find(self, query: query, args: args)
    }
    
    static public func get<T:Entity>(id:Int) -> T? {
        return findById(id)
    }
    
//    public func findById<T:Entity>(ids:[String]) {
//        String whereClause = "id IN (" + QueryBuilder.generatePlaceholders(ids.length) + ")";
//        return findAll(type, whereClause, ids);
//    }
//
//    public static <T extends Entity> T first(Class<T> type) {
//        List list = findWithQuery(type, "SELECT * FROM " + SqlUtils.toSQLName(type) + " ORDER BY ID ASC LIMIT 1", new String[0]);
//        return list.isEmpty()?null:(Entity)list.get(0);
//    }
//
//    public static <T extends Entity> T last(Class<T> type) {
//        List list = findWithQuery(type, "SELECT * FROM " + SqlUtils.toSQLName(type) + " ORDER BY ID DESC LIMIT 1", new String[0]);
//        return list.isEmpty()?null:(Entity)list.get(0);
//    }
//
//    public func findAll<T:Entity>(whereClause:String, whereArgs) {
//        return findAll(type, whereClause, whereArgs, (String)null, (String)null, (String)null);
//    }
//
//    public static <T extends Entity> List<T> findWithQuery(Class<T> type, String query, String... arguments) {
//        SQLiteDatabase db = getDatabase();
//
//        ArrayList e1;
//        try {
//            ArrayList list = new ArrayList();
//            Cursor c = db.rawQuery(query, arguments);
//
//            try {
//                while(c.moveToNext()) {
//                    Entity e = fill(c, type);
//                    list.add(e);
//                }
//            } catch (Exception var15) {
//                LogUtil.logError(var15.getMessage(), var15);
//            } finally {
//                c.close();
//            }
//            
//            e1 = list;
//        } finally {
//            SqlUtils.close(db);
//        }
//        
//        return e1;
//    }

    //MARK: - Parser
    
    /**
     Obtains a Dictionary<String, String> that contains all mappings between atributte names and JSON tags for the class which overrides this method. If the class has no such mappings, it does not need to override this method.
     
     For example, if ["userBirthday" : "user_birthday"] is added, it means that "userBirthday" is the name of the attribute, and "user_birthday" is the equivalent JSON tag.
     
     - Returns: Dictionary<String, String> with name mapping. Returns an empty dictionary by default.
     
     */
    public class func getAttributeMappings() -> [String: String] {
        return [:]
    }
    
    static private func logDbException(exception: ErrorType) {
        LogUtils.log(ExceptionUtils.getDBExceptionMessage(exception))
    }
}