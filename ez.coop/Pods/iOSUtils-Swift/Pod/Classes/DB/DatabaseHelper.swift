//
//  DatabaseHelper.swift
//  SugarIOS
//
//  Created by Livetouch on 08/06/16.
//  Copyright © 2016 Livetouch. All rights reserved.
//

import UIKit

import sqlite3

public protocol DatabaseDelegate {
    
    func getDatabaseFile() throws -> String
    
    func getDatabaseVersion() throws -> Int
    
    func getDomainClasses() throws -> [AnyClass]
    
    func onCreate()
    
    func onUpgrade(oldVersion: Int, newVersion: Int)
}

public class DatabaseHelper : NSObject, DatabaseDelegate {
    
    //MARK: - Variables
    
    ///Equivalente do Swift para o 'sqlite3 *db'
    public var db: COpaquePointer = nil;
    
    //MARK: - Inits
    
    override public init() {
        super.init()
        
        start()
    }
    
    //MARK: - Starter
    
    public func start() {
        var dbFile : String!
        do {
            dbFile = try getDatabaseFile()
        } catch {
            log(ExceptionUtils.getDBExceptionMessage(error))
        }
        
        let path = getFilePath(dbFile)
        
        let exists = NSFileManager.defaultManager().fileExistsAtPath(path)
        
        log("dbFile \(dbFile)")
        
        db = open(dbFile)
        
        var currentVersion : Int!
        do {
            currentVersion = try getDatabaseVersion()
        } catch {
            log(ExceptionUtils.getDBExceptionMessage(error))
        }
        
        if exists {
            let oldVersion = Prefs.getInt("db.version")
            
            if(oldVersion != currentVersion) {
                LogUtils.sql("DatabaseHelper.onUpgrade(oldVersion:\(oldVersion), newVersion: \(currentVersion))")
                
                onUpgrade(oldVersion, newVersion: currentVersion)
            }
        } else {
            LogUtils.sql("DatabaseHelper.onCreate()")
            
            onCreate()
            Prefs.setInt(currentVersion, forKey: "db.version")
        }
    }

    // Caminho do banco de dados
    private func getFilePath(nome: String) -> String {
        let path = NSHomeDirectory() + "/Documents/" + nome
        log("Database: \(path)")
        return path
    }
    
    //MARK: - Bind
    
    // Faz o bind dos parametros (?,?,?) de um SQL
    public func bindParams(stmt:COpaquePointer, params: [AnyObject]) {
        let size = params.count
        
        if (size > 0) {
            for i in 1...size {
                let value : AnyObject = params[i-1]
                
                if let integer = value as? Int {
                    SQLiteUtils.bindInt(stmt, atIndex: i, withInteger: integer)
                
                } else if let double = value as? Double {
                    SQLiteUtils.bindDouble(stmt, atIndex: i, withDouble: double)
                    
                } else if let text = value as? String {
                    SQLiteUtils.bindText(stmt, atIndex: i, withString: text)
                    
                } else {
                    LogUtils.sqlError("Value not bound: \(value)")
                }
            }
        }
    }
    
    //MARK: - SQL
    
    // Executa o SQL
    public func execSql(sql: String, withParameters params: [AnyObject]? = nil) -> Int {
        var result:CInt = 0
        
        // Statement
        let stmt = query(sql, withParameters: params)
        
        // Step
        result = sqlite3_step(stmt)
        if result != SQLITE_OK && result != SQLITE_DONE {
            sqlite3_finalize(stmt)
            let msg = "Erro ao executar SQL\n\(sql)\nError: \(getLastSqlError())"
            log(msg)
            return -1
        }
        
        // Se for insert recupera o id
        if sql.uppercaseString.hasPrefix("INSERT") {
            // http://www.sqlite.org/c3ref/last_insert_rowid.html
            let rid = sqlite3_last_insert_rowid(self.db)
            result = CInt(rid)
        } else {
            result = 1
        }
        
        // Fecha o statement
        sqlite3_finalize(stmt)
        
        return Int(result)
    }
    
    public func execSqlExclusive(script: String) -> COpaquePointer {
        var stmt : COpaquePointer = nil
        
        let sql = script.UTF8String
        
        sqlite3_exec(db, "BEGIN EXCLUSIVE TRANSACTION", nil, nil, nil)
        
        // prepare
        if sqlite3_prepare_v2(db, sql, -1, &stmt, nil) != SQLITE_OK {
            sqlite3_finalize(stmt)
            log("database prepare error: \(sqlite3_errmsg(db)) - \(script)")
            
            return nil
        }
        
        // execute
        if sqlite3_exec(db, sql, nil, nil, nil) == SQLITE_OK {
            if sqlite3_exec(db, "COMMIT TRANSACTION", nil, nil, nil) != SQLITE_OK {
                log("Sql Commit Error: \(sqlite3_errmsg(db))")
            }
            
            return stmt
            
        } else {
            sqlite3_finalize(stmt)
            log("Sql Execute Error: \(sqlite3_errmsg(db))\n[\(script)]")
            
            return nil
        }
    }
    
    // Executa o SQL e retorna o statement
    public func query(sql:String, withParameters params: [AnyObject]? = nil) -> COpaquePointer {
        
        var stmt : COpaquePointer = nil
        
        let cSql = sql.UTF8String
        
        let result = sqlite3_prepare_v2(db, cSql, -1, &stmt, nil)
        
        if (result != SQLITE_OK) {
            sqlite3_finalize(stmt)
            
            let msg = "Erro ao preparar SQL\n\(sql)\nError: \(getLastSqlError())"
            log("SQLite Error: \(msg)")
        }
        
        if let params = params {
            bindParams(stmt, params:params)
        }
        
        log(sql)
        
        return stmt
    }
    
    //MARK: - Script
    
    public func execScript(filename: String) {
        var script : String?
        
        do {
            let path = try FileUtils.getResourcePathOfFile(filename)
            script = try FileUtils.readBundleFileAtPath(path)
            
        } catch {
            switch error {
                case Exception.GenericException:
                    log("Erro ao ler o arquivo '\(filename)'.")
                    
                case Exception.FileNotFoundException:
                    log("Arquivo '\(filename)' não existe.")
                    
                default:
                    break
            }
        }
        
        if let script = script {
            let stmt = execSqlExclusive(script)
            sqlite3_finalize(stmt)
        }
    }
    
    /**
     *  Verifica se a próxima linha da consulta existe.
     *
     *  - returns: Retorna 'true', caso exista a próxima linha da consulta, e 'false' caso contrário.
     */
    public func nextRow(stmt: COpaquePointer) -> Bool {
        let result = sqlite3_step(stmt)
        let next = result == SQLITE_ROW
        return next
    }
    
    //MARK: - Helpers
    
    // Abre o banco de dados
    public func open(database: String) -> COpaquePointer {
        
        var db : COpaquePointer = nil
        
        let path = getFilePath(database)
        let cPath = path.UTF8String
        
        let result = sqlite3_open(cPath, &db)
        
        if(result == SQLITE_CANTOPEN) {
            log("Não foi possível abrir o banco de dados SQLite")
            return nil
        }
        
        return db
    }
    
    // Fecha o banco de dados
    public func close() {
        sqlite3_close(db)
    }
    
    public func closeStatement(stmt:COpaquePointer) {
        // Fecha o statement
        sqlite3_finalize(stmt)
    }
    
    //MARK: - Error
    
    // Retorna o último erro de SQL
    public func getLastSqlError() -> String {
        var err:UnsafePointer<Int8>? = nil
        err = sqlite3_errmsg(db)
        
        if let err = err {
            if let erro = String(UTF8String: err) {
                return erro
            }
        }
        
        return ""
    }
    
    //MARK: - Get Information
    
    // Lê uma coluna do tipo Int
    public func getInt(stmt:COpaquePointer, index:CInt) -> Int {
        let val = sqlite3_column_int(stmt, index)
        return Int(val)
    }
    
    // Lê uma coluna do tipo Double
    public func getDouble(stmt:COpaquePointer, index:CInt) -> Double {
        let val = sqlite3_column_double(stmt, index)
        return Double(val)
    }
    
    // Lê uma coluna do tipo Float
    public func getFloat(stmt:COpaquePointer, index:CInt) -> Float {
        let val = sqlite3_column_double(stmt, index)
        return Float(val)
    }
    
    // Lê uma coluna do tipo String
    public func getString(stmt:COpaquePointer, index:CInt) -> String {
        let cString  = SQLiteUtils.getText(stmt, atIndex: Int(index))
        
        let s = String(cString)
        return s
    }
    
    //MARK: - Log
    
    override public func log(s:String) {
        LogUtils.sql(s)
    }
    
    //MARK: - Database Delegate
    
    public func getDatabaseFile() throws -> String {
        throw SQLException.NotImplemented(message: "getDatabaseFile()")
    }
    
    public func getDatabaseVersion() throws -> Int {
        throw SQLException.NotImplemented(message: "getDatabaseVersion()")
    }
    
    public func getDomainClasses() throws -> [AnyClass] {
        throw SQLException.NotImplemented(message: "getDomainClasses()")
    }
    
    public func onCreate() {
        SQLUtils.createDatabase()
    }
    
    public func onUpgrade(oldVersion: Int, newVersion: Int) {}
}