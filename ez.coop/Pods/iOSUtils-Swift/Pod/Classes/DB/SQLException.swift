//
//  NotImplementedException.swift
//  SugarIOS
//
//  Created by Livetouch on 09/06/16.
//  Copyright © 2016 Livetouch. All rights reserved.
//

import Foundation

public enum SQLException: ErrorType {
    case NotImplemented(message: String)
    case DatabaseHelperNotFound
}
