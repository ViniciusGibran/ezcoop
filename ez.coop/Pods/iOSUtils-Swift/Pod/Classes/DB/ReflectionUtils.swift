//
//  ReflectionUtils.swift
//  SugarIOS
//
//  Created by Livetouch on 08/06/16.
//  Copyright © 2016 Livetouch. All rights reserved.
//

import UIKit

public class ReflectionUtils: NSObject {
    
    static public func getClassName(cls: AnyClass) -> String {
        let s = NSStringFromClass(cls).componentsSeparatedByString(".").last!
        return s;
    }
    
    static public func setFieldValue(object: NSObject, fieldName: String, value: AnyObject) {
        let mirror = Mirror(reflecting: object)
        if let properties = AnyBidirectionalCollection(mirror.children) {
            for property in properties {
                if(property.label == fieldName) {
                    SQLiteUtils.setValue(value, withName: fieldName, andObject: object)
                    return
                }
            }
        }
    }
    
    //TODO: Tentar voltar o tipo correto. (Int, String, etc..)
    static public func getFieldValue(object: NSObject, fieldName: String) -> NSObject {
       let mirror = Mirror(reflecting: object)
        if let properties = AnyBidirectionalCollection(mirror.children) {
            for property in properties {
                if(property.label == fieldName) {
                    // http://blog.krzyzanowskim.com/2015/11/19/swift-reflection-about-food/
                    return String(property.value)
                }
            }
        }
        return ""
    }
    
    static public func getFields(cls: AnyClass) -> [Field] {
        
        var fields : [Field] = []
        
        let clz: NSObject.Type = cls as! NSObject.Type
        let tipo = clz.init()
        
        let mirror = Mirror(reflecting: tipo)
        
        if let properties = AnyBidirectionalCollection(mirror.children) {
            for property in properties {
                
                if let label = property.label {
                    let f = Field()
                    f.name = label;
                    f.type = String(property.value.dynamicType)
                    
                    f.type = String(f.type).replaceFirstOccurrence("Optional<", withString: "")
                    f.type = String(f.type).replaceFirstOccurrence(">", withString: "")
                    
                    fields.append(f)
                    
                }
            }
        }
        
        return fields
    }
    
    
}
