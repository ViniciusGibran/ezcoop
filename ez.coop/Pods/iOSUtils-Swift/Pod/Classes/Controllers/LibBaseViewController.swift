//
//  LibBaseViewController.swift
//  Pods
//
//  Created by Livetouch-Mini01 on 15/06/2016.
//
//

import UIKit

public class LibBaseViewController: UIViewController, TaskDelegate {
    
    //MARK: - Variables
    
    //MARK: Menu
    
    ///Booleano que indicada se o view controller deve cancelar automaticamente as tasks ao passar por viewWillDisappear.
    public var cancelTasksOnViewWillDisappear: Bool = true
    
    //MARK: Internet
    
    private var reachability    : Reachability!
    
    private var isOnline        : Bool = false
    
    //MARK: - View Lifecycle
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        setupQueue()
    }
    
    override public func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        if cancelTasksOnViewWillDisappear {
            cancelTasks()
        }
    }
    
    //MARK: - Sleep
    
    public func sleep(timeInterval: NSTimeInterval) {
        NSThread.sleepForTimeInterval(timeInterval)
    }
    
    //MARK: - Alert
    
    public func alert(title: String, withMessage message: String? = nil) {
        AlertUtils.alert(title, message: message, onViewController: self)
    }
    
    //MARK: - Navigation Bar
    
    public func showNavigationBar() {
        NavigationBarUtils.show(self)
        StatusBarUtils.show()
    }
    
    public func hideNavigationBar(hideStatusBar: Bool = false) {
        NavigationBarUtils.hide(self)
        
        if hideStatusBar {
            StatusBarUtils.hide()
        }
    }
    
    //MARK: - Menu
    
    public func onClickMenu(sender: UIBarButtonItem) {
        guard let menuContainerViewController = menuContainerViewController else {
            return
        }
        
        menuContainerViewController.toggleLeftSideMenuCompletion(nil)
    }
    
    public func setupMenuButton(imageName: String) {
        guard let navigationController = navigationController else {
            return
        }
        
        guard navigationController.viewControllers.count == 1 else {
            return
        }
        
        NavigationBarUtils.setLeftBarButton(UIImage(named: imageName), withTarget: self, andAction: #selector(LibBaseViewController.onClickMenu(_:)))
    }
    
    //MARK: - Web View
    
    public func openWebViewWithUrl(url: String, andTitle screenTitle: String = "", andErrorCallback callbackError: (() -> Void)? = nil) {
        let web = LibWebViewController(nibName: "LibWebViewController", bundle: NSBundle(forClass: LibWebViewController.classForCoder()))
        web.url = url
        web.screenTitle = screenTitle
        web.callbackError = callbackError
        
        pushViewController(web)
    }
    
    //MARK: - Keyboard
    
    public func showKeyboard(view: UIKeyInput?) {
        KeyboardUtils.show(view)
    }
    
    public func hideKeyboard() {
        KeyboardUtils.hide(self.view)
    }
    
    public func registerKeyboardNotifications() {
        registerNotification(UIKeyboardWillShowNotification, withSelector: #selector(keyboardDidShow(_:)))
        registerNotification(UIKeyboardWillHideNotification, withSelector: #selector(keyboardDidHide(_:)))
    }
    
    public func keyboardDidShow(notification: NSNotification) {}
    
    public func keyboardDidHide(notification: NSNotification) {}
    
    //MARK: - Transition Delegate
    
    public func getTransitionDelegate() -> TransitionDelegate {
        return TransitionDelegate()
    }
    
    public func showViewControllerOnTop(viewController: UIViewController) {
        let navigationController = UINavigationController(rootViewController: viewController)
        
        navigationController.transitioningDelegate = getTransitionDelegate()
        navigationController.modalPresentationStyle = .Custom
        
        self.navigationController?.presentViewController(navigationController, animated: true, completion: nil)
    }
    
    //MARK: - Reachability
    
    public func registerNetworkNotification() {
        do {
            reachability = try Reachability.reachabilityForInternetConnection()
            try reachability.startNotifier()
            
            registerNotification(ReachabilityChangedNotification, withSelector: #selector(reachabilityDidChange(_:)))
            
            setReachabilityStatus(reachability)
            
        } catch {
            switch error {
                case ReachabilityError.FailedToCreateWithAddress(_):
                    log("Erro ao registrar notificação de Internet.")
                
                default:
                    break
            }
        }
    }
    
    public func reachabilityDidChange(notification: NSNotification) {
        if let reachability = notification.object as? Reachability {
            setReachabilityStatus(reachability)
        }
    }
    
    public func setReachabilityStatus(reachability: Reachability) {
        let netStatus = reachability.currentReachabilityStatus
        
        if (netStatus == .ReachableViaWWAN || netStatus == .ReachableViaWiFi) {
            isOnline = true
        } else {
            isOnline = false
        }
        
        connectionStatusDidChange(isOnline)
    }
    
    public func connectionStatusDidChange(connected: Bool) {}
    
}
