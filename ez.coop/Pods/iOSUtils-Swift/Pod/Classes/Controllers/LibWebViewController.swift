//
//  LibWebViewController.swift
//  Pods
//
//  Created by Livetouch-Mini01 on 23/06/2016.
//
//

import UIKit

public class LibWebViewController: LibBaseViewController, UIWebViewDelegate {
    
    //MARK: - Outlets
    
    @IBOutlet var webView : UIWebView!
    
    @IBOutlet var progress : UIActivityIndicatorView!
    
    //MARK: - Variables
    
    public var url         : String!
    public var screenTitle : String?
    
    public var callbackError   : (() -> Void)?
    
    //MARK: - View Lifecycle
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        if let screenTitle = screenTitle {
            title = screenTitle
        } else {
            title = "Web"
        }
        
        progress.startAnimating()
    }
    
    override public func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        if let nsurl = NSURL(string: url) {
            let request = NSURLRequest(URL: nsurl)
            webView.loadRequest(request)
        }
    }
    
    //MARK: - Web View Delegate
    
    public func webViewDidFinishLoad(webView: UIWebView) {
        progress.stopAnimating()
    }
    
    public func webView(webView: UIWebView, didFailLoadWithError error: NSError?) {
        progress.stopAnimating()
        
        if let onError = callbackError {
            onError()
        }
        
        if let error = error {
            log(error.localizedDescription)
        }
    }
}
