//
//  ThirdViewController.swift
//  ezlaw
//
//  Created by Vinicius Gibran on 16/10/16.
//  Copyright © 2016 ezlaw. All rights reserved.
//

import UIKit
import iOSUtils_Swift

class ThirdViewController: BaseViewController {

    //MARK: Outlats
    @IBOutlet var tabViewContent    : UIView!
    @IBOutlet var btnsMenu : [UIButton]!
    //MARK: Variables
    var tabBar : TabBarView!
    
    
    //MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        StatusBarUtils.show()
        NavigationBarUtils.hide(self)
        //        navigationController!.navigationBar.setBackgroundImage(UIImage(), forBarMetrics: .Default)
        //        navigationController!.navigationBar.shadowImage = UIImage()
        //        navigationController!.navigationBar.translucent = true
        //
        //        for btn in btnsMenu{
        //            btn.layer.shadowRadius = 2.0
        //            btn.layer.shadowColor = UIColor.blackColor().CGColor
        //            btn.layer.shadowOffset = CGSizeMake(1.0, 1.0)
        //            btn.layer.shadowOpacity = 1.0
        //            btn.layer.shadowRadius = 1.0
        //            btn.layer.borderWidth = 1
        //            let color = UIColor.darkGrayColor()
        //            btn.layer.borderColor = color.CGColor
        //        }
        
        
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        //create TabBar
        if (tabBar == nil) {
            tabBar = UIView.loadFromNibNamed("TabBarView", bundle: nil) as! TabBarView
            tabBar.pushDelegate = self
            tabBar.setItem3Selected()
            tabBar.frame = CGRectMake(0, 0, tabViewContent.bounds.width, tabViewContent.bounds.height)
            tabViewContent.addSubview(tabBar)
            
            tabViewContent.layer.shadowRadius = 0.0
            tabViewContent.layer.shadowColor = UIColor.blackColor().CGColor
            tabViewContent.layer.shadowOffset = CGSizeMake(1.0, 1.0)
            tabViewContent.layer.shadowOpacity = 1.0
            tabViewContent.layer.shadowRadius = 5.0
        }
    }
    

}
