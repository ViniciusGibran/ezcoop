//
//  BaseViewController.swift
//  ezlaw
//
//  Created by Vinicius Gibran on 16/10/16.
//  Copyright © 2016 ezlaw. All rights reserved.
//

import UIKit
import iOSUtils_Swift

class BaseViewController: LibBaseViewController, PushDelegate {

    //MARK: _ View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        setupNavigationBar()
    }
    //MARK: - Setups
    func setupNavigationBar() {
        setupNavigationBar(barTintColor: UIColor.colorWithRed(33, green: 33, blue: 33, andAlpha: 0.0), tintColor: UIColor.whiteColor(), textColor: UIColor.whiteColor())
        StatusBarUtils.setTextColorToWhite()
    }
    
    func setupNavigationBar(barTintColor barTintColor: UIColor, tintColor: UIColor, textColor: UIColor) {
        guard let navigationController = self.navigationController else {
            return
        }
        //        navigationController.hidesBarsOnSwipe = true
        navigationController.navigationBar.opaque = true
        navigationController.navigationBar.translucent = true
        navigationController.navigationBar.titleTextAttributes = [NSFontAttributeName: UIFont(name: "SourceSansPro-Regular", size: 18)!, NSForegroundColorAttributeName: textColor]
        navigationController.navigationBar.barTintColor = .clearColor()
        navigationController.navigationBar.tintColor = tintColor
        
        
        //        NavigationBarUtils.setBackBarButtonWithImage(UIImage(named: "nav_back"), forViewController: self)
    }
    
    func setupNavConTitleAtributes(textFont textFont:String, size:CGFloat, textColor:UIColor){
        navigationController!.navigationBar.titleTextAttributes = [NSFontAttributeName: UIFont(name: textFont, size: size)!, NSForegroundColorAttributeName: textColor]
    }
    
    
    //MARK: - Push Delegate
    func showAddClientView() {
        //override
    }
    
    func showSearchView() {
        //override
    }
    
    func pushViewController(viewController: UIViewController) {
        
        //if (iOSUtils.isNetworkAvailable()) {
            self.navigationController?.pushViewController(viewController, animated: true)
       
        /*} else {
            if let container = self.menuContainerViewController {
                container.dismissViewControllerAnimated(true, completion: {
                    self.alert("Vitri", withMessage : "Você deve estar conectado a uma rede de internet para utilizar o aplicativo. Por favor verifique sua conexão e tente novamente.")
                })
            } else {
                alert("Vitri", withMessage: "Você deve estar conectado a uma rede de internet para utilizar o aplicativo. Por favor verifique sua conexão e tente novamente.")
                self.navigationController?.popToRootViewControllerAnimated(true)
            }
        }*/
    }
    
    func presentViewController(viewController: UIViewController) {
        self.navigationController?.presentViewController(viewController, animated: true, completion: nil)
    }
    
    func setViewControllers(viewControllers: [UIViewController], animated: Bool) {
        self.navigationController?.setViewControllers(viewControllers, animated: animated)
    }
    
    func setLastViewController(viewController: UIViewController) {
        if let navigationController = navigationController {
            var viewControllers = navigationController.viewControllers
            viewControllers[viewControllers.count - 1] = viewController
            setViewControllers(viewControllers, animated: false)
        }
    }
    
    func blurEffectOnAddReport(viewIn: UIView, open: Bool){
        
        if(open){
            //view start true for hidden
            viewIn.backgroundColor = UIColor.clearColor()
            viewIn.hidden = false
            UIView.animateWithDuration(0.3, delay: 0.0, options: .CurveEaseIn, animations: {
                viewIn.backgroundColor = UIColor.colorWithRed(19, green: 19, blue: 19, andAlpha: 0.55)
                
                }, completion: { (true) in
            })
            
        }else{
            
            UIView.animateWithDuration(0.15, delay: 0.0, options: .CurveEaseIn, animations: {
                viewIn.backgroundColor = UIColor.clearColor()
                
                }, completion: { (true) in
                    viewIn.hidden = true
            })
        }
    }
    
    //MARK: - Create Left Menu
    func createUserSession() {
        
        StatusBarUtils.show()
        NavigationBarUtils.hide(self)
        
        let centerController = FirstViewController(nibName: "FirstViewController", bundle: nil)
        let leftMenu = SideMenuViewController(nibName: "SideMenuViewController", bundle: nil)
        
        let navigationController = UINavigationController(rootViewController: centerController)
        
        let container = SideMenuContainerViewController.containerWithCenterViewController(navigationController, andLeftMenuViewController: leftMenu, andRightMenuViewController: nil)
        container.setLeftMenuWidth(DeviceUtils.getScreenWidth() - 75)
        container.setShadowRadius(5)
        
        //disable pan mode - deixar assim?
        container.setPanMode(.None)
        
        
        self.navigationController?.presentViewController(container, animated: true, completion: {
            if (self.navigationController == nil) {
                return
            }
            self.navigationController!.viewControllers = [self.navigationController!.viewControllers[0]]
        })
    }
    

}
