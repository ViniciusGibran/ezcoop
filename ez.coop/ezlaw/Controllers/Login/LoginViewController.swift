//
//  LoginViewController.swift
//  ezlaw
//
//  Created by Vinicius Gibran on 16/10/16.
//  Copyright © 2016 ezlaw. All rights reserved.
//

import UIKit
import iOSUtils_Swift

class LoginViewController: BaseViewController {
    
    //MARK: Outlats
    @IBOutlet weak var btSignIn             : UIButton!
    @IBOutlet weak var btRegisterAccount    : UIButton!
    @IBOutlet weak var tfEmail              : UITextField!
    @IBOutlet weak var tfPassword           : UITextField!
    

    //MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        setupView()
        StatusBarUtils.show()
        NavigationBarUtils.hide(self)
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    //MARK: - Setups
    func setupView(){
        tfEmail.setLeftPadding(10)
        tfPassword.setLeftPadding(10)
    }
    
    
    //MARK: - Actions
    @IBAction func onClickNovaConta(sender: UIButton) {
        let cadastro = RegisterViewController(nibName: "RegisterViewController", bundle: nil)
        navigationController?.pushViewController(cadastro, animated: true)
    }
    
    @IBAction func onClickEntrar(sender: UIButton) {
//        let firstVC = FirstViewController(nibName: "FirstViewController", bundle: nil)
        createUserSession()
    }
    
    @IBAction func onTapView(sender: AnyObject) {
        view.endEditing(true)
    }
    
    
}
