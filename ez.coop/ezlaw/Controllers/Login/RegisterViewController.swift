//
//  RegisterViewController.swift
//  Hacka
//
//  Created by Vinicius Gibran on 05/10/16.
//  Copyright © 2016 er of the light. All rights reserved.
//

import UIKit
import iOSUtils_Swift

class RegisterViewController: BaseViewController {

    
    //MARK: - Outlats
    @IBOutlet weak var tfName        : UITextField!
    @IBOutlet weak var tfEmail       : UITextField!
    @IBOutlet weak var tfPassword    : UITextField!
    @IBOutlet weak var tfConfirmPass : UITextField!
    
    
    //MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Cadastro"

    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        NavigationBarUtils.show(self)
        setupViews()
    }
    
    
    //MARK: - Setups
    func setupViews(){
        
        //textfields
        tfName.setLeftPadding(10)
        tfEmail.setLeftPadding(10)
        tfPassword.setLeftPadding(10)
        tfConfirmPass.setLeftPadding(10)
        
    }
    
    
    
    //MARK: - Actions
    @IBAction func onTapView(sender: AnyObject) {
        view.endEditing(true)
    }
    

}
