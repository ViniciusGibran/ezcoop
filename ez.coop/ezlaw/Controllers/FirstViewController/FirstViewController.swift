//
//  FirstViewController.swift
//  ezlaw
//
//  Created by Vinicius Gibran on 16/10/16.
//  Copyright © 2016 ezlaw. All rights reserved.
//

import UIKit
import iOSUtils_Swift

class FirstViewController: BaseViewController, AddClientDelegate, SearchDelegate, UICollectionViewDelegate, UICollectionViewDataSource {

    //MARK: Outlats
    @IBOutlet var tabViewContent    : UIView!
    @IBOutlet var viewShadow    : UIView!
    @IBOutlet var viewEffectForAddReport    : UIView!
    @IBOutlet weak var collectionView : UICollectionView!
    @IBOutlet var lblEmptyData    : UILabel!
    
    @IBOutlet var btnsMenu : [UIButton]!
    
    
    //MARK: Variables
    var tabBar : TabBarView!
    var groupList : [Group]?
    var objVO = ValueObject.shared()
    
    //MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        StatusBarUtils.show()
        NavigationBarUtils.hide(self)
        
        setupCollectionView()
        getGroups()
    
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        //create TabBar
        if (tabBar == nil) {
            tabBar = UIView.loadFromNibNamed("TabBarView", bundle: nil) as! TabBarView
            tabBar.pushDelegate = self
            
            tabBar.frame = CGRectMake(0, 0, tabViewContent.bounds.width, tabViewContent.bounds.height - 13)
            tabViewContent.addSubview(tabBar)
            
            viewShadow.layer.shadowRadius = 0.0
            viewShadow.layer.shadowColor = UIColor.blackColor().CGColor
            viewShadow.layer.shadowOffset = CGSizeMake(1.0, 1.0)
            viewShadow.layer.shadowOpacity = 1.0
            viewShadow.layer.shadowRadius = 5.0
        }
    }
    
    //MARK: - Setups
    func setupCollectionView(){
        
        let nib = UINib(nibName: "FirstViewCollectionCell", bundle:nil)
        collectionView.registerNib(nib, forCellWithReuseIdentifier: "FirstViewCollectionCell")
        collectionView.backgroundView = nil
        collectionView.backgroundColor = .clearColor()
        
        collectionView.delegate = self
        collectionView.dataSource = self
        
    }

    
    override func showAddClientView() {
        print("show client")
        let addGV = AddGroupViewController(nibName: "AddGroupViewController", bundle: nil)
        addGV.delegate = self
        showViewControllerOnTop(addGV)
        blurEffectOnAddReport(viewEffectForAddReport, open: true)
    }
    
    override func showSearchView() {
        print("show search")
        let searchCV = SearchViewController(nibName: "SearchViewController", bundle: nil)
        searchCV.delegate = self
        showViewControllerOnTop(searchCV)
        blurEffectOnAddReport(viewEffectForAddReport, open: true)
    }
    
    //MARK: - Add Client Delegate
    func dismissClientView() {
        blurEffectOnAddReport(viewEffectForAddReport, open: false)
    }
    
    func addClient(){
        getGroups()
    }
    
    //MARK: - Search Delegate
    func dismissSearchView() {
        blurEffectOnAddReport(viewEffectForAddReport, open: false)
    }
    
    func searchViewAction() {
        
    }
    
    //MARK: - CollectionView Delegate & Datasource
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let g = groupList?.count{
            return g
        }
        return 0
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("FirstViewCollectionCell", forIndexPath: indexPath) as! FirstViewCollectionCell
        if let g = groupList
        {
           cell.setupCell(g[indexPath.row])
        }
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, didDeselectItemAtIndexPath indexPath: NSIndexPath) {
        let groupVC = GroupViewController(nibName: "GroupViewController", bundle: nil)
        groupVC.group = groupList![indexPath.row ]
        navigationController?.pushViewController(groupVC, animated: true)
        
    }
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        let size = ( 110 * (4 + 1)) / 4
        return CGSize(width: 140, height: 140)
    }
    
    //MARK: - Tasks
    func getGroups(){
        groupList = []
        if let list = DataBaseHelper.getGroupListFromDB(){
            groupList = list
            collectionView.reloadData()
            lblEmptyData.hidden = true
        }else {
            lblEmptyData.hidden = false
        }
    }
    
}
