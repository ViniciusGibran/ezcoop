//
//  FirstViewCollectionCell.swift
//  ezlaw
//
//  Created by Vinicius Gibran on 16/10/16.
//  Copyright © 2016 ezlaw. All rights reserved.
//

import UIKit

class FirstViewCollectionCell: UICollectionViewCell {

    @IBOutlet weak var labelName : UILabel!
    @IBOutlet var backView : UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let color = UIColor.colorWithRed(196, green: 84, blue: 22, andAlpha: 1.0)//orange
        backView.backgroundColor = color
        backView.layer.shadowColor = UIColor.blackColor().CGColor
        backView.layer.shadowOffset = CGSizeMake(1.0, 1.0)
        backView.layer.shadowOpacity = 1.0
        backView.layer.shadowRadius = 3.0
     
    }
    
    func setupCell(obj: Group){
        labelName.text = obj.name
    }

}
