
//
//  ClientPeopleView.swift
//  ezlaw
//
//  Created by Vinicius Gibran on 17/10/16.
//  Copyright © 2016 ezlaw. All rights reserved.
//

import UIKit

class ClientPeopleView: UIView {
    
        @IBOutlet var btnArr : [UIButton]!
    
    
    override func awakeFromNib() {
        for tf in btnArr{
            tf.layer.borderWidth = 2
            let color = UIColor.colorWithRed(196, green: 84, blue: 22, andAlpha: 1.0)
            tf.layer.borderColor = color.CGColor
        }
    }

}
