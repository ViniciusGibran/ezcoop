//
//  SecondViewController.swift
//  ezlaw
//
//  Created by Vinicius Gibran on 16/10/16.
//  Copyright © 2016 ezlaw. All rights reserved.
//

import UIKit
import iOSUtils_Swift

class GroupViewController: BaseViewController {

    //MARK: Outlats
    
    
    //MARK: Variables
    
    var group : Group?
    
    var clientInfo : ClientInfo!
    var clientPeople : ClientPeopleView!
    var lastView : LastView!
    
    @IBOutlet weak var contentView : UIView!
    @IBOutlet weak var btnNext : UIButton!
    @IBOutlet weak var gerarResumo : UIButton!
    @IBOutlet  var plusBtns : [UIButton]!
    @IBOutlet  var treBtns : [UIButton]!
    
    var selected1 = false
    var selected2 = false
    var selected3 = false
    
    
    //MARK: - Constantes
    let DADOS_BASICOS  = 0
    let PESSOAS = 1
    let ORGAOS = 2
    
    //MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        title = group?.name
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
           }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        setupView()
        StatusBarUtils.show()
        NavigationBarUtils.show(self)
        
        for btn in treBtns{
            btn.hidden = true
            btn.layer.shadowRadius = 0.0
            btn.layer.shadowColor = UIColor.blackColor().CGColor
            btn.layer.shadowOffset = CGSizeMake(1.0, 1.0)
            btn.layer.shadowOpacity = 1.0
            btn.layer.shadowRadius = 3.0
        }
        
        for plus in plusBtns{
            plus.hidden = true
            plus.layer.shadowRadius = 0.0
            plus.layer.shadowColor = UIColor.blackColor().CGColor
            plus.layer.shadowOffset = CGSizeMake(1.0, 1.0)
            plus.layer.shadowOpacity = 1.0
            plus.layer.shadowRadius = 3.0
        }
        
        let color = UIColor.colorWithRed(196, green: 84, blue: 22, andAlpha: 1.0)//orange
        btnNext.layer.borderWidth = 2
        btnNext.layer.borderColor = color.CGColor
        
        gerarResumo.layer.borderWidth = 2
        gerarResumo.layer.borderColor = color.CGColor
        
        if (clientInfo == nil) {
            clientInfo = UIView.loadFromNibNamed("ClientInfo", bundle: nil) as! ClientInfo
            clientInfo.frame = CGRectMake(0, 0, contentView.bounds.width, contentView.bounds.height)
            
            contentView.addSubview(clientInfo)
        }
    }
    
    
    @IBAction func nextAction(sender: UIButton) {
        if clientInfo != nil{
            clientInfo.removeFromSuperview()
        }
        
        if (clientPeople == nil) {
            
            clientPeople = UIView.loadFromNibNamed("ClientPeopleView", bundle: nil) as! ClientPeopleView
            clientPeople.frame = CGRectMake(0, 0, contentView.bounds.width, contentView.bounds.height)
            
            btnNext.setTitle("Finalizar", forState: .Normal)
            contentView.addSubview(clientPeople)
            
        } else{
            btnNext.hidden = true
            contentView.hidden = true
            for btn in treBtns{
                btn.hidden = false
            }
            clientPeople.removeFromSuperview()
        }
        
    }
    
    @IBAction func selectResumo(sender: UIButton) {
        
        
        
        switch sender.tag {
        case 0:
            if(!selected1){
                plusBtns[0].hidden = false
                selected1 = true
            } else {
                plusBtns[0].hidden = true
                selected1 = false
            }
        case 1:
            if(!selected2){
                plusBtns[1].hidden = false
                selected2 = true
            }else{
                plusBtns[1].hidden = true
                selected2 = false
            }
        case 2:
            if(!selected3){
                selected3 = true
                plusBtns[2].hidden = false
            }else {
                plusBtns[2].hidden = true
                selected3 = false
            }
        default:
            return
        }
        
        if (selected1 || selected2 || selected3){
            gerarResumo.hidden = false
        }else {
            gerarResumo.hidden = true
        }
        
        
    }
    
    
    func setupView(){
     

    }
    
    //MARK: - Actions

}

