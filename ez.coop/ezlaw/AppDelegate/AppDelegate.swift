//
//  AppDelegate.swift
//  ezlaw
//
//  Created by Vinicius Gibran on 16/10/16.
//  Copyright © 2016 ezlaw. All rights reserved.
//

import UIKit
import iOSUtils_Swift

@UIApplicationMain
class AppDelegate: BaseAppDelegate{

    override func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        
        if Prefs.getInt("DB_IDs") == 0{
            DataBaseHelper.setGroupIDs(-1)
        }
        
        let firstView = LoginViewController(nibName: "LoginViewController", bundle: nil)
        window = createNavigationController(firstView)
        
        return true
    }

}

