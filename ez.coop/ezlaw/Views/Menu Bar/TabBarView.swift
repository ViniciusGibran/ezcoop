
//
//  TabBarView.swift
//  Hacka
//
//  Created by Vinicius Gibran on 09/10/16.
//  Copyright © 2016 er of the light. All rights reserved.
//

import UIKit
import iOSUtils_Swift

protocol PushDelegate {
    func pushViewController(viewController: UIViewController)
    func presentViewController(viewController: UIViewController)
    func setViewControllers(viewControllers: [UIViewController], animated: Bool)
    func setLastViewController(viewController: UIViewController)
    func showAddClientView()
    func showSearchView()
}


class TabBarView: UIView {
    
    //MARK: - Outlats
    @IBOutlet weak var item1 : UIButton!
    @IBOutlet weak var item2 : UIButton!
    @IBOutlet weak var item3 : UIButton!
    
    @IBOutlet weak var imgItem1 : UIImageView!
    @IBOutlet weak var imgItem2 : UIImageView!
    @IBOutlet weak var imgItem3 : UIImageView!
    
    //MARK: - Variables
    var pushDelegate    : PushDelegate?
    var lastButton      : UIButton!
    var selectedButton  : UIButton!
    
    //MARK: - Init
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    //MARK: - Awake
    override func awakeFromNib() {
        let itemArray = [item1, item2, item3]
        
        for item in itemArray {
            let image = UIImage(named: getImageName(item))
            
            item.setImage(image?.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate), forState: .Normal)
            item.tintColor = UIColor.colorWithRed(5, green: 46, blue: 58, andAlpha: 1.0)
            
        }
    }
    
    //MARK: - Actions
    @IBAction func onItem1(sender: UIButton!) {
        LogUtils.log("tocou alert")
//        let firstVC = FirstViewController(nibName: "FirstViewController", bundle: nil)
//        pushDelegate?.setLastViewController(firstVC)
        
        pushDelegate?.showAddClientView()
    

    }
    
    @IBAction func onItem2(sender: UIButton!) {
        
        LogUtils.log("tocou bus")
//        let secondVC = SecondViewController(nibName: "SecondViewController", bundle: nil)
//        pushDelegate?.setLastViewController(secondVC)
        
        pushDelegate?.showSearchView()
    }
    
    @IBAction func onItem3(sender: UIButton!) {
        LogUtils.log("tocou tool")
        let thirdVC = ThirdViewController(nibName: "ThirdViewController", bundle: nil)
        pushDelegate?.setLastViewController(thirdVC)
    }

    
    //MARK: - Update Buttons
    func setButtonSelected(button: UIButton!){
        selectedButton = button
        configButtonState(selectedButton)
        
        if let lastButton = lastButton {
            if (lastButton != button){
                configButtonState(lastButton)
            }
        }
        self.lastButton = button
    }
    
    //MARK: - Helper
    func configButtonState(item: UIButton){
        let image = UIImage(named: "bg_selected")
        if(item == selectedButton){
            switch item {
            case item1:
                imgItem1.image = image
                imgItem1.hidden = false
                imgItem2.hidden = true
                imgItem3.hidden = true
            case item2:
                imgItem2.image = image
                imgItem2.hidden = false
                imgItem1.hidden = true
                imgItem3.hidden = true
            case item3:
               imgItem3.image = image
               imgItem3.hidden = false
               imgItem2.hidden = true
               imgItem1.hidden = true
            default:
                return
            }
            
        }
    }

    
    //MARK: - Seters
    func setItem1Selected(){
        setButtonSelected(item1)
    }
    
    func setItem2Selected(){
        setButtonSelected(item2)
    }
    
    func setItem3Selected(){
        setButtonSelected(item3)
    }
    
    func getImageName(button: UIButton) -> String {
        if (button == item1){
            return "item1"
        } else if (button == item2){
            return "item2"
        } else {
            return "item3"
        }
    }
}
