//
//  SearchViewController.swift
//  ezlaw
//
//  Created by Vinicius Gibran on 16/10/16.
//  Copyright © 2016 ezlaw. All rights reserved.
//

import UIKit
import iOSUtils_Swift

protocol SearchDelegate {
    func searchViewAction()
    func dismissSearchView()
}

class SearchViewController: BaseViewController {

    //MARK: - Outlets
    @IBOutlet var selecBtns : [UIButton]!
    @IBOutlet var btnDismiss : UIButton!
    @IBOutlet var content : UIView!
    @IBOutlet var tfContent : UIView!
    @IBOutlet var tfSearch : UITextField!
    
    var selectedView = UIView()
    
    var delegate : SearchDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        setSearchType(selecBtns[0])
        NavigationBarUtils.hide(self)
        tfSearch.setLeftPadding(10)
        content.backgroundColor = .whiteColor()
        
        Animation.rotate(btnDismiss, duration: 0, degress: 45)
        content.layer.shadowRadius = 2.0
        content.layer.shadowColor = UIColor.blackColor().CGColor
        content.layer.shadowOffset = CGSizeMake(1.0, 1.0)
        content.layer.shadowOpacity = 1.0
        content.layer.shadowRadius = 1.0
        
        //            tfNome.backgroundColor = .whiteColor()
        //            tfContent.layer.shadowRadius = 2.0
        tfContent.layer.shadowColor = UIColor.blackColor().CGColor
        tfContent.layer.shadowOffset = CGSizeMake(1.0, 1.0)
        tfContent.layer.shadowOpacity = 1.0
        tfContent.layer.shadowRadius = 2.0
        let color = UIColor.darkGrayColor()
        tfContent.layer.borderColor = color.CGColor

       
        for btn in selecBtns{    
//            btn.layer.shadowRadius = 2.0
//            btn.layer.shadowColor = UIColor.blackColor().CGColor
//            btn.layer.shadowOffset = CGSizeMake(1.0, 1.0)
//            btn.layer.shadowOpacity = 1.0
            btn.layer.borderWidth = 1
            let color = UIColor.darkGrayColor()
            btn.layer.borderColor = color.CGColor
        }
    }
    
    @IBAction func setSearchType(sender: UIButton){
        
        func removeBG(){
            for btn in selecBtns{
                if btn.tag != sender.tag{
                    btn.backgroundColor = .clearColor()
                }
            }
        }
        switch sender.tag {
        case 1:
            sender.backgroundColor = .orangeColor()
            tfSearch.placeholder = "CPF"
            removeBG()
        case 2:
            sender.backgroundColor = .orangeColor()
            tfSearch.placeholder = "CNPJ"
            removeBG()
        case 3:
            sender.backgroundColor = .orangeColor()
            tfSearch.placeholder = "Nome"
            removeBG()
        case 4:
            sender.backgroundColor = .orangeColor()
            tfSearch.placeholder = "Diretor"
            removeBG()
        case 5:
            sender.backgroundColor = .orangeColor()
            tfSearch.placeholder = "Acionista"
            removeBG()
        case 6:
            sender.backgroundColor = .orangeColor()
            tfSearch.placeholder = "Conselheiro"
            removeBG()
        default:
            return
        }
        
    }
    
    @IBAction func onTapContent(sender: AnyObject) {
        editEnding()
    }
    @IBAction func onTap(sender: AnyObject) {
        editEnding()
        dismissViewControllerAnimated(true, completion: nil)
        delegate?.dismissSearchView()
    }
    @IBAction func onSearch(sender: AnyObject) {
        editEnding()
        dismissViewControllerAnimated(true, completion: nil)
        delegate?.dismissSearchView()
        delegate?.searchViewAction()
    }
    
    func editEnding(){
        view.endEditing(true)
    }
    

}
