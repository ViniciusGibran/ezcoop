//
//  AddReportViewController.swift
//  Hacka
//
//  Created by Vinicius Gibran on 09/10/16.
//  Copyright © 2016 er of the light. All rights reserved.
//

import UIKit
import iOSUtils_Swift

protocol AddClientDelegate {
    func addClient()
    func dismissClientView()
}

class AddGroupViewController: BaseViewController {

    //MARK: - Outlets
    @IBOutlet var btnDismiss : UIButton!
    @IBOutlet var content : UIView!
    @IBOutlet var tfContent : UIView!
    @IBOutlet var tfNome : UITextField!
    
    var delegate : AddClientDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        NavigationBarUtils.hide(self)
        tfNome.setLeftPadding(10)
        content.backgroundColor = .whiteColor()
        
        
        Animation.rotate(btnDismiss, duration: 0, degress: 45)
        content.layer.shadowRadius = 2.0
        content.layer.shadowColor = UIColor.blackColor().CGColor
        content.layer.shadowOffset = CGSizeMake(1.0, 1.0)
        content.layer.shadowOpacity = 1.0
        content.layer.shadowRadius = 1.0
        
        tfContent.layer.shadowColor = UIColor.blackColor().CGColor
        tfContent.layer.shadowOffset = CGSizeMake(1.0, 1.0)
        tfContent.layer.shadowOpacity = 1.0
        tfContent.layer.shadowRadius = 2.0
        let color = UIColor.darkGrayColor()
        tfContent.layer.borderColor = color.CGColor
        
        
    }
    
    @IBAction func onTapContent(sender: AnyObject) {
        editEnding()
    }
    @IBAction func onTap(sender: AnyObject) {
        editEnding()
        dismissViewControllerAnimated(true, completion: nil)
        delegate?.dismissClientView()
    }
    
    @IBAction func onAdd(sender: AnyObject) {
        let group = Group()
        let vo = ValueObject.shared()
        
        if let text = tfNome.text{
            group.name = text
            vo.setGroupObj(group)
        }
        editEnding()
        dismissViewControllerAnimated(true, completion: nil)
        delegate?.dismissClientView()
        delegate?.addClient()
    }
    
    func editEnding(){
        view.endEditing(true)
    }
    
    
    
}
