//
//  Animation.swift
//  nemo
//
//  Created by livetouch on 18/4/16.
//  Copyright © 2016 we4nemo. All rights reserved.
//

import UIKit
import iOSUtils_Swift

public class Animation {
    
    static func positionXY(object: NSObject, duration: Double, delay: Double, x: CGFloat, y: CGFloat, shouldHide: Bool, changeText: Bool, text: String!, alpha: CGFloat) {
        
        let objView = object as! UIView
        objView.setNeedsLayout()
        
        
        
        UIView.animateWithDuration(duration, delay: delay, options: UIViewAnimationOptions.CurveEaseOut, animations: {
            
            objView.hidden = false
            
            objView.transform = CGAffineTransformMakeTranslation(x, y)
            
            if(changeText){
                
                let label = objView as! UIButton
                label.setTitle(text, forState: .Normal)
            }
            
            if (shouldHide)
            {
                objView.alpha = 0
            }
            else
            {
                objView.alpha = alpha
            }
            
            objView.layoutIfNeeded()
            
            }, completion: { (Bool) -> Void in
                
                //do something
        })
        
        
    }
    
    static func rotate(objct: NSObject, duration: Double, degress: Double){
        
        let radians = CGFloat(Double(M_PI) * degress / 180.0)
        let objView = objct as! UIView
        objView.setNeedsLayout()
        
        UIView.animateWithDuration(0.2, delay: 0, options: UIViewAnimationOptions.TransitionCrossDissolve, animations: {
            
            objView.transform = CGAffineTransformMakeRotation(radians)
            
        }) { (Bool) in
            
        }
        
        
    }
    
    
    //MARK: - Resets
    static func resetStateOnly(objct: NSObject, duration: Double){
        let objView = objct as! UIView
        objView.setNeedsLayout()
        
        UIView.animateWithDuration(duration, animations: {
            
            objView.transform = CGAffineTransformIdentity
            
        })
        
    }
    
    static func resetState(objct: NSObject, duration: Double, delay: Double, shouldHide: Bool, changeText: Bool, text: String!, alpha: CGFloat) {
        
        
        let objView = objct as! UIView
        objView.setNeedsLayout()
        
        
        
        UIView.animateWithDuration(duration, delay: delay, options: UIViewAnimationOptions.CurveEaseIn, animations: {
            
            objView.hidden = false
            objView.transform = CGAffineTransformIdentity
            
            if(changeText){
                
                let label = objView as! UIButton
                label.setTitle(text, forState: .Normal)
            }
            
            if (shouldHide)
            {
                objView.alpha = 0
            }
            else
            {
                objView.alpha = alpha
            }
            
            objView.layoutIfNeeded()
            
            }, completion: { (Bool) -> Void in
                
                //do something
        })
    }
    
    
    
    //MARK: - Helpers
    
}


















