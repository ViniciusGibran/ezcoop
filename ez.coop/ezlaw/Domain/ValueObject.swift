//
//  ValueObject.swift
//  ezlaw
//
//  Created by Vinicius Gibran on 17/10/16.
//  Copyright © 2016 ezlaw. All rights reserved.
//

import UIKit
import iOSUtils_Swift

class ValueObject: NSObject {
    
    //MARK: - Singleton
    static var instance : ValueObject?
    static func shared() -> ValueObject {
        if let instance = ValueObject.instance {
            return instance
        }
        instance = ValueObject()
        return ValueObject.instance!
    }
    
    
    //MARK: - Variables
    var client : [Client] = []
    var group : [Group] = []
    
    var groupIDs : Int?{
        set{
            if let ids = Prefs.getInt("DB_IDs"){
                self.groupIDs = ids
            }
        }
        get{
            if let ids = self.groupIDs{
                return ids
            }else{
                return -1
            }
        }
    }
    
    
    //MARK: - Getters
    func getClientObj() -> [Client]?{
            return client
    }
    
    func getGroupObjList() -> [Group]?{
        return group
    }
    
    //MARK: - Setters
    func setClientObjList(cObj: Client){
       self.client.append(cObj)
    }
    
    func setGroupObj(gObj: Group){
        if let id = DataBaseHelper.getGroupIDs(){
            if id == -1{
                let value = 1
                gObj.groupID = value
                DataBaseHelper.setGroupDB(gObj, id: String(value))
                DataBaseHelper.setGroupIDs(value)
            }else{
                let value = id + 1
                gObj.id = value
                DataBaseHelper.setGroupDB(gObj, id: String(value))
                DataBaseHelper.setGroupIDs(value)
            }
            
        }
        self.group.append(gObj)
    }
}
