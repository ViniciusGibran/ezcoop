//
//  DataBaseHelper.swift
//  ezlaw
//
//  Created by Vinicius Gibran on 17/10/16.
//  Copyright © 2016 ezlaw. All rights reserved.
//

import UIKit
import iOSUtils_Swift

class DataBaseHelper {
    
    static func setGroupIDs(value: Int){
        Prefs.setInt(value, forKey: "DB_IDs")
    }
    
    static func getGroupIDs() -> Int?{
        if let value = Prefs.getInt("DB_IDs"){
            return value
        }else{
            return -1
        }
    }

    static func setGroupDB(obj: Group, id: String){
        let dic = obj.toDictionary()
        Prefs.setObject(dic, forKey: id)
    }
    
    static func getGroupListFromDB() -> [Group]?{
        var response : [Group] = []
        
        if let value = Prefs.getInt("DB_IDs"){
            if value != -1{
                for i in 0...value{
                    let id = String(i)
                    if let obj = Prefs.getObject(id){
                        let groupObj = Group()
                        let clientObj = Client()
                        
                        let objDict = obj as! [String: AnyObject]
                        let cliDic = objDict.getDictionaryWithKey("client")
                        
                        clientObj.name = cliDic.getStringWithKey("name")
                        
                        groupObj.client = clientObj
                        groupObj.groupID = objDict.getIntWithKey("groupID")
                        groupObj.name = objDict.getStringWithKey("name")
                        
                     response.append(groupObj)
                    }
                }
            return response
            }
        }
        return nil
    }
}
